// crystal.h
//
// Definition of the crystal-related data types
//


#ifndef _CRYSTAL_
#define _CRYSTAL_

#include "dmatrix.h"
#include "cmatrix.h"
#include "vector2.h"
#include "vector3.h"
#include "potential.h"
#include "table.h"

#ifndef NULL
#define NULL	0
#endif

#define CELEM_UNUSED 0
#define CELEM_USED   1

// For spin dynamics flags from and to
#define CCELL_SD_UNSET 0
#define CCELL_SD_SET   1

struct csurfpot{
  int         ng;     // Number of reciprocal lattice vectors g
  int         *h;
  int         *k;     // Indices of the gs
  double      zmin;
  double      zmax;
  int         nstep;  // z range
  doubletable *vg;    // Potential V_g(z)
};

struct celem{
  potential	p;		// Atomic potential
  int		used;		// Flag for being used
  double        conc;           // Concentration (for disorder)
  char		name[80];	// Name
  celem();			// Constructor
  ~celem();			// Destructor
};

struct catom{
  int		eind;		// Index of the associated element
  celem		*elem;		// Pointer to the associated element
  vector3	s;		// Position within the unit cell [in Bohr]
  vector3	m;		// Magnetization direction
//  complex	ap, am, az;	// Local electric field vector (for photoemission)
  complexmatrix	R;		// Local rotation matrix
  catom();			// Constructor
  ~catom();			// Destructor
};

struct cslab{
  double	t;		// Thickness of the slab
  double	v0;		// Muffin-tin zero [in Hartree]
  double        uor[3];         // Real part of the optical potential, upper state
  double        uoi[3];         // Imaginary part of the optical potential, upper state
  double        lor[3];         // Real part of the optical potential, lower state
  double        loi[3];         // Imaginary part of the optical potential, lower state
  int		natom;		// Number of atoms in the unit cell
  catom		*atom;		// Pointer to the atoms
  cslab();			// Constructor
  ~cslab();			// Destructor
};

struct cslice{
  int		sind;		// Index of the slice
  cslab		*slab;		// Pointer to the associated slab
  vector3	d;		// Displacement to the next layer [in Bohr]
  cslice();			// Cnstructor
  ~cslice();			// Destructor
};

struct  celementry{
  int           layer;          // Layer index
  int           cell;           // Cell index
  int           cell1;          // Cell index
  int           cell2;          // Cell index
  int           atom;           // Site index
  vector3       displacement;   // Displacement from bulk position
  char          name[80];       // Name of the element
  int		eind;		// Index of the element
  celem		*elem;		// Pointer to the element
};

struct celemlist{
  int           nelem;           // Number of elements
  celementry     *elem;          // The elements
};


struct ccell{
  int           cind;             // Cell index
  int           n1;               // Displacement lattice vector 1
  int           n2;               // Displacement lattice vector 2
  vector2       r;                // Displacement vector (relative to the layer origin)
  vector3       rabs;             // Displacement vector (relative to the crystal origin, defined by layer 0)
  int		natom;		  // Number of atoms in this cell (same for all cells)
  catom		*atom;		  // Pointer to the atoms
  int           *defflag;         // Flags for defects
  celementry    *defect;          // Pointer to the defects
  int		from; 		  // Flag for spin dynamics calculations
  int           to; 		  // Flag for spin dynamics calculations
  ccell();			  // Constructor
  ~ccell();			  // Destructor
};

struct clayer{
  int		cind;		// Index of the layer
  cslice	*slice;		// Pointer to the associated slice
  int           ncell;          // Number of cells in this layer (for real-space modes)
  int           ncelloffset;    // Offset of the total cell index of this layer (for real-space modes)
  ccell         *cell;          // Pointer to cells  (for real-space modes)
  vector3       pos;            // Position of the layer (w.r.t. the layer with index 0)
  clayer();			// Constructor
  ~clayer();			// Destructor
};

struct crystal{
  double	lattice;	// Lattice constant [in Bohr]
  double	ar;		// Area of the unit cell [in Bohr^2]
  vector2	r1, r2; 	// Lattice vectors [in Bohr]
  vector2	b1, b2; 	// Reziprocal lattice vectors [in 1/Bohr]
  int		nelem;		// Number of elements
  celem		*elem;		// Pointer to the elements
  int		nslab;		// Number of slabs
  cslab		*slab;		// Pointer to the slabs
  int		nslice;		// Number of slices
  cslice	*slice;		// Pointer to the slices
  int		nbulk;		// Number of bulk layers (bulk side, ending at layer nlayer-1)
  int		nbulk2;		// Number of bulk layers (surface side, starting at layer 0)
  int           nlr[4];         // Integration range, for J_BWT_GF_NOME
  clayer	*layer;		// Pointer to the layers
  int           ncelltotal;     // Total number of cells in the cluster (for real-space modes))
  int           ubarrier;       // Surface barrier type, upper state
  double        ubparamup[5];   // Parameters for surface barrier, upper state (spin up)
  double        ubparamdo[5];   // Parameters for surface barrier, upper state (spin down)
  vector3       ubparamv;       // Magnetization direction in the surface barrier, upper state
  int           ubarrier2;      // Surface barrier type, upper state (bulk side)
  double        ubparam2[5];    // Parameters for surface barrier, upper state (bulk side)
  int           lbarrier;       // Surface barrier type, lower state
  double        lbparamup[7];   // Parameters for surface barrier, lower state (spin up)
  double        lbparamdo[7];   // Parameters for surface barrier, lower state (spin down)
  vector3       lbparamv;       // Magnetization direction in the surface barrier, lower state
  int           lbarrier2;      // Surface barrier type, lower state (bulk side)
  double        lbparam2[4];    // Parameters for surface barrier, lower state (bulk side)
  complextable	ubtab2;		// Table for barrier, upper state
  complextable	lbtab2;		// Table for barrier, lower state
  csurfpot      usp;             // Corrugated surface barrier, upper state
  csurfpot      lsp;             // Corrugated surface barrier, lower state
  doublematrix	Cp, Cz, Cm;	// Selection rules, for pohotoemission
  celemlist     defectelems;    // List of defect elements (for real-space modes)
  int           scp1;           // Supercell size parameter
  int           scp2;           // Supercell size parameter
  crystal();			// Constructor
  ~crystal();			// Destructor
};

inline	celem::celem(){
}

inline	celem::~celem(){
}

inline	catom::catom(){
}

inline	catom::~catom(){
}

inline	cslab::cslab(){
  atom = NULL;
}

inline	cslab::~cslab(){
  delete[] atom;
}

inline	cslice::cslice(){
}

inline	cslice::~cslice(){
}

inline	ccell::ccell(){
}

inline	ccell::~ccell(){
}


inline	clayer::clayer(){
}

inline	clayer::~clayer(){
}

inline	crystal::crystal(){
  elem = NULL;
  slab = NULL;
  slice = NULL;
  layer = NULL;
}

inline	crystal::~crystal(){
  delete[] elem;
  delete[] slab;
  delete[] slice;
  delete[] layer;
}

#endif
