%
% Note on angles in SARPES
%
\documentclass[a4paper,fleqn]{scrreprt}


\usepackage{a4}
\usepackage{cite}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{units}
\usepackage{wasysym}
\usepackage[english]{babel}
\selectlanguage{english}

\renewcommand{\vec}[1]{\boldsymbol{#1}}
\newcommand{\mat}[1]{\boldsymbol{\mathsf{#1}}}

\newcommand{\divergence}{\mathrm{div}\,}
\newcommand{\rot}{\mathbf{rot}\,}
\newcommand{\dAlembert}{\Box\,}
\newcommand{\Laplace}{\triangle\,}
\newcommand{\qed}{\hfill\ensuremath{\Square}}

\renewcommand{\Im}{\mathrm{Im}\,}
\renewcommand{\Re}{\mathrm{Re}\,}
\newcommand{\Tr}{\mathrm{Tr}\,}

\newcommand{\cone}{\mathrm{i}}
\newcommand{\euler}{\mathrm{e}}
\newcommand{\grad}{\vec{\nabla}}
\newcommand{\conj}[1]{{#1}^{\star}}

\newcommand{\braket}[1]{\langle {#1} \rangle}
\newcommand{\bra}[1]{\langle {#1} |}
\newcommand{\ket}[1]{| {#1} \rangle}
\newcommand{\operator}[1]{\ensuremath{\hat{#1}}}
\newcommand{\species}[1]{\ensuremath{\mathrm{#1}}}
\newcommand{\average}[1]{\ensuremath{\langle #1 \rangle}}

\parindent 1em
\parskip 0ex

\hyphenation{nano-struc-tu-re nano-struc-tu-res}



\begin{document}

\section*{Angles in Photoemission}
\textsc{omni} can treat various setups in photoemission.

\minisec{Fixed angles between light incidence and electron emission}
This is a common setup in ARPES experiments because the layout of the chamber dictates the angles between light incidence direction and electron detection direction. The sample is rotated by the manipulator.

If the electrons are detected in normal emission, that is
\begin{align*}
 \vec{e}_{\mathrm{el}} = \begin{pmatrix} 0 \\ 0 \\ 1 \end{pmatrix},
\end{align*}
the light impinges at angles $\psi$ (polar angle) and $\phi$ (azimuth), that is, 
\begin{align*}
 \vec{e}_{\mathrm{ph}} = \begin{pmatrix} \sin\psi \cos\phi \\ \sin\psi\sin\phi \\ \cos\psi \end{pmatrix}.
\end{align*}

\paragraph{Example.} A chamber in Markus Donath's group has four detectors, namely \textsf{C1}, \textsf{C2}, \textsf{C3} and \textsf{C4}; see figure~\ref{fig:setups}. The angles are given in table~\ref{tab:angles}.

\begin{figure}[h]
 \centering
 \includegraphics[width = \columnwidth]{ExpSetup}
 \caption{Photoemission setups in Markus Donath's group.}
 \label{fig:setups}
\end{figure}

\begin{table}[h]
 \centering
\begin{tabular}{ccc}
 \hline \hline
 Detector & $\psi$ & $\phi$ \\
 \hline
 \textsf{C1} & $\unit[70]{^{\circ}}$ & $\unit[180]{^{\circ}}$ \\
 \textsf{C2} & $\unit[35]{^{\circ}}$ & $\unit[90]{^{\circ}}$ \\
 \textsf{C3} & $\unit[35]{^{\circ}}$ & $\unit[0]{^{\circ}}$ \\
 \textsf{C4} & $\unit[65]{^{\circ}}$ & $\unit[32]{^{\circ}}$ \\
 \hline \hline
\end{tabular}
\caption{Angles for the setups of figure~\ref{fig:setups}.}
\label{tab:angles}
\end{table}
\hfill\qed

The sample is rotated by a given axis $\vec{a}$ which lies within the sample surface ($a_{z} = 0$). It is defined by the azimuth $\Psi$, with respect to the $x$ axis. The rotation is thus a change of the polar angle $\Theta$. The rotation matrix reads
\begin{align*}
 \mat{R}(\vec{a}, \Theta)
 & = 
  \begin{pmatrix}
    \cos\Theta + a_{x}^{2} (1 - \cos\Theta) & a_{x} a_{y} (1 - \cos\Theta) &  a_{y} \sin\Theta \\
    a_{x} a_{y} (1 - \cos\Theta)            & \cos\Theta + a_{y}^{2} (1 - \cos\Theta) &  -a_{x} \sin\Theta \\
    -a_{y} \sin\Theta                       & a_{x} \sin\Theta & \cos\Theta 
  \end{pmatrix},
\end{align*}
with
\begin{align*}
 a_{x} & = \cos\Psi,
 \\
 a_{y} & = \sin\Psi.
\end{align*}


We now calculate the electron and light direction vectors that are subject to the sample rotation. First, electrons:
\begin{align*}
 \vec{e}_{\mathrm{el}}(\Psi, \Theta)
 & = 
  \begin{pmatrix}
    \cos\Theta + a_{x}^{2} (1 - \cos\Theta) & a_{x} a_{y} (1 - \cos\Theta) &  a_{y} \sin\Theta \\
    a_{x} a_{y} (1 - \cos\Theta)            & \cos\Theta + a_{y}^{2} (1 - \cos\Theta) &  -a_{x} \sin\Theta \\
    -a_{y} \sin\Theta                       & a_{x} \sin\Theta & \cos\Theta 
  \end{pmatrix}
  \begin{pmatrix} 0 \\ 0 \\ 1 \end{pmatrix}
  \\
  & = 
  \begin{pmatrix} a_{y} \sin\Theta \\ -a_{x} \sin\Theta \\ \cos\Theta \end{pmatrix}
  \\
  & = 
  \begin{pmatrix} \sin\Psi \sin\Theta \\ -\cos\Psi \sin\Theta \\ \cos\Theta \end{pmatrix}.
\end{align*}
Then photons:
\begin{align*}
 \vec{e}_{\mathrm{ph}}(\Psi, \Theta; \psi, \phi)
 & = 
  \begin{pmatrix}
    \cos\Theta + a_{x}^{2} (1 - \cos\Theta) & a_{x} a_{y} (1 - \cos\Theta) &  a_{y} \sin\Theta \\
    a_{x} a_{y} (1 - \cos\Theta)            & \cos\Theta + a_{y}^{2} (1 - \cos\Theta) &  -a_{x} \sin\Theta \\
    -a_{y} \sin\Theta                       & a_{x} \sin\Theta & \cos\Theta 
  \end{pmatrix}
 \begin{pmatrix} \sin\psi \cos\phi \\ \sin\psi\sin\phi \\ \cos\psi \end{pmatrix}.
\end{align*}

\paragraph{Example -- rotation about the $x$ axis.} Here, $\Psi \equiv 0$ yields $a_{x} = 1$ and $a_{y} = 0$:
\begin{align*}
 \mat{R}(0, \Theta)
 & = 
  \begin{pmatrix}
    1  & 0  &  0  \\
    0  &  \cos\Theta & -\sin\Theta \\
    0  &  \sin\Theta &  \cos\Theta 
  \end{pmatrix}
\end{align*}
which is the well-known rotation matrix for the $x$ axis. Furthermore,
\begin{align*}
 \vec{e}_{\mathrm{el}}(0, \Theta)
  & = 
  \begin{pmatrix} 0 \\ -\sin\Theta \\ \cos\Theta \end{pmatrix}
\end{align*}
and
\begin{align*}
 \vec{e}_{\mathrm{ph}}(0, \Theta; \psi, \phi)
  & = 
  \begin{pmatrix}
    \sin\psi \cos\phi \\
    \cos\Theta \sin\psi\sin\phi - \sin\Theta \cos\psi \\
    \sin\Theta \sin\psi\sin\phi +  \cos\Theta \cos\psi 
  \end{pmatrix}.
\end{align*}



\hfill\qed

\paragraph{Example -- rotation about the $y$ axis.} Here, $\Psi \equiv \unit[90]{^{\circ}}$ yields
\begin{align*}
 \mat{R}(\unit[90]{^{\circ}}, \Theta)
 & = 
  \begin{pmatrix}
    \cos\Theta  & 0 &  \sin\Theta \\
    0 & 1  &  0 \\
    - \sin\Theta  & 0 & \cos\Theta 
  \end{pmatrix},
\end{align*}
And for the vectors:
\begin{align*}
 \vec{e}_{\mathrm{el}}(\unit[90]{^{\circ}}, \Theta)
 & = 
  \begin{pmatrix} \sin\Theta \\ 0 \\ \cos\Theta \end{pmatrix},
\\
 \vec{e}_{\mathrm{ph}}(\unit[90]{^{\circ}}, \Theta; \psi, \phi)
  & = 
  \begin{pmatrix}
    \cos\Theta  \sin\psi \cos\phi + \sin\Theta \cos\psi \\
    \sin\psi\sin\phi \\
    - \sin\Theta \sin\psi \cos\phi + \cos\Theta \cos\psi
  \end{pmatrix}.
\end{align*}

\hfill\qed

\paragraph{Setup in \textsf{omni}.} To run this mode correctly, use the following recipe. We give an example for detector \textsf{C2}.
\begin{enumerate}
 \item Set \verb|theta| and \verb|phi| for the light incidence direction. \verb|aflag| has to be $3$ (`with respect to the detection direction, alternative version').
 \begin{verbatim}
+ Electric-field parameters (only for photoemission) ++++
(1.000000, 0.0000)                      epsilon
35.00000  90.00000 3                    theta, phi
(1.00000, 0.00000) (0.00000, 1.00000)   as, ap
 \end{verbatim}

\item Set the $k_{\parallel}$ mode to $6$ (see file \verb|spec.h|). The rotation is about the $y$ axis, thus $\unit[90]{^{\circ}}$. The tilting is from $\unit[-10]{^{\circ}}$ to $\unit[+10]{^{\circ}}$ in steps of $\unit[1]{^{\circ}}$.
 \begin{verbatim}
+ k-parallel and angular parameters ++++++++++++++++++++
 6                       k-parallel mode
 0 90.0 -10.0 10.0 1.0   file flag, azimuth of the rotation axis, range of tilt angle
 \end{verbatim}

\end{enumerate}


\end{document}

