\documentclass[12pt,a4paper]{article}
\usepackage{ae}
\usepackage[T1]{fontenc}

\input{defines}

\begin{document}
\begin{center}
        \textbf{\LARGE \omni}

        \textbf{\large Fully relativistic electron spectroscopy calculations}

        Installation Notes

        \textit{\today}
\end{center}


\section{Compilation and installation}
\subsection{Platforms}
The package has been successfully compiled and run on the following
platforms:
\begin{itemize}
\item DEC alpha (DEC unix), g++ version 2.8.x.x \ldots\ 4.6.x.x
\item SGI, MIPS compiler
\item Pentium Pro, Pentium II, and Pentium III PCs under Linux
  (RedHat~5.2 with egcs 1.0.3, RedHat~6.0 with egcs 1.1.2, and
  SuSe~6.0, \ldots, 7.3 with g++ 2.95.3)
\item Compaq Tru64 UNIX V5.1A (DEC alpha)
\item SUN FIRE6800 (Solaris 8.0)  
\end{itemize}
There is a single makefile in each source directory. The compiler
flags are in the `flags.machine' files where `machine' is your
computer. These are linked to the file `flags'.

From now on, openmp v.3 is required.

\subsection{Unpacking the archive}
\begin{enumerate}
\item Change the current directory to a suitable place.
\item Type \command{unzip omni2k.zip}. This creates the directory tree
  and unpacks the archive.
\end{enumerate}

\subsection{Directory structure}
The whole package is stored using the following directory structure.
\begin{description}
\item[applications] contains subdirectories with (more or less)
  physically meaningful examples (input and output files).
\item[bin] contains the binary files (executables) for the various
  programs.
\item[doc] contains the documentation.
\item[source] contains the source code in a hierarchical structure.
  Further, the codes for the main functions of the programs and
  global header files are located here.
  \begin{description}
  \item[platform] contains platform specific code.
  \item[math] contains basic mathematical functions and class
    definitions (uses \command{platform.a}).
  \item[basic] contains functions for basic tasks; for example,
    compute a Green function (uses \command{platform.a} and
    \command{math.a}).
  \item[modes] contains the mode functions for \omni\ (uses
    \command{platform.a}, \command{math.a}, and \command{basic.a}).
  \end{description}
\item[test] contains input and output files for testing purposes.
\end{description}
  
\subsection{Building the programs}
The package comprises different programs:
\begin{description}
\item[\omni] the electron spectroscopy program
\item[\pecore] program for photoemission from core-levels of atoms
\item[\vcm]  computes potentials in the virtual crystal approximation.
\item[\emesh] generates input files of energy contours.
\item[\gmesh] generates input files of semi-circle energy contours
  used in magnetic anisotropy calculations.
\item[\gl] generates abscissas and weights for Gauss-Legendre quadrature
\item[\kmesh] generates sets of special points for Brillouin zone integration.
\item[\benergy] calculates from \omni\ output the band energies 
  (used for magnetic anisotropy calculations).
\item[\denergy] calculates  the dipole-dipole energies 
  (used for magnetic anisotropy calculations).
\item[\moments] calculates from \omni\ output the magnetic moments
  (used for magnetic anisotropy calculations).
\item[\geninput] generates an input file for \omni.
\end{description}

Follow the instructions below in order to make the executables.
\begin{enumerate}
\item In directory omni2k execute the shell script
  \command{Install.platform}. On a Linux machine, just type
  \command{Install.linux}. This will link the platform-dependent make
  files \command{Makefile.machine} to \command{Makefile}.
\item Change the current directory to omni2k/source.
\item Type \command{make depend} in order to create the dependency files.
\item Type \command{make} in order to compile the libraries and \omni.
\item Type \command{make program}. This creates in
  omni2k/bin the executables for the program \command{program}, where
  \command{program} is one of the following: \command{omni},
  \command{pecore}, \command{vcm}, \command{kmesh}, \command{emesh},
  \command{gmesh}, \command{gl}, \command{benergy}, \command{denergy},
  \command{moments}, \command{machine}, \command{geninput}.
\item Alternatively you can type \command{make all} to build the whole package
\end{enumerate}

Setting the variable DEBUG will activate the debug version of the code
which is used in particular in the mathematical part (files in the
directory \command{source/math}). Instead of using the
\command{Install.machine} script, use \command{Install.machine.debug}
and the corresponding make files are linked (\textit{Do not to forget
  to remove the object files! Use \command{make clean}}).

\subsection{Conditional compilation}
Global parameters can or have to be set in the makefiles in order to
perform conditional compilation. The parameters are described below.
\begin{description}
\item[USELU] In order to increase the computational accuracy, some
  functions for complex matrices are implemented using LU
  decomposition and iterative improvement (\command{invlu},
  \command{solvelu}, \command{solve2lu}, \command{detlu})---instead of
  using the simple Gau\ss{}\ elimination algorithm. If you want to use
  the former functions, define \command{USELU} in the respective
  makefiles. Otherwise the simpler but faster functions will be used.
\item[LINUX] Defines conditional compilation for Linux PCs.
\item[SGI] Defines conditional compilation for SGI machines. For
  example, some constructions for functions of complex matrices
  compile using g++ but not with the MIPS compiler on a SGI\@. This
  problem is circumvented using conditional compilation.
\item[DEBUG] Activates the debug part of the code, in particular in
  the mathematical part (directory \command{source/math}).
\end{description}

Platform specific functions of general type are located in the
directory source/platform/. For example, the \command{exit()} function
is included from \command{/usr/include/unistd.h} on SGI machines (See
also: B.  Stroustrup, \textit{The C++ Programming Language}, 2nd
edition, p.\ 85, (Addison-Wesley, Reading, 1991). This file is
included if the variable \command{SGI} is defined.

The perl script \command{build.increase} counts the number of
compilations. Probably you have to change the fist line of this script
in order to find the perl program. If you do not know where to find
it, type \command{which perl} or \command{whereis perl}.

\subsection{Known problems}
\begin{itemize}
\item Under RedHat5.2 using egcs version 1.0.3, basic/see.C and
  basic/leed.C have to be compiled without optimization (-O0).
  Otherwise a segmentation fault occurs during runtime.
\item Under RedHat6.0 using egcs version 1.1.2, the complete package has
  to be compiled without alignment (-malign-double).  Otherwise segmentation
  fault and/or core dumps occur during runtime.
\end{itemize}

\subsection{Machine accuracy}
The supplied program \machine\ determines the least number $d$
for which $1 + d > 1$. It can be compiled using \command{make
  machine}.

\section{Conversion of \omni\ output files}
The output files written by \omni\ are usually not ideally suited as
input files for graphing programs. Thus, programs for format
conversion have to be supplied. Instead of writing C++ programs for
these tasks, we supply scripts written in perl (Practical Extraction
and Report Language), an interpreter language which should be
installed on any somewhat modern computer. These scripts can be found
in the directory \command{convert}.

\section{Generation of input files for \omni}
The program \geninput\ generates input files for \omni\ from scratch.
It asks the user for parameters (such as type of the calculation for
example) and makes some simple checks on the input specified by the
user. Note that the input checks in \omni\ are more strict. Here rules
the gigo principle: garbage in, garbage out.

\section{Applications}
As a standard application, the band structures, density of states, and
photoemission from Cu-films on fcc-Co(001) are included (see directory
\command{applications/cu.co001/}). Further, free electrons are
calculated (see \command{application/free.electrons}).

The functions for solving ordinary differential equations can be
tested using the codes in \command{ode.test}. One code version is for
real ODEs (\command{dodetest.C}), the other for complex ones
(\command{codetest.C}).

\section{Agenda}
There is a lot of work to do. 
\begin{itemize}
  \item All comments translated into English.
  \item The verbosity should be changeable (This is alreay implemented
    in some code files via the variable \command{VERBOSITY}).
  \item The documentation has to enhanced.
  \item In the far future, self-consistency should be included.
\end{itemize}

\section{Contributors}
In alphabetical order: Peter Bose, Roland Feder, Samed Halilov, J\"urgen Henk,
Hossein Mirhosseini, Francisco Mu\~{n}oz, Thomas Scheunemann, Eiichi Tamura

\section{Contact}
In case of questions or problems, mail to juergen.henk@physik.uni-halle.de\ .

\end{document}