// eldenstot.h
//
// Avaraged charge density
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void eldenstot(job &j, crystal &c, crystal &c2, int lflagw, std::ofstream &out);

