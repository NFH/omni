// dos.h
//
// Density of states
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void dos(job &j, crystal &c, crystal &c2, int lflagw, std::ofstream &out);

