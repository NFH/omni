// sigma_elph.C
//
// Main program
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <omp.h>

#include "constants.h"
#include "spec.h"
#include "job.h"
#include "crystal.h"

#include "actual.h"
#include "iostuff.h"
#include "electron-phonon.h"

#include "gauss.h"

using namespace std;


// The mode function

void sigma_elph(job &j, ofstream &out, crystal &c){

  complex sigma;
  
  actual a;
  
  unsigned int ieval;

  int ielem; 
  
  double kt, debye_temperature, kt_debye;
  double mue, lambda;
 
  Logger logger(std::cout);
  if(j.parallel.mpi.rank != j.parallel.mpi.NO_MPI)
    logger.setHeader(string("$LEVEL (proc=$RANK) from sigma_elph(): "));
  else
    logger.setHeader(string("$LEVEL from sigma_elph(): "));
  
  connect_actual(a, c);
  
  
  // Write header (for GLE)
  out << "! Energy range ";
  out << setw(15) << setprecision(10) << j.general.el[0].item1 * HART;
  out << setw(15) << setprecision(10) << j.general.el[j.general.el.size() - 1].item1  * HART;
  out << setw(15) << setprecision(10) << j.general.el.size();
  out << endl;

  // Temperature, in Kelvin
  kt = j.general.t * BLTZ;
  logger << "Temperature = " << setw(12) << setprecision(6) << j.general.t << " K equiv." << setw(12) << setprecision(6) << kt * HART << " eV" << endl << endl;

 
  // Loop over all elements
  for(ielem = 0; ielem < c.nelem; ielem++){
    logger << "Element " << ielem << " " << c.elem[ielem].name << endl;
  
    debye_temperature = c.elem[ielem].p.td;
    kt_debye = debye_temperature * BLTZ;
    logger << "Debye temperature = " << setw(12) << setprecision(6) << debye_temperature << " K equiv." << setw(12) << setprecision(6)  << kt_debye * HART << " eV" << endl;

    // Fermi energy
    mue = c.elem[ielem].p.ef;
    logger << "Fermi energy = " << setw(12) << setprecision(6) << mue * HART << " eV" << endl;

    // Electron-phonon coupling strength
    lambda = c.elem[ielem].p.lambda;
    logger << "Electron-phonon coupling strength = " << setw(12) << setprecision(6) << lambda << endl;

    // Some output for GLE
    out << "! Element " << ielem << " " << c.elem[ielem].name << endl;
    out << "! Debye temperature = ";
    out << setw(15) << setprecision(10) << debye_temperature << " K";
    out << endl;
    out << "! Debye energy = ";
    out << setw(15) << setprecision(10) << kt_debye * HART << " eV";
    out << endl;
    out << "! Temperature = ";
    out << setw(15) << setprecision(10) << j.general.t << " K";
    out << endl;
    out << "! Thermal energy = ";
    out << setw(15) << setprecision(10) << kt * HART << " eV";
    out << endl;
    out << "! Fermi energy = ";
    out << setw(15) << setprecision(10) << mue * HART << " eV";
    out << endl;
    out << "! Electron-phonon coupling strength = ";
    out << setw(15) << setprecision(10) << lambda;
    out << endl;

    // Energy loop
    for(ieval = 0; ieval < j.general.el.size(); ieval++){
      // Set actual energy
      a.e = cmplx(j.general.el[ieval].item1, j.general.el[ieval].item2);

      // Self-energy due to electron-phonon coupling
      sigma = electron_phonon_selfenergy(a.e.re, kt, kt_debye, mue, lambda);
    
      logger << "Energy = " << setw(15) << setprecision(10) << a.e.re * HART << " eV";
      logger << " Re(self-energy) = " << setw(15) << setprecision(10) << sigma.re * HART << " eV";
      logger << " Im(self-energy) = " << setw(15) << setprecision(10) << sigma.im * HART << " eV" << endl;

      out << setw(15) << setprecision(10) << a.e.re * HART << setw(15) << setprecision(10) <<  sigma.re * HART << setw(15) << setprecision(10) <<  sigma.im * HART << endl;
    } // end ieval
    out << endl;
    logger << endl;
    
  } // end ielem
  logger << endl;
}
