// spectral.h
//
// Electron density
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void eldens(job &j, crystal &c, crystal &c2, int lflagw, std::ofstream &out);

