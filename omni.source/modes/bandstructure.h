// bandstructure.h
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function
void bandstructure(job &j, crystal &c, crystal &c2, int lflagw, std::ofstream &out);

