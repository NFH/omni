// spectralrs.h
//
// Bloch spectral function in defect mode
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void spectralrs(job &j, crystal &c, int lflagw, std::ofstream &out);

