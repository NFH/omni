// weissfield.h
//

#include <iostream>

#include "crystal.h"
#include "job.h"

// The mode function
void weissfield(job &j, crystal &c, int lflagw, std::ofstream &out);
