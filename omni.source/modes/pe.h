// pe.h
//

#include <fstream>

#include "crystal.h"
#include "job.h"

// The mode function

void pe(job &j, crystal &c, crystal &c2, int lflagw, int uflagw, std::ofstream &out);
