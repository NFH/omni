// bwtsc.h
//
// Bloch-wave transmission with supercell
//

#include <fstream>

#include "crystal.h"
#include "job.h"
#include "supercell.h"

// The mode function

void bwtsc(job &j, crystal &c, crystal &c2, int lflagw, std::ofstream &out);
