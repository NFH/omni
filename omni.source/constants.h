// constants.h
//
// defines physical constants in Hartree atomic units
// e = m = hbar = 1
//
// Speed of light in atomic units
#define CLIGHT		137.036
#define CLIGHT2		18778.865296

// Boltzmann constant (conversion Kelvin - hartree)
#define BLTZ		0.31667E-5

// Proton mass
#define HMASS		1.836E3

// 1 hartree in eV
#define HART		  27.2116
#define AU_ENERGY	27.2116

// 1 bohr in Angstroem
#define BOHR    0.529177249

// 1 atomic time unit  in fs
#define AU_TIME 0.02418884326505

// Euler's constant
#define EULERGAMMA 0.577215664901532860606512

// Machine accuracy
#define EMACH		1.E-15
