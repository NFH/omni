// ldosdis.h
//
// enthaelt Prototypen der in ldosdis.C definierten Funktionen
//

#include <iostream>
using namespace std;

#include "job.h"
#include "state.h"
#include "crystal.h"

void ldosdis(actual &a, job &j, state &s, crystal &c, state &s2, crystal &c2, state &s3, state &s4, ostream &os, int lflagw, int ikval);
void ldosdis3(actual &a, job &j, state &s, crystal &c, state &s2, crystal &c2, state &s3, crystal &c3, state &s4, state &s5, ostream &os, int lflagw, int ikval);

void compdosdis(actual &a, job &j, state &s, crystal &c, state &s2, crystal &c2, state &s3, ostream &os, int lflagw);

