// spectral.C
//
// Main program
//

#include <iostream>
using namespace std;

#include <fstream>
#include <iomanip>
#include <math.h>

#include "constants.h"
#include "spec.h"
#include "beam.h"
#include "crystal.h"
#include "job.h"
#include "state.h"

#include "actual.h"
#include "connect.h"
#include "disorder.h"
#include "ldos.h"
#include "ldosdis.h"
#include "mmat.h"
#include "order.h"
#include "rotate.h"
#include "xmatk.h"
#include "xmatkc.h"

// JH Test
#include "vintegrate.h"


// The mode function

void spectral(job &j, crystal &c, crystal &c2, crystal &c3, int lflagw, ofstream &out){
  actual a;
  
  int    ikval, ieval;
  
  // Energy loop
  for(ieval = 0; ieval < j.general.el.nval; ieval++){

    beamset b;
    
    state l, l2, l3, l4, l5;

    // Set actual energy
    a.e = cmplx(j.general.el.val[ieval].item1, j.general.el.val[ieval].item2);

    // Energy in vacuum
    cout << "MESSAGE from spectral(): Calculation for energy ";
    cout << setw(12) << setprecision(6) << a.e * HART;
    cout << " eV" << endl;
    l.ev = a.e;
    switch(j.general.dis){
    case DIS_ATA:
    case DIS_CPA:
      l2.ev = l.ev;
      l3.ev = l.ev;
      l4.ev = l.ev;
      break;
    case DIS_CPA3:
      l2.ev = l.ev;
      l3.ev = l.ev;
      l4.ev = l.ev;
      l5.ev = l.ev;
      break;
    default:
      break;
    }
    
    connect(j, l, c, lflagw);
    switch(j.general.dis){
    case DIS_ATA:
    case DIS_CPA:
      connect(j, l2, c2, lflagw);
      connect(j, l3, c2, lflagw);
      connect(j, l4, c2, lflagw);
      break;
    case DIS_CPA3:
      connect(j, l2, c2, lflagw);
      connect(j, l3, c2, lflagw);
      connect(j, l4, c2, lflagw);
      connect(j, l5, c2, lflagw);
      break;
    default:
      break;
    }
    
    // Calculate single-site t-matrices
    tmat(l, c, j.general.x, j.general.t, j.general.optpotl, j.general.spec);
    switch(j.general.dis){
    case DIS_ATA:
      tmat(l2, c2, j.general.x, j.general.t, j.general.optpotl, j.general.spec);
      ata(l, c, l2, c2, l3);
      break;
    case DIS_CPA:
      tmat(l2, c2, j.general.x, j.general.t, j.general.optpotl, j.general.spec);
      ata(l, c, l2, c2, l3);
      ata(l, c, l2, c2, l4);
      break;
    case DIS_CPA3:
      tmat(l2, c2, j.general.x, j.general.t, j.general.optpotl, j.general.spec);
      tmat(l3, c3, j.general.x, j.general.t, j.general.optpotl, j.general.spec);
      ata3(l, c, l2, c2, l3, c3, l4);
      ata3(l, c, l2, c2, l3, c3, l5);
      break;
    default:
      break;
    }
    
    // Integral
    imat(l, c);
    switch(j.general.dis){
    case DIS_ATA:
    case DIS_CPA:
      imat(l2, c2);
      break;
    case DIS_CPA3:
      imat(l2, c2);
      imat(l3, c3);
      break; 
    default:
      break;
    }
    
    // JH Test begin
    // JH The following lines has been taken from spectral.C
    // Compute the k-averaged V matrices of the effectice medium (s3)
    switch(j.general.dis){
    case DIS_ATA:
      ata(l, c, l2, c2, l4);

      // Averaging over k-parallel
      vmatmean(j, a, l3, l4, c, V_DIAGONAL);
      break;
    case DIS_CPA:
      cpa(j, a, l, c, l2, c2, l3, l4, lflagw);
      copyT(l3, c, l4);
      
      // Averaging over k-parallel
      vmatmean(j, a, l3, l4, c, V_DIAGONAL);
      break;
    case DIS_CPA3:
      cpa3(j, a, l, c, l2, c2, l3, c3, l4, l5, lflagw); //l4 and l5 instead of old l3 and l4
      copyT(l4, c, l5);
      
      // Averaging over k-parallel
      vmatmean(j, a, l4, l5, c, V_DIAGONAL);
      break;
    default:
      break;
    } 
    // JH Test end


    // For each angle or k-parallel
    for(ikval = 0; ikval < j.general.kl.nval; ikval++){
      
      getkpar(j, a, ikval);
      get_beamset(a.e.re, j.general.eg, a.q, c, b);
      // Has k-parallel to be rotated?
      if(lflagw & REVERSEK){
	revbeam(b);
      }

      // Layer-KKR
      xmat(l, b, c);
      mmat(l, b, c);
      pmat(l, b, c);
      rflmat(l, b, c, j.general.dbl);
      if(c.nbulk2 > 0){
	rflmat2(l, b, c, j.general.dbl);
      }
      rfltot(l, c);
      
      switch(j.general.dis){
      case DIS_ATA:
      case DIS_CPA:
	xmat(l2, b, c2);
	mmat(l2, b, c2);
	pmat(l2, b, c2);
	rflmat(l2, b, c2, j.general.dbl);
	if(c2.nbulk2 > 0){
	  rflmat2(l2, b, c2, j.general.dbl);
	}
	rfltot(l2, c2);
	
	xmat(l3, b, c2);
	mmat(l3, b, c2);
	pmat(l3, b, c2);
	rflmat(l3, b, c2, j.general.dbl);
	if(c.nbulk2 > 0){
	  rflmat2(l3, b, c, j.general.dbl);
	}
	rfltot(l3, c2);
	break;
        case DIS_CPA3:
	xmat(l2, b, c2);
	mmat(l2, b, c2);
	pmat(l2, b, c2);
	rflmat(l2, b, c2, j.general.dbl);
	if(c2.nbulk2 > 0){
	  rflmat2(l2, b, c2, j.general.dbl);
	}
	rfltot(l2, c2);
	
	xmat(l3, b, c3);
	mmat(l3, b, c3);
	pmat(l3, b, c3);
	rflmat(l3, b, c3, j.general.dbl);
	if(c3.nbulk2 > 0){
	  rflmat2(l3, b, c3, j.general.dbl);
	}
	rfltot(l3, c3);

	xmat(l4, b, c2);
	mmat(l4, b, c2);
	pmat(l4, b, c2);
	rflmat(l4, b, c2, j.general.dbl);
	if(c.nbulk2 > 0){
	  rflmat2(l4, b, c, j.general.dbl);
	}
	rfltot(l4, c2);
	  break;
      default:
	break;
      }
      
      switch(j.general.dis){
      case DIS_ATA:
      case DIS_CPA:
	ldosdis(a, j, l, c, l2, c2, l3, l4, out, lflagw, ikval);
	break;
      case DIS_CPA3:	
        ldosdis3(a, j, l, c, l2, c2, l3, c3, l4, l5, out, lflagw, ikval);
	break;
      default:
	ldos(a, j, l, c, out);
	break;
      }
      
      if(lflagw & REVERSEK){
	revbeam(b);
      }
    }
    cout << endl;
  }
}
