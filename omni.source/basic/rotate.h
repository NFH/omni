// rotate.h
//
// enthaelt Prototypen der in rotate.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _ROTATE_
#define _ROTATE_

#include "job.h"
#include "crystal.h"
#include "cmatrix.h"

void WignerD(complexmatrix &R, double alpha, double beta, double gamma);
void rotate(complexmatrix &R, double alpha, double beta, double gamma);
void localrotmat(job &j, crystal &c, crystal &c2, crystal &c3);
void localrotmatph(job &j, actual &a, crystal &c, crystal &c2);

#endif
