//
// Adaptive grid method (2dim. case) for V-matrix integration
//
// 17/01/00 Juergen Henk
//

#ifndef _VINTEGRATE_
#define _VINTEGRATE_

void vmatmean(job &j, actual &a, state &s, state &s2, crystal &c, int mode);

#endif
