// idirac.C
//
// Dirac equation solver for irregular solutions
//
// Removed SIC treatment. SIC is now handled in dirac() (JH 2007)

#include <cmath>

#include "complex.h"
#include "cmatrix.h"
#include "function.h"
#include "constants.h"
#include "potential.h"
#include "waves.h"

// Unterprogramm zur Bestimmung der irregulaeren Loesung der Dirac-Gleichung
// ohne Kopplung (abs(mu) > l) und fuer den nichtmagnetischen Fall (mu := 0)
// p enthaelt das potential
// e ist die komplexe Energie [in Hartree]
// kp enthaelt kappa
// mu2 enthaelt 2*mu
// x enthaelt Faktor fuer Spin-Bahn Kopplung
// w enthaelt den Startwert und bei Rueckkehr die Wellenfunktion
// revb enthaelt das Vorzeichen von B
// itr enthaelt die Zahl der Iterationen in der innersten Schleife

void idirac(potential &p, complex e, int kp, int mu2, double x, wavefunction &w, double revb, int itr, int ninf, int nturn){
  
  waveelement	y[5], dy[5];
  waveelement	y1, y2;
  complex	vv, bb;
  complex	p12, p21;
  double	q11, q22;
  int		i, k;
  int		kp1;
  int		nm, kk;
  int		itrr;
  double	g, g1, r;
  double	exph, dr, x1;
  double	b1, b2;
  
  psic &pl = p.sic[0];
  
  // Einige konstante Werte
  exph = exp(p.h / itr);
  dr = -p.h / (3 * itr);
  nm = ninf - 1;
  x1 = 1 - x;
  kp1 = kp + 1;
  
  // Weitere konstante Werte
  b1 = mu2 / (2 * kp - 1.0) / CLIGHT;
  b2 = mu2 / (2 * kp + 1.0) / CLIGHT;
  
  // Korrektur
  b1 = b1 + x1 * (b2 - b1);
  
  // Berechne Startwerte fuer die Iteration
  g1 = (1 - x * x) * kp * kp1 + x * x1 * kp1;
  g = sqrt(fabs(kp * kp + x1 * kp1 - p.z * p.z / CLIGHT2));
  q11 = g + 1 - x * kp1;
  q22 = g - 1 + x * kp1;
  
  // Selbst startende Formel (rueckwaerts)
  r = pow(p.r(ninf-1), g);
  w.f1(nm) *= r;
  w.g1(nm) *= r;
  w.f2(nm) = 0;
  w.g2(nm) = 0;
  for(k = 0; k < 5; k++){
    y[k].f1 = w.f1(nm);
    y[k].g1 = w.g1(nm);
  }

  // Berechne die ersten drei Iterationen
  for(i = 0; i < 5; i++){
    r = p.r(ninf-1) * exph;
    for(k = 0; k < 5; k++){
      kk = nm - k / itr;
      r /= exph;
      // Interpoliere das Potential
      bb = pl.b(kk) + (pl.b(kk - 1) - pl.b(kk)) * (r - p.r(kk)) / (p.r(kk - 1) - p.r(kk));
      bb *= revb;
      vv = pl.v(kk) + (pl.v(kk - 1) - pl.v(kk)) * (r - p.r(kk)) / (p.r(kk - 1) - p.r(kk));
      p21 = -(e*r - vv + p.z) / CLIGHT;
      p12 = 2 * CLIGHT * r - p21;
      dy[k].f1 = dr * (q11 * y[k].f1 + (p12 + bb * b1) * y[k].g1);
      dy[k].g1 = dr * (q22 * y[k].g1 + (p21 + g1 / p12 - bb * b2) * y[k].f1);
    }
    y[1].f1 = y[0].f1 + (251 * dy[0].f1 + 646 * dy[1].f1 - 264 * dy[2].f1 + 106 * dy[3].f1 - 19 * dy[4].f1) / 240;
    y[2].f1 = y[0].f1 + (116 * dy[0].f1 + 496 * dy[1].f1 + 96 * dy[2].f1 + 16 * dy[3].f1 - 4 * dy[4].f1) / 120;
    y[3].f1 = y[0].f1 + (81 * dy[0].f1 + 306 * dy[1].f1 + 216 * dy[2].f1 + 126 * dy[3].f1 - 9 * dy[4].f1) / 80;
    y[4].f1 = y[0].f1 + (56 * dy[0].f1 + 256 * dy[1].f1 + 96 * dy[2].f1 + 256 * dy[3].f1 + 56 * dy[4].f1) / 60;
    y[1].g1 = y[0].g1 + (251 * dy[0].g1 + 646 * dy[1].g1 - 264 * dy[2].g1 + 106 * dy[3].g1 - 19 * dy[4].g1) / 240;
    y[2].g1 = y[0].g1 + (116 * dy[0].g1 + 496 * dy[1].g1 + 96 * dy[2].g1 + 16 * dy[3].g1 - 4 * dy[4].g1) / 120;
    y[3].g1 = y[0].g1 + (81 * dy[0].g1 + 306 * dy[1].g1 + 216 * dy[2].g1 + 126 * dy[3].g1 - 9 * dy[4].g1) / 80;
    y[4].g1 = y[0].g1 + (56 * dy[0].g1 + 256 * dy[1].g1 + 96 * dy[2].g1 + 256 * dy[3].g1 + 56 * dy[4].g1) / 60;
  }

  // Der Startwert fuer die weitere Iteration
  r *= exph;
  y1 = y[3];

  // Berechne nun die Iterationen bis zur naechsten Stuetzstelle
  switch(itr){
  case 1:
    w.f1(nm - 1) = y[1].f1;
    w.g1(nm - 1) = y[1].g1;
    w.f2(nm - 1) = 0;
    w.g2(nm - 1) = 0;
    w.f1(nm - 2) = y[2].f1;
    w.g1(nm - 2) = y[2].g1;
    w.f2(nm - 2) = 0;
    w.g2(nm - 2) = 0;
    w.f1(nm - 3) = y[3].f1;
    w.g1(nm - 3) = y[3].g1;
    w.f2(nm - 3) = 0;
    w.g2(nm - 3) = 0;

    // Es ist eine Iteration bis zur naechsten Stuetzstelle
    itrr = 1;

    // die letzte Stuetzstelle
    nm -= 3;
    break;
  case 2:
    w.f1(nm - 1) = y[2].f1;
    w.g1(nm - 1) = y[2].g1;
    w.f2(nm - 1) = 0;
    w.g2(nm - 1) = 0;
    // Es ist eine Iteration bis zur naechsten Stuetzstelle
    itrr = 1;
    // die letzte Stuetzstelle
    nm -= 1;
    break;
  case 3:
    w.f1(nm - 1) = y[3].f1;
    w.g1(nm - 1) = y[3].g1;
    w.f2(nm - 1) = 0;
    w.g2(nm - 1) = 0;
    // Es sind drei Iterationen bis zur naechsten Stuetzstelle
    itrr = 3;
    // die letzte Stuetzstelle
    nm -= 1;
    break;
  default:
    // Es sind noch itr - 3 Iterationen bis zur naechsten Stuetzstelle
    itrr = itr - 3;
    break;
  }
  // Hier beginnt die eigentliche Iteration ueber alle Stuetzstellen
  for(k = nm - 1; k >= nturn; k--){
    // Berechne dir itrr Iterationen ueber die Zwischenstuetzstellen
    for(i = 0; i < itrr; i++){
      // Prediktor
#ifndef ORIGINAL
      y2.f1.re = y1.f1.re + 6.875 * dy[3].f1.re - 7.375 * dy[2].f1.re + 4.625 * dy[1].f1.re - 1.125 * dy[0].f1.re;
      y2.g1.re = y1.g1.re + 6.875 * dy[3].g1.re - 7.375 * dy[2].g1.re + 4.625 * dy[1].g1.re - 1.125 * dy[0].g1.re;
      y2.f1.im = y1.f1.im + 6.875 * dy[3].f1.im - 7.375 * dy[2].f1.im + 4.625 * dy[1].f1.im - 1.125 * dy[0].f1.im;
      y2.g1.im = y1.g1.im + 6.875 * dy[3].g1.im - 7.375 * dy[2].g1.im + 4.625 * dy[1].g1.im - 1.125 * dy[0].g1.im;
#else
      y2.f1 = y1.f1 + 6.875 * dy[3].f1 - 7.375 * dy[2].f1 + 4.625 * dy[1].f1 - 1.125 * dy[0].f1;
      y2.g1 = y1.g1 + 6.875 * dy[3].g1 - 7.375 * dy[2].g1 + 4.625 * dy[1].g1 - 1.125 * dy[0].g1;
#endif /* ORIGINAL */
      r /= exph;
      // Interpoliere das Potential
      bb = pl.b(k) + (pl.b(k + 1) - pl.b(k)) * (r - p.r(k)) / (p.r(k + 1) - p.r(k));
      bb *= revb;
      vv = pl.v(k) + (pl.v(k + 1) - pl.v(k)) * (r - p.r(k)) / (p.r(k + 1) - p.r(k));
      p21 = -(e * r - vv + p.z) / CLIGHT;
      p12 = 2 * CLIGHT * r - p21;
      dy[0] = dy[1];
      dy[1] = dy[2];
      dy[2] = dy[3];
      dy[3].f1 = dr * (q11 * y2.f1 + (p12 + bb * b1) * y2.g1);
      dy[3].g1 = dr * (q22 * y2.g1 + (p21 + g1 / p12 - bb * b2) * y2.f1);
      // Korrektor
#ifndef ORIGINAL
      y2.f1.re = y1.f1.re + 1.125 * dy[3].f1.re + 2.375 * dy[2].f1.re - 0.625 * dy[1].f1.re + 0.125 * dy[0].f1.re;
      y2.g1.re = y1.g1.re + 1.125 * dy[3].g1.re + 2.375 * dy[2].g1.re - 0.625 * dy[1].g1.re + 0.125 * dy[0].g1.re;
      y2.f1.im = y1.f1.im + 1.125 * dy[3].f1.im + 2.375 * dy[2].f1.im - 0.625 * dy[1].f1.im + 0.125 * dy[0].f1.im;
      y2.g1.im = y1.g1.im + 1.125 * dy[3].g1.im + 2.375 * dy[2].g1.im - 0.625 * dy[1].g1.im + 0.125 * dy[0].g1.im;
#else
      y2.f1 = y1.f1 + 1.125 * dy[3].f1 + 2.375 * dy[2].f1 - 0.625 * dy[1].f1 + 0.125 * dy[0].f1;
      y2.g1 = y1.g1 + 1.125 * dy[3].g1 + 2.375 * dy[2].g1 - 0.625 * dy[1].g1 + 0.125 * dy[0].g1;
#endif /* ORIGINAL */
      dy[3].f1 = dr * (q11 * y2.f1 + (p12 + bb * b1) * y2.g1);
      dy[3].g1 = dr * (q22 * y2.g1 + (p21 + g1 / p12 - bb * b2) * y2.f1);
      y1 = y2;
    }
    w.f1(k) = y1.f1;
    w.g1(k) = y1.g1;
    w.f2(k) = 0;
    w.g2(k) = 0;
    // Der neue Radius
    r = p.r(k);
    // Nun sind es wieder itr Iterationen bis zur naechsten Stuetzstelle
    itrr = itr;
  }
  for(i = nturn; i < ninf; i++){
    r = pow(p.r(i), g);
    w.f1(i) /= r;
    w.g1(i) /= r;
  }
  for(i = 0; i < nturn; i++){
    w.f1(i) = 0;
    w.g1(i) = 0;
  }
  for(i = ninf; i < p.n; i++){
    w.f1(i) = 0;
    w.g1(i) = 0;
  }
}

// Unterprogramm zur Bestimmung der irregulaeren Loesung der Dirac-Gleichung
// mit Kopplung (abs(mu) < l)
// p enthaelt das potential
// e ist die komplexe Energie [in Hartree]
// kp enthaelt kappa
// mu2 enthaelt 2*mu
// x enthaelt Faktor fuer Spin-Bahn Kopplung
// w enthaelt den Startwert und bei Rueckkehr die Wellenfunktion
// revb enthaelt das Vorzeichen von B
// itr enthaelt die Zahl der Iterationen in der innersten Schleife

void idiracm(potential &p, complex e, int kp, int mu2, double x, wavefunction &w, double revb, int itr, int ninf, int nturn){
  waveelement	y[5], dy[5];
  waveelement	y1, y2;
  complex	vv, bb;
  complex	p12, p21;
  double	p11, p22;
  double	q12, q21;
  double	q11, q22;
  int		i, k;
  int		kp1;
  int		nm, kk;
  int		itrr;
  double	g, g1, g2, r;
  double	exph, dr, x1;
  double	b1, b2, b3, b4;

  psic &pl = p.sic[0];

  // Einige konstante Werte
  exph = exp(p.h / itr);
  dr = -p.h / (3 * itr);
  nm = ninf - 1;
  x1 = 1 - x;
  kp1 = kp + 1;

  // Weitere konstante Werte
  b1 = mu2 / (2 * kp - 1.0) / CLIGHT;
  b2 = mu2 / (2 * kp + 1.0) / CLIGHT;
  b3 = mu2 / (2 * kp + 3.0) / CLIGHT;
  b4 = sqrt(fabs(1 - mu2 * mu2 / ((2 * kp + 1.0) * (2 * kp + 1.0)))) / CLIGHT;

  // Korrektur
  b1 = b1 + x1 * (b2 - b1);
  b3 = b3 + x1 * (b2 - b3);

  // Berechne Startwerte fuer die Iteration
  q21 = -p.z / CLIGHT;
  q12 = -q21;
  //
  g1 = (1 - x * x) * kp * kp1 + x * x1 * kp1;
  g2 = (1 - x * x) * kp * kp1 - x * x1 * kp;
  g = sqrt(fabs(kp * kp + x1 * kp1 - q12 * q12));
  q11 = g + 1 - x * kp1;
  q22 = g - 1 + x * kp1;
  p11 = g + 1 + x * kp;
  p22 = g - 1 - x * kp;

  // Selbst startende Formel (rueckwaerts)
  r = pow(p.r(ninf-1), g);
  w.f1(nm) *= r;
  w.g1(nm) *= r;
  w.f2(nm) *= r;
  w.g2(nm) *= r;
  for(k = 0; k < 5; k++){
    y[k].f1 = w.f1(nm);
    y[k].g1 = w.g1(nm);
    y[k].f2 = w.f2(nm);
    y[k].g2 = w.g2(nm);
  }

  // Berechne die ersten drei Iterationen
  for(i = 0; i < 5; i++){
    r = p.r(ninf-1) * exph;
    for(k = 0; k < 5; k++){
      kk = nm - k / itr;
      r /= exph;
      // Interpoliere das Potential
      bb = pl.b(kk) + (pl.b(kk - 1) - pl.b(kk)) * (r - p.r(kk)) / (p.r(kk - 1) - p.r(kk));
      bb *= revb;
      vv = pl.v(kk) + (pl.v(kk - 1) - pl.v(kk)) * (r - p.r(kk)) / (p.r(kk - 1) - p.r(kk));
      p21 = -(e*r - vv + p.z) / CLIGHT;
      p12 = 2 * CLIGHT * r - p21;
      dy[k].f1 = dr * (q11 * y[k].f1 + (p12 + bb * b1) * y[k].g1 + x1 * bb * b4 * y[k].g2);
      dy[k].g1 = dr * (q22 * y[k].g1 + (p21 + g1 / p12 - bb * b2) * y[k].f1 - bb * b4 * y[k].f2);
      dy[k].f2 = dr * (p11 * y[k].f2 + (p12 - bb * b3) * y[k].g2 + x1 * bb * b4 * y[k].g1);
      dy[k].g2 = dr * (p22 * y[k].g2 + (p21 + g2 / p12 + bb * b2) * y[k].f2 - bb * b4 * y[k].f1);
    }
    y[1].f1 = y[0].f1 + (251 * dy[0].f1 + 646 * dy[1].f1 - 264 * dy[2].f1 + 106 * dy[3].f1 - 19 * dy[4].f1) / 240;
    y[2].f1 = y[0].f1 + (116 * dy[0].f1 + 496 * dy[1].f1 + 96 * dy[2].f1 + 16 * dy[3].f1 - 4 * dy[4].f1) / 120;
    y[3].f1 = y[0].f1 + (81 * dy[0].f1 + 306 * dy[1].f1 + 216 * dy[2].f1 + 126 * dy[3].f1 - 9 * dy[4].f1) / 80;
    y[4].f1 = y[0].f1 + (56 * dy[0].f1 + 256 * dy[1].f1 + 96 * dy[2].f1 + 256 * dy[3].f1 + 56 * dy[4].f1) / 60;
    y[1].g1 = y[0].g1 + (251 * dy[0].g1 + 646 * dy[1].g1 - 264 * dy[2].g1 + 106 * dy[3].g1 - 19 * dy[4].g1) / 240;
    y[2].g1 = y[0].g1 + (116 * dy[0].g1 + 496 * dy[1].g1 + 96 * dy[2].g1 + 16 * dy[3].g1 - 4 * dy[4].g1) / 120;
    y[3].g1 = y[0].g1 + (81 * dy[0].g1 + 306 * dy[1].g1 + 216 * dy[2].g1 + 126 * dy[3].g1 - 9 * dy[4].g1) / 80;
    y[4].g1 = y[0].g1 + (56 * dy[0].g1 + 256 * dy[1].g1 + 96 * dy[2].g1 + 256 * dy[3].g1 + 56 * dy[4].g1) / 60;
    y[1].f2 = y[0].f2 + (251 * dy[0].f2 + 646 * dy[1].f2 - 264 * dy[2].f2 + 106 * dy[3].f2 - 19 * dy[4].f2) / 240;
    y[2].f2 = y[0].f2 + (116 * dy[0].f2 + 496 * dy[1].f2 + 96 * dy[2].f2 + 16 * dy[3].f2 - 4 * dy[4].f2) / 120;
    y[3].f2 = y[0].f2 + (81 * dy[0].f2 + 306 * dy[1].f2 + 216 * dy[2].f2 + 126 * dy[3].f2 - 9 * dy[4].f2) / 80;
    y[4].f2 = y[0].f2 + (56 * dy[0].f2 + 256 * dy[1].f2 + 96 * dy[2].f2 + 256 * dy[3].f2 + 56 * dy[4].f2) / 60;
    y[1].g2 = y[0].g2 + (251 * dy[0].g2 + 646 * dy[1].g2 - 264 * dy[2].g2 + 106 * dy[3].g2 - 19 * dy[4].g2) / 240;
    y[2].g2 = y[0].g2 + (116 * dy[0].g2 + 496 * dy[1].g2 + 96 * dy[2].g2 + 16 * dy[3].g2 - 4 * dy[4].g2) / 120;
    y[3].g2 = y[0].g2 + (81 * dy[0].g2 + 306 * dy[1].g2 + 216 * dy[2].g2 + 126 * dy[3].g2 - 9 * dy[4].g2) / 80;
    y[4].g2 = y[0].g2 + (56 * dy[0].g2 + 256 * dy[1].g2 + 96 * dy[2].g2 + 256 * dy[3].g2 + 56 * dy[4].g2) / 60;
  }

  // Der Startwert fuer die weitere Iteration
  r *= exph;
  y1 = y[3];

  // Berechne nun die Iterationen bis zur naechsten Stuetzstelle
  switch(itr){
  case 1:
    w.f1(nm - 1) = y[1].f1;
    w.g1(nm - 1) = y[1].g1;
    w.f2(nm - 1) = y[1].f2;
    w.g2(nm - 1) = y[1].g2;
    w.f1(nm - 2) = y[2].f1;
    w.g1(nm - 2) = y[2].g1;
    w.f2(nm - 2) = y[2].f2;
    w.g2(nm - 2) = y[2].g2;
    w.f1(nm - 3) = y[3].f1;
    w.g1(nm - 3) = y[3].g1;
    w.f2(nm - 3) = y[3].f2;
    w.g2(nm - 3) = y[3].g2;
    // Es ist eine Iteration bis zur naechsten Stuetzstelle
    itrr = 1;
    // die letzte Stuetzstelle
    nm -= 3;
    break;
  case 2:
    w.f1(nm - 1) = y[2].f1;
    w.g1(nm - 1) = y[2].g1;
    w.f2(nm - 1) = y[2].f2;
    w.g2(nm - 1) = y[2].g2;
    // Es ist eine Iteration bis zur naechsten Stuetzstelle
    itrr = 1;
    // die letzte Stuetzstelle
    nm -= 1;
    break;
  case 3:
    w.f1(nm - 1) = y[3].f1;
    w.g1(nm - 1) = y[3].g1;
    w.f2(nm - 1) = y[3].f2;
    w.g2(nm - 1) = y[3].g2;
    // Es sind drei Iterationen bis zur naechsten Stuetzstelle
    itrr = 3;
    // die letzte Stuetzstelle
    nm -= 1;
    break;
  default:
    // Es sind noch itr - 3 Iterationen bis zur naechsten Stuetzstelle
    itrr = itr - 3;
    break;
  }
  // Hier beginnt die eigentliche Iteration ueber alle Stuetzstellen
  for(k = nm - 1; k >= 0; k--){
    // Berechne dir itrr Iterationen ueber die Zwischenstuetzstellen
    for(i = 0; i < itrr; i++){
      // Prediktor
#ifndef ORIGINAL
      y2.f1.re = y1.f1.re + 6.875 * dy[3].f1.re - 7.375 * dy[2].f1.re + 4.625 * dy[1].f1.re - 1.125 * dy[0].f1.re;
      y2.g1.re = y1.g1.re + 6.875 * dy[3].g1.re - 7.375 * dy[2].g1.re + 4.625 * dy[1].g1.re - 1.125 * dy[0].g1.re;
      y2.f2.re = y1.f2.re + 6.875 * dy[3].f2.re - 7.375 * dy[2].f2.re + 4.625 * dy[1].f2.re - 1.125 * dy[0].f2.re;
      y2.g2.re = y1.g2.re + 6.875 * dy[3].g2.re - 7.375 * dy[2].g2.re + 4.625 * dy[1].g2.re - 1.125 * dy[0].g2.re;
      y2.f1.im = y1.f1.im + 6.875 * dy[3].f1.im - 7.375 * dy[2].f1.im + 4.625 * dy[1].f1.im - 1.125 * dy[0].f1.im;
      y2.g1.im = y1.g1.im + 6.875 * dy[3].g1.im - 7.375 * dy[2].g1.im + 4.625 * dy[1].g1.im - 1.125 * dy[0].g1.im;
      y2.f2.im = y1.f2.im + 6.875 * dy[3].f2.im - 7.375 * dy[2].f2.im + 4.625 * dy[1].f2.im - 1.125 * dy[0].f2.im;
      y2.g2.im = y1.g2.im + 6.875 * dy[3].g2.im - 7.375 * dy[2].g2.im + 4.625 * dy[1].g2.im - 1.125 * dy[0].g2.im;
#else
      y2.f1 = y1.f1 + 6.875 * dy[3].f1 - 7.375 * dy[2].f1 + 4.625 * dy[1].f1 - 1.125 * dy[0].f1;
      y2.g1 = y1.g1 + 6.875 * dy[3].g1 - 7.375 * dy[2].g1 + 4.625 * dy[1].g1 - 1.125 * dy[0].g1;
      y2.f2 = y1.f2 + 6.875 * dy[3].f2 - 7.375 * dy[2].f2 + 4.625 * dy[1].f2 - 1.125 * dy[0].f2;
      y2.g2 = y1.g2 + 6.875 * dy[3].g2 - 7.375 * dy[2].g2 + 4.625 * dy[1].g2 - 1.125 * dy[0].g2;
#endif /* ORIGINAL */
      r /= exph;
      // Interpoliere das Potential
      bb = pl.b(k) + (pl.b(k + 1) - pl.b(k)) * (r - p.r(k)) / (p.r(k + 1) - p.r(k));
      bb *= revb;
      vv = pl.v(k) + (pl.v(k + 1) - pl.v(k)) * (r - p.r(k)) / (p.r(k + 1) - p.r(k));
      p21 = -(e * r - vv + p.z) / CLIGHT;
      p12 = 2 * CLIGHT * r - p21;
      dy[0] = dy[1];
      dy[1] = dy[2];
      dy[2] = dy[3];
      dy[3].f1 = dr * (q11 * y2.f1 + (p12 + bb * b1) * y2.g1 + x1 * bb * b4 * y2.g2);
      dy[3].g1 = dr * (q22 * y2.g1 + (p21 + g1 / p12 - bb * b2) * y2.f1 - bb * b4 * y2.f2);
      dy[3].f2 = dr * (p11 * y2.f2 + (p12 - bb * b3) * y2.g2 + x1 * bb * b4 * y2.g1);
      dy[3].g2 = dr * (p22 * y2.g2 + (p21 + g2 / p12 + bb * b2) * y2.f2 - bb * b4 * y2.f1);
      // Korrektor
#ifndef ORIGINAL
      y2.f1.re = y1.f1.re + 1.125 * dy[3].f1.re + 2.375 * dy[2].f1.re - 0.625 * dy[1].f1.re + 0.125 * dy[0].f1.re;
      y2.g1.re = y1.g1.re + 1.125 * dy[3].g1.re + 2.375 * dy[2].g1.re - 0.625 * dy[1].g1.re + 0.125 * dy[0].g1.re;
      y2.f2.re = y1.f2.re + 1.125 * dy[3].f2.re + 2.375 * dy[2].f2.re - 0.625 * dy[1].f2.re + 0.125 * dy[0].f2.re;
      y2.g2.re = y1.g2.re + 1.125 * dy[3].g2.re + 2.375 * dy[2].g2.re - 0.625 * dy[1].g2.re + 0.125 * dy[0].g2.re;
      y2.f1.im = y1.f1.im + 1.125 * dy[3].f1.im + 2.375 * dy[2].f1.im - 0.625 * dy[1].f1.im + 0.125 * dy[0].f1.im;
      y2.g1.im = y1.g1.im + 1.125 * dy[3].g1.im + 2.375 * dy[2].g1.im - 0.625 * dy[1].g1.im + 0.125 * dy[0].g1.im;
      y2.f2.im = y1.f2.im + 1.125 * dy[3].f2.im + 2.375 * dy[2].f2.im - 0.625 * dy[1].f2.im + 0.125 * dy[0].f2.im;
      y2.g2.im = y1.g2.im + 1.125 * dy[3].g2.im + 2.375 * dy[2].g2.im - 0.625 * dy[1].g2.im + 0.125 * dy[0].g2.im;
#else
      y2.f1 = y1.f1 + 1.125 * dy[3].f1 + 2.375 * dy[2].f1 - 0.625 * dy[1].f1 + 0.125 * dy[0].f1;
      y2.g1 = y1.g1 + 1.125 * dy[3].g1 + 2.375 * dy[2].g1 - 0.625 * dy[1].g1 + 0.125 * dy[0].g1;
      y2.f2 = y1.f2 + 1.125 * dy[3].f2 + 2.375 * dy[2].f2 - 0.625 * dy[1].f2 + 0.125 * dy[0].f2;
      y2.g2 = y1.g2 + 1.125 * dy[3].g2 + 2.375 * dy[2].g2 - 0.625 * dy[1].g2 + 0.125 * dy[0].g2;
#endif /* ORIGINAL */
      dy[3].f1 = dr * (q11 * y2.f1 + (p12 + bb * b1) * y2.g1 + x1 * bb * b4 * y2.g2);
      dy[3].g1 = dr * (q22 * y2.g1 + (p21 + g1 / p12 - bb * b2) * y2.f1 - bb * b4 * y2.f2);
      dy[3].f2 = dr * (p11 * y2.f2 + (p12 - bb * b3) * y2.g2 + x1 * bb * b4 * y2.g1);
      dy[3].g2 = dr * (p22 * y2.g2 + (p21 + g2 / p12 + bb * b2) * y2.f2 - bb * b4 * y2.f1);
      y1 = y2;
    }
    w.f1(k) = y1.f1;
    w.g1(k) = y1.g1;
    w.f2(k) = y1.f2;
    w.g2(k) = y1.g2;
    // Der neue Radius
    r = p.r(k);
    // Nun sind es wieder itr Iterationen bis zur naechsten Stuetzstelle
    itrr = itr;
  }
  for(i = 0; i < ninf; i++){
    r = pow(p.r(i), g);
    w.f1(i) /= r;
    w.g1(i) /= r;
    w.f2(i) /= r;
    w.g2(i) /= r;
  }
  for(i = 0; i < nturn; i++){
    w.f1(i) = 0;
    w.g1(i) = 0;
    w.f2(i) = 0;
    w.g2(i) = 0;
  }
  for(i = ninf; i < p.n; i++){
    w.f1(i) = 0;
    w.g1(i) = 0;
    w.f2(i) = 0;
    w.g2(i) = 0;
  }
}
