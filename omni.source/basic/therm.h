// therm.h
//
// contains prototypes of functions defined in therm.C
//

#ifndef _THERM_
#define _THERM_

#include "potential.h"
#include "job.h"
#include "crystal.h"

void therm(complexmatrix &T, double a, complex e);
void tempcorr(job &j, crystal &c, crystal &c2, crystal &c3);

#endif
