// electron-phonon.h
//
// Electron-phonon coupling
//
// Electron self-energy due to electron-phonon coupling
complex electron_phonon_selfenergy(double e, double kt, double hbar_omega_max, double mue, double lambda);
