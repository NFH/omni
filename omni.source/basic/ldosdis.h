// ldosdis.h
//
// enthaelt Prototypen der in ldosdis.C definierten Funktionen
//

#ifndef _LDOSDIS_
#define _LDOSDIS_

#include <iostream>

#include "job.h"
#include "state.h"
#include "crystal.h"

// For ATA and CPA
void ldosdis(actual &a, job &j, state &s, crystal &c, state &s2, crystal &c2, state &s3, state &s4,\
	     std::ostream &os, int lflagw, int ikval, std::ostream &out=std::cout);

// For DLM
void ldosdisdlm(actual &a, job &j, state &s, crystal &c, state &s3, std::ostream &os, int lflagw, int ikval, std::ostream &out=std::cout);

// For CPA3
void ldosdis3(actual &a, job &j, state &s, crystal &c, state &s2, crystal &c2,\
	      state &s3, crystal &c3, state &s4, state &s5, std::ostream &os, int lflagw, int ikval, std::ostream &out=std::cout);

void compdosdis(actual &a, job &j, state &s, crystal &c, state &s2, crystal &c2, state &s3,\
		std::ostream &os, int lflagw, std::ostream &out=std::cout);

#endif
