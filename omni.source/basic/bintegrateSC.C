//
// bintegrateSC.C
//
// Based on tintegrate0() - Special pionts
// spring 2007 Peter Bose
//

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

#include "job.h"
#include "state.h"
#include "crystal.h"
#include "spec.h"

#include "blochSC.h"

#include "actual.h"
#include "constants.h"
#include "vector2.h"
#include "clebgn.h"
#include "beam.h"
#include "order.h"
#include "mmat.h"
#include "green.h"

#include "cmatrix.h"
#include "csmatrix.h"
#include "intmath.h"

#define VERBOSITY 1

using namespace std;


// Calculation of the k-parallel averaged transmission via the
// supercell method
void tintegrate0sc(job &j, actual &a, state &l_UC, state &r_UC, state &l_SC, state &r_SC, crystal &c_UC, crystal &c_SC, double &TotalSumTpp_spec, double &TotalSumTpm_spec, double &TotalSumTmp_spec, double &TotalSumTmm_spec, double &TotalSumTpp_diff, double &TotalSumTpm_diff, double &TotalSumTmp_diff, double &TotalSumTmm_diff, int lflagw, ofstream &out){

  unsigned int nk;
  int beamSCindex, beamSCqindex, n, m, block, Vi_size, N = c_SC.scp1, M = c_SC.scp2; 

  double SumTpp_spec, SumTpm_spec, SumTmp_spec, SumTmm_spec; // specular contributions
  double SumTpp_diff, SumTpm_diff, SumTmp_diff, SumTmm_diff; // diffusive contributions
  
  beamset  b_UC; // Beamset of the original unitcell. 
  beamset  b_SC; // Beamset of the supercell.
  beamset b_SCq; // Beamset of the respective shifted unitcell beamset.

  vector2 q;
  vector3 rR, rL;

  // LOOP over k_{||} values defined in k.mesh or kaver.mesh
  for(nk = 0; nk < j.kaverage.klmean.size(); nk++){
    getkparmean(j, a, nk);
    
    // Set which beamset (g-vectors) are to be used.
    get_beamset(j, a.e.re, j.general.eg, a.q, c_UC, b_UC); 

    // Generate a superbeamset b_SC that will contain at the end all
    // g-vectors for the more finely woven reciprocal mesh of the SC
    // BZ.
    b_SC = b_UC;                        // Use of copyconstructor
    b_SC.nbeam = N * M * b_UC.nbeam;    // Calculate new number of beams in the SC BZ
    delete[] b_SC.vec;                  // Delete old vectorfield with b_UC.nbeam beams (g-vectors)
    b_SC.vec = new vector2[b_SC.nbeam]; // Allocate new vectorfield for new number of beams
    
    // Every shift about a reciprocal supercell vector q, corresponds
    // to a certain block within the diagonal eigenvectors and -values
    // matrices. These blocks (next matrices denoted by Vi) have all
    // equal sizes of Vi_size = 2*2*b_UC.nbeam.
    Vi_size  = 2*2*b_UC.nbeam;

    // Set size for supermatrices that will contain eigenvectors and
    // eigenvalues of all blochwaves for neighboured supercell BZs at
    // equivalent k_{||}-points, but at different ones within the
    // unitcell BZ. The prefactors 2*2* arise from the distinction of
    // bloch waves traveling in + and - direction, plus the allowance
    // of spin up and down.  //l_SC.E.setsize(2*2*b_SC.nbeam) = 0; //
    l_SC.Esc.setsize(N * M, N * M, Vi_size, Vi_size);
    r_SC.Esc.setsize(N * M, N * M, Vi_size, Vi_size);
    l_SC.Vsc.setsize(N * M, N * M, Vi_size, Vi_size);
    r_SC.Vsc.setsize(N * M, N * M, Vi_size, Vi_size);

    // LOOP over all shifts of reciprocal SC lattice vectors.
     for(n = 0; n < N; n++){
      for(m = 0; m < M; m++){
        // block counts the index of the corresponding block within
        // the diagonal superblockmatrix
        block = (m + n * M);
        	      
        // Generate a beamset copy that will later contain g-vectors
        // of the UC beamset shifted about a reciprocal lattice vector
        // q of the supercell reciprocal lattice.
        b_SCq = b_UC; // Use of copyconstructor 
        
	// Reciprocal shifting vector q
	q = n * c_SC.b1 + m * c_SC.b2;
		
	// LOOP over all g-vectors of the current beamset SCq
        for(beamSCqindex = 0; beamSCqindex < b_SCq.nbeam; beamSCqindex++){
	  // Determine the corresponding index for the superbeamset
	  beamSCindex = beamSCqindex + (m + n * M) * b_SCq.nbeam;
	  
	  // Fill current beamset and superbeamset with g-vectors of
	  // the unitcell reciprocal lattice plus a shift about q.
          b_SCq.vec[beamSCqindex] = b_UC.vec[beamSCqindex] + q;
	  b_SC.vec[beamSCindex]   = b_UC.vec[beamSCqindex] + q;
          
#if(VERBOSITY >= 2)
           // cout of old and new vectors: g_UC + q = g_SC
	   cout << beamSCindex << " (";
	   cout << setw (9) << setprecision (6) << b_UC.vec[beamSCqindex].x << ","; 
           cout << setw (9) << setprecision (6) << b_UC.vec[beamSCqindex].y << ")";
	   cout << " + (";
	   cout << setw (9) << setprecision (6) << q.x << ","; 
           cout << setw (9) << setprecision (6) << q.y << ") ";
	   cout << "= (";
	   cout << setw (9) << setprecision (6) << b_SCq.vec[beamSCqindex].x << ","; 
           cout << setw (9) << setprecision (6) << b_SCq.vec[beamSCqindex].y << ")";
           	  
	   // Every g-vector with index 0 within b_SCq corresponds to
	   // the new shifted k_{||}-point (k_{||, new} = k_{||, old}
	   // + q ). The same k-point is indexed within the
	   // superbeamset b_SC by the index: (m+n*M)*b_UC.nbeam .
	   cout << " " << setw (3) << beamSCqindex << " (";
           cout << setw (9) << setprecision (6) << b_SCq.vec[0].x;
           cout << "," << setw (9) << setprecision (6) << b_SCq.vec[0].y << ") " << endl;
#endif	   	  
        }

  	// Region L, unit cell, bulk-side
        // Layer KKR for l
	xmat (l_UC, b_SCq, c_UC);
        mmat (l_UC, b_SCq, c_UC);
        pmat (l_UC, b_SCq, c_UC);
        rflmat (l_UC, b_SCq, c_UC, j.general.dbl);
	
	// Write Bloch coefficients (eigenvectors) and eigenvalues
	// into the respective blocks of the supermatrices V and E.
        l_SC.Vsc(block, block) = l_UC.V;
	l_SC.Esc(block, block) = l_UC.E;

        // Region R, surface-side
	// layer KKR for r 
        xmat (r_UC, b_SCq, c_UC);
        mmat (r_UC, b_SCq, c_UC);
        pmat (r_UC, b_SCq, c_UC);
        rflmat (r_UC, b_SCq, c_UC, j.general.dbl);
        if(c_UC.nbulk2 > 0){
          rflmat2(r_UC, b_SCq, c_UC, j.general.dbl);
	}
	
	// Write Bloch coefficients (eigenvectors) and eigenvalues
	// into the respective blocks of the supermatrices V and E.
        r_SC.Vsc(block, block) = r_UC.V;
	r_SC.Esc(block, block) = r_UC.E;
      }
    }
                 
    // Bloch-wave transmission
    blochSC(a, j, l_SC, r_SC, b_SC, c_SC, out, SumTpp_spec, SumTpm_spec, SumTmp_spec, SumTmm_spec, SumTpp_diff, SumTpm_diff, SumTmp_diff, SumTmm_diff); 
    
    // Sum the weighted results
    TotalSumTpp_spec += a.w * SumTpp_spec;
    TotalSumTpm_spec += a.w * SumTpm_spec;
    TotalSumTmp_spec += a.w * SumTmp_spec;
    TotalSumTmm_spec += a.w * SumTmm_spec;
    TotalSumTpp_diff += a.w * SumTpp_diff;
    TotalSumTpm_diff += a.w * SumTpm_diff;
    TotalSumTmp_diff += a.w * SumTmp_diff;
    TotalSumTmm_diff += a.w * SumTmm_diff;
    cout << endl;
    
    // Was k-parallel rotated?
    //if (lflagw & REVERSEK){
    //  revbeam(b_UC); }
    
  }
}

// The driver function
void bwtmeanSC(job &j, actual &a, state &l, state &r, state &l2, state &r2, crystal &c, crystal &c2, double &TotalSumTpp_spec, double &TotalSumTpm_spec, double &TotalSumTmp_spec, double &TotalSumTmm_spec, double &TotalSumTpp_diff, double &TotalSumTpm_diff, double &TotalSumTmp_diff, double &TotalSumTmm_diff, int lflagw, ofstream &out){

  // Calculate all the things...
  switch(j.kaverage.kmeanmode){
  case KAVER_SP:
    tintegrate0sc(j, a, l, r, l2, r2, c, c2, TotalSumTpp_spec, TotalSumTpm_spec, TotalSumTmp_spec, TotalSumTmm_spec, TotalSumTpp_diff, TotalSumTpm_diff, TotalSumTmp_diff, TotalSumTmm_diff, lflagw, out);
    break;
  default:
    cout << "ERROR from bwtmeanSC(): Wrong k-avering mode. Use special points." << endl;
    myexit(1);
    break;
  }
}

#undef VERBOSITY
