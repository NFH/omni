// scatter.h
//
// enthaelt Prototypen der in scatter.C definierten Funktionen
//
// Autor: 1999 Dipl. Phys. Thomas Scheunemann
//        Gerhard Mercator Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _SCATTER_
#define _SCATTER_

#include "job.h"
#include "state.h"
#include "crystal.h"

void scatter(actual &a, state &u, crystal &c, std::ostream &os, std::ostream &out=std::cout);

#endif
