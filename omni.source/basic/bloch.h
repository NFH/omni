// bloch.h
//
// Function for Bloch-wave transmission
//

#ifndef _BLOCH_
#define _BLOCH_

#include <iostream>

#include "job.h"
#include "beam.h"
#include "crystal.h"
#include "state.h"

void bloch(actual &a, job &j, state &sl, state &sr, beamset &b, crystal &c, std::ostream &os, double &SumTpp, double &SumTpm, double &SumTmp, double &SumTmm, double &Errp, double &Errm, double &FanoFp, double &FanoFm);

#endif
