// dm.h
//
// enthaelt Prototypen der in dm.C definierten Funktionen
//

#ifndef _DM_
#define _DM_

#include <iostream>

#include "job.h"
#include "state.h"
#include "crystal.h"

#define REF_MAG_X 0
#define REF_MAG_Y 1
#define REF_MAG_Z 2


void check_gs(job &j, crystal &c);
void set_gs_mag(job &j, crystal &c, int &ref_mag);
void dmrs(job &j, state &sdis, state &s, crystal &c, std::ostream &os, int& ref_mag);

void tmat_dt(complexmatrix &t_dt, complexmatrix &t_0, double theta, double phi);
void tmat_dp(complexmatrix &t_dp, complexmatrix &t_0, double theta, double phi);

#endif
