// phase.C
//
// Calculation of phase shifts
//
#include <iostream>
#include <iomanip>
#include <cmath>

#include "complex.h"
#include "cmatrix.h"
#include "constants.h"
#include "representation.h"
#include "sfunc.h"

#include "job.h"
#include "state.h"
#include "crystal.h"

#include "clebgn.h"

// Lower limit four output
#define LIMIT 0.001

using namespace std;


// Decompose a matrix into spin-up and down parts (no SOC)
void decompose_mat_spin_phase(complexmatrix &T, complexmatrix &Tuu, complexmatrix &Tdd){

  int km, kmp, iup, idn, ipup, ipdn;

  // Initialize
  Tuu = 0;
  Tdd = 0;

  // Transformation
  for(km = 0; km < kmmax; km++){
    // Index for spin-up part
    iup = kmlm1[km];
    // Index for spin-dn part
    idn = kmlm2[km];

    for(kmp = 0; kmp < kmmax; kmp++){
      // Index for spin-up part
      ipup = kmlm1[kmp];
      // Index for spin-dn part
      ipdn = kmlm2[kmp];

      Tuu(iup, ipup) += cleb1[km] * T(km, kmp) * cleb1[kmp];
      Tdd(idn, ipdn) += cleb2[km] * T(km, kmp) * cleb2[kmp];
    }
  }

}


void phase(actual &a, state &s, crystal &c, ostream &os){

  int atom, slab, lm, lmp;

  complex tuu, tdd, puu, pdd;

  complexmatrix T, Tuu, Tdd;

  cout << "MESSAGE from phase(): Phase shifts..." << endl;

  // Initialize the spin-dependent matrices
  Tuu.setsize(lmmax, lmmax);
  Tdd.setsize(lmmax, lmmax);


  // For all slabs
  for(slab = 0; slab < c.nslab; slab++){
    sslab &ss = s.slab[slab];
    cslab &cs = c.slab[slab];

    // For all atoms of this slab
    for(atom = 0; atom < cs.natom; atom++){
      catom &ca  = cs.atom[atom];

      // Pickout the t-matrix and rotate it into the local frame
      // z-axis = magnetization direction
      T =  ca.R * ss.T(atom, atom) * herm(ca.R);

      // Transform the t-matrix from omni to Durham representation
      omni2durham_t(T, ss.k);

      // Decompose into spin parts (kappa-mu -> l m s)
      decompose_mat_spin_phase(T, Tuu, Tdd);

      // For all elements of the spin-resolved t-matrices
      for(lm = 0; lm < lmmax; lm++){
	for(lmp = 0; lmp < lmmax; lmp++){
	  // Pick out an element 
	  tuu = Tuu(lm ,lmp);
	  tdd = Tdd(lm ,lmp);

	  // Proceed if tuu or tdd are large enough
	  if((abs(tuu) >= LIMIT) || (abs(tdd) >= LIMIT)){
	    // Phase shift
	    // t = - 1/k * sin p * exp(i p)
	    puu = cmplx(0, 0.5) * log(-2.0 * ss.k * tuu - cmplx(0, 1));
	    pdd = cmplx(0, 0.5) * log(-2.0 * ss.k * tdd - cmplx(0, 1));

	    // Output
	    cout << setw(8) << setprecision(3) << a.e.re * HART;
	    cout << setw(8) << setprecision(3) << a.e.im * HART;
	    cout << setw(7) << setprecision(2) << a.theta * 180 / M_PI;
	    cout << setw(7) << setprecision(2) << a.phi * 180 / M_PI;
	    cout << setw(3) << slab;
	    cout << setw(3) << atom;
	    cout << setw(3) << lm;
	    cout << setw(3) << lmp;
	    cout << setw(14) << setprecision(8) << tuu;
	    cout << setw(14) << setprecision(8) << puu;
	    cout << setw(14) << setprecision(8) << tdd;
	    cout << setw(14) << setprecision(8) << pdd << endl;

	    os << setw(10) << setprecision(5) << a.e.re * HART;
	    os << setw(10) << setprecision(5) << a.e.im * HART;
	    os << setw(10) << setprecision(5) << a.theta * 180 / M_PI;
	    os << setw(10) << setprecision(5) << a.phi * 180 / M_PI;
	    os << setw(3) << slab;
	    os << setw(3) << atom;
	    os << setw(3) << lm;
	    os << setw(3) << lmp;
	    os << setw(14) << setprecision(8) << real(tuu);
	    os << setw(14) << setprecision(8) << imag(tuu);
	    os << setw(14) << setprecision(8) << real(puu);
	    os << setw(14) << setprecision(8) << imag(puu);
	    os << setw(14) << setprecision(8) << real(tdd);
	    os << setw(14) << setprecision(8) << imag(tdd);
	    os << setw(14) << setprecision(8) << real(pdd);
	    os << setw(14) << setprecision(8) << imag(pdd) << endl;
	  }
	}
      }
    }
  }

  cout << "MESSAGE from phase(): Done." << endl;

}

#undef LIMIT

