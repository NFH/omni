// emiss.h
//
// enthaelt Prototypen der in emiss.C definierten Funktionen
//

#ifndef _EMISS_
#define _EMISS_

#include <iostream>

#include "job.h"
#include "state.h"
#include "crystal.h"

void emiss(actual &a, job &j, state &l, state &u, crystal &c, std::ostream &os, std::ostream &out=std::cout);
void emisscoredis(actual &a, job &j, state &l, state &u, state &l2, std::ostream &os, std::ostream &out=std::cout);
void emissdis(actual &a, job &j, state &l, state &l2, state &l3, state &l4, state &u, state &u2, state &u3, \
	      crystal &c, crystal &c2, std::ostream &os, std::ostream &out=std::cout);
void emiss_time(actual &a, job &j, state &l, state &ul, state &ur, crystal &c, std::ostream &os, std::ostream &out=std::cout);

#endif
