// mmat.h
//

#ifndef _MMAT_
#define _MMAT_

#include "state.h"
#include "crystal.h"
#include "beam.h"
#include "job.h"

void tmat(const job &j, state &s, const crystal &c, double x, double t, int pottype, int calctype);
void dmat(actual &a, state &l, state &u, crystal &c);
void dmat_time(actual &a, state &l, state &ul, state &ur, crystal &c);
void rmat(state &s, crystal &c);
void imat(state &s, crystal &c);
void xmat(state &s, beamset &b, crystal &c);
void mmat(state &s, beamset &b, crystal &c);
void pmat(state &s, beamset &b, crystal &c);
void rflmat(state &s, beamset &b, crystal &c, int dbl, std::ostream &out=std::cout);
void rflmat2(state &s, beamset &b, crystal &c, int dbl, std::ostream &out=std::cout);
void rfltot(state &s, crystal &c);
void rfltot2(state &s, crystal &c);

#endif
