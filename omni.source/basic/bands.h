// bands.h
//
// enthaelt Prototypen der in bands.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _BANDS_
#define _BANDS_

#include "job.h"
#include "state.h"
#include "crystal.h"
#include "beam.h"

void bands(actual &a, job &j, state &s, beamset &b, crystal &c, std::ostream &os);


#endif
