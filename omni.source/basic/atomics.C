// atomics.C
//
// Functions for reading potential files
//

#include <iostream>

#include <iomanip>
#include <fstream>
#include <cmath>

#include "platform.h"

#include "clebgn.h"
#include "constants.h"

#include "job.h"
#include "vector2.h"
#include "potential.h"
#include "sfunc.h"
#include "spec.h"
#include "table.h"

#include "function.h"
#include "cmatfunc.h"
#include "dmatrix.h"

using namespace std;

// Check the quantum numbers for SIC potentials For SIC, only the
// nonrelativistic quantum numbers l and m are specified. NB: There is
// no relativistic SIC at the moment.
void check_lm(int l, int m){

  if(l < 0){
    cout << "ERROR from check_lm(): Negative angular momentum. l = " << l << endl;
    myexit(1);
  }
  if(m < -l){
    cout << "ERROR from check_lm(): m less than -l. m = " << m << "  l = " << l << endl;
    myexit(1);
  }
  if(m > l){
    cout << "ERROR from check_lm(): m larger than l. m = " << m << "  l = " << l << endl;
    myexit(1);
  }
}

// Check the quantum numbers for L(S)DA+U potentials
// For L(S)DA+U, the relativistic quantum numbers kappa and mu are
// specified.
void check_kappa(int kappa, int mu2){

  int j2, l;

  if(kappa < 0){
    // kappa = -l - 1 (j = l + 1/2 or 2 j = 2 l + 1)
    // Extract the angular momentum l from kappa
    l  = -kappa - 1;
    j2 = 2 * l + 1;

    if(abs(mu2) > j2){
      cerr << "ERROR from check_kappa(): abs(mu2) too large " << kappa << " " << mu2 << endl;
      myexit(1);
    }
  }
  else{
    // kappa = l (j = l - 1/2 or 2 j = 2 l - 1)
    // Extract the angular momentum l from kappa
    l = kappa;
    j2 = 2 * l - 1;
    
    if(abs(mu2) > j2){
      cerr << "ERROR from check_kappa(): abs(mu2) too large " << kappa << " " << mu2 << endl;
      myexit(1);
    }
  }
}


// Read a potential
void atomics(const job &j, potential &p, char *name, int ptype){
  char		   buf[160];

  int		   i, isic, iu;

  double           u;

  ifstream	   inp;

  doublematrix     fp, fm;
  doublematrix     rb;

  complex	   v0, vup, vdo;
  complexfunction  vu, vd;
  complexfunction  v;
  
  Logger log(std::cout);
  log.setHeader("$LEVEL ($RANK) from atomics(): ");
  log.set_mpi_rank(j.parallel.mpi.rank);


  // Open input file
  inp.open(name);
  // Error handling
  if(!inp){
    cerr << "ERROR from atomics(): Cannot open inputfile '" << name << "'" << endl;
    myexit(1);
  }
  switch(ptype){
  case POT_EMPTY_SPHERE_SIC:
  case POT_MAG_EMPTY_SPHERE_SIC:
  case POT_NONMAG_SPHERE_SIC:
  case POT_MAG_SPHERE_SIC:
    // For SIC potentials, read the number of SIC levels, nsic
    inp >> p.z >> p.rm >> p.h >> p.n >> p.m >> p.td >> p.ef >> p.lambda >> p.nsic;
    break;
  case POT_NONMAG_SPHERE_U:
  case POT_MAG_SPHERE_U:
    // For L(S)DA+U potentials, read the number of SIC levels, nsic
    // L(S)DA+U potentials
    inp >> p.z >> p.rm >> p.h >> p.n >> p.m >> p.td >> p.ef >> p.lambda >> p.nsic;
    break;
  default:
    // Non-SIC potentials
    inp >> p.z >> p.rm >> p.h >> p.n >> p.m >> p.td >> p.ef >> p.lambda;
    break;
  }
  log << "Atomic potential: ";
  log << "z     =" ;
  log << setw(8) << setprecision(3) << p.z << "  ";
  log << "m     =" ;
  log << setw(12) << setprecision(6) << p.m << "  ";
  log << "td    =" ;
  log << setw(12) << setprecision(6) << p.td << " Kelvin ";
  log << "ef    =" ;
  log << setw(12) << setprecision(6) << p.ef << " eV ";
  // Fermi level in Hartree
  p.ef /= HART;
  log << "lambda =" ;
  log << setw(12) << setprecision(6) << p.lambda << endl;
  log << "Radial mesh: ";
  log << "n     =" ;
  log << setw(4) << p.n << "      ";
  log << "h     =" ;
  log << setw(12) << setprecision(6) << p.h << "  ";
  log << "rmt   =" ;
  log << setw(12) << setprecision(6) << p.rm << " Bohr";
  switch(ptype){
  case POT_EMPTY_SPHERE_SIC:
  case POT_MAG_EMPTY_SPHERE_SIC:
  case POT_NONMAG_SPHERE_SIC:
  case POT_MAG_SPHERE_SIC:
    // SIC potentials
    log << " nsic  =" ;
    log << setw(12) << setprecision(6) << p.nsic << endl;
    break;
  case POT_NONMAG_SPHERE_U:
  case POT_MAG_SPHERE_U:
    // L(S)DA+U potentials
    log << " nu  =" ;
    log << setw(12) << setprecision(6) << p.nsic << endl;
    break;
  default:
    // Non-SIC potentials
    log << endl;
    break;
  }
  inp.getline(buf, sizeof(buf));

  // Set exchange scaling; this is now set in getelements()
  // p.scale = scale;

  // Compute the table of radial mesh points
  p.r.setsize(p.n);
  for(i = 0; i < p.n; i++){
    p.r(i) = p.rm * exp((i - p.n + 1) * p.h);
  }

  // Check core charge
  switch(ptype){
  case POT_MAG_EMPTY_SPHERE_SIC:
  case POT_EMPTY_SPHERE_SIC:
  case POT_MAG_EMPTY_SPHERE:
  case POT_EMPTY_SPHERE:   
    if(fabs(p.z) > EMACH){
      cerr << "ERROR from atomics(): Core charge nonzero. p.z = " << p.z << endl;
      myexit(1);
    }
    break;
  default:
    break;
  }
  
  // Read the potential at the radial mesh points
  switch(ptype){
  case POT_MAG_EMPTY_SPHERE:
    inp >> vup;
    inp >> vdo;
    p.v.setsize(p.n) = 0.5 * (vup + vdo) * p.r;
    p.b.setsize(p.n) = 0.5 * p.scale * (vup - vdo) * p.r;
    if(fabs(p.scale) < EMACH){
      p.ptype = POT_NONMAG_SPHERE;
    }
    else{
      p.ptype = POT_MAG_EMPTY_SPHERE;
    }

    // Copy the potential to the SIC structure with indx == 0
    p.nsic = 0;
    p.sic = new psic[1];
    p.sic[0].v = p.v;
    p.sic[0].b = p.b;

    // Print potential
    log << "V:" << setprecision(6) << p.v(0) << " ... ";
    log << setprecision(6) << p.v(p.n - 1) << endl;
    log << "B:" << setprecision(6) << p.b(0) << " ... ";
    log << setprecision(6) << p.b(p.n - 1) << endl;
    break; 
  case POT_MAG_EMPTY_SPHERE_SIC:
    if(fabs(p.scale) < EMACH){
      p.ptype = POT_EMPTY_SPHERE_SIC;
    }
    else{
      p.ptype = POT_MAG_EMPTY_SPHERE_SIC;
    }
    
    if(p.nsic == 0){
      log.set_log_level(log.WARNING);
      log << "SIC not included! nsic = 0." << endl;
      log.set_log_level(log.MESSAGE);
    }
    
    p.sic = new psic[p.nsic + 1];
    
    inp >> vup;
    inp >> vdo;
    p.v.setsize(p.n) = 0.5 * (vup + vdo) * p.r;
    p.b.setsize(p.n) = 0.5 * p.scale * (vup - vdo) * p.r;
    
    
    // Copy the potential to the SIC structure with indx == 0
    p.sic[0].v = p.v;
    p.sic[0].b = p.b;
    
    // Write potential
    log << "V:" << setprecision(6) << p.v(0) << " ... ";
    log << setprecision(6) << p.v(p.n - 1) << endl;
    log << "B:" << setprecision(6) << p.b(0) << " ... ";
    log << setprecision(6) << p.b(p.n - 1) << endl;
    
    for(isic = 1; isic <= p.nsic; isic++){
       psic &pl = p.sic[isic];

       // Read angular-momentum quantum numbers, to be SICed
       inp >> pl.l >> pl.m;
       check_lm(p.sic[isic].l, p.sic[isic].m);

       // Read spin-up potential
       inp >> vup;

       // Read spin-down potential
       inp >> vdo;
       
       pl.v = 0.5 * (vup + vdo) * p.r;
       pl.b = 0.5 * p.scale * (vup - vdo) * p.r;
       // Write potential
       log << "V: " << pl.l << " " << pl.m << " " << ": ";
       log << setprecision(6) << pl.v(0) << " ... ";
       log << setprecision(6) << pl.v(p.n - 1)<<endl;
       log << "B: ";
       log << setprecision(6) << pl.b(0) << " ... ";
       log << setprecision(6) << pl.b(p.n - 1) << endl;
    }
    break;
  case POT_EMPTY_SPHERE:
    inp >> v0;
    p.v = v0 * p.r;
    p.b.setsize(p.n) = 0;
    p.ptype = POT_EMPTY_SPHERE;

    // Copy the potential to the SIC structure with indx == 0
    p.nsic = 0;
    p.sic = new psic[1];
    p.sic[0].v = p.v;
    p.sic[0].b = p.b;

    // Print potential
    log << "V:" << setprecision(6) << p.v(0) << " ... ";
    log << setprecision(6) << p.v(p.n - 1) << endl;

    break;
  case POT_NONMAG_SPHERE:
    // Non-magnetic spherical
    p.ptype = POT_NONMAG_SPHERE;
    inp >> p.v.setsize(p.n);
    p.b.setsize(p.n) = 0;

    // Copy the potential to the SIC structure with indx == 0
    p.nsic = 0;
    p.sic = new psic[1];
    p.sic[0].v = p.v;
    p.sic[0].b = p.b;

    // Print potential
    log << "V:" << setprecision(6) << p.v(0) << " ... ";
    log << setprecision(6) << p.v(p.n - 1) << endl;
    break;
  case POT_EMPTY_SPHERE_SIC:
    p.ptype = POT_EMPTY_SPHERE_SIC;
    
    p.sic = new psic[p.nsic + 1];
    
    if(p.nsic == 0){
      log.set_log_level(log.WARNING);
      log << "SIC not included! nsic = 0." << endl;
      log.set_log_level(log.MESSAGE);
    }

    inp >> v0;
    p.v.setsize(p.n) = v0 * p.r;
    p.b.setsize(p.n) = 0;

    // Copy the potential to the SIC structure with indx == 0
    p.sic[0].v = p.v;
    p.sic[0].b = p.b;

    // Write potential
    log << "V:" << setprecision(6) << p.v(0) << " ... ";
    log << setprecision(6) << p.v(p.n - 1) << endl;

    for(isic = 1; isic <= p.nsic; isic++){
      psic &pl = p.sic[isic];

      inp >> pl.l;

      inp >> v0;
      p.v.setsize(p.n) = v0 * p.r;
      p.b.setsize(p.n) = 0;

      pl.v = p.v;
      pl.b = p.b;

      // Write potentials
      log << "V: " << pl.l << ": ";
      log << setprecision(6) << pl.v(0) << " ... ";
      log << setprecision(6) << pl.v(p.n - 1) << endl;
    }
    break;
  case POT_MAG_SPHERE:
    // Magnetic spherical
    // Read spin-up potential
    inp >> vu.setsize(p.n);
    // Read spin-down potential
    inp >> vd.setsize(p.n);
    p.v = 0.5 * (vu + vd);
    p.b = 0.5 * p.scale * (vu - vd);

    // Copy the potential to the SIC structure with indx == 0
    p.nsic = 0;
    p.sic = new psic[1];
    p.sic[0].v = p.v;
    p.sic[0].b = p.b;

    if(fabs(p.scale) < EMACH){
      p.ptype = POT_NONMAG_SPHERE;
    }
    else{
      p.ptype = POT_MAG_SPHERE;
    }
    // Print potential
    log << "V:" << setprecision(6) << p.v(0) << " ... ";
    log << setprecision(6) << p.v(p.n - 1) << endl;
    log << "B:" << setprecision(6) << p.b(0) << " ... ";
    log << setprecision(6) << p.b(p.n - 1) << endl;
    break;
  case POT_MAG_NONSPHERE:
    // Magnetic non-spherical
    int l, m, s;
    double ffp, ffm;
    int km;
    int km1, km2, kmn1, kmn2;
    int l1, l2, m1, m2;
    double c1, c2;
    
    p.ptype = POT_MAG_NONSPHERE;
    rb.setsize(kmmax, kmmax) = 0;
    for(km = 0; km < kmmax; km++){
      rb(km, trkm[km]) = trsg[km];
    }
    v.setsize(p.n);
    fp.setsize(kmmax, kmmax);
    fm.setsize(kmmax, kmmax);
    p.vp.setsize(p.n, kmmax, kmmax) = 0;
    p.vm.setsize(p.n, kmmax, kmmax) = 0;
    while(! !inp){
      inp >> l;
      if(l == -1){
	break;
      }
      inp >> m >> s;
      inp >> v;
      log << setw(2) << l << " ";
      log << setw(2) << m << " ";
      log << setw(2) << s << " ";
      log << setprecision(6) << v(0) << " ... ";
      log << setprecision(6) << v(p.n - 1) << endl;
      for(km1 = 0; km1 < kmmax; km1++){
	kmn1 = kmn[km1];
	for(km2 = 0; km2 < kmmax; km2++){
	  kmn2 = kmn[km2];
	  ffp = 0;
	  ffm = 0;
	  if(s >= 0){
	    l1 = kml[km1];
	    l2 = kml[km2];
	    m1 = kmm1[km1];
	    m2 = kmm1[km2];
	    c1 = cleb1[km1];
	    c2 = cleb1[km2];
	    ffp += c1 * c2 * blm(l1, -m1, l, m, l2, m2) * (m1 & 1 ? -1 : 1);
	    l1 = kml[kmn1];
	    l2 = kml[kmn2];
	    m1 = kmm1[kmn1];
	    m2 = kmm1[kmn2];
	    c1 = cleb1[kmn1];
	    c2 = cleb1[kmn2];
	    ffm += c1 * c2 * blm(l1, -m1, l, m, l2, m2) * (m1 & 1 ? -1 : 1);
	  }
	  if(s <= 0){
	    l1 = kml[km1];
	    l2 = kml[km2];
	    m1 = kmm2[km1];
	    m2 = kmm2[km2];
	    c1 = cleb2[km1];
	    c2 = cleb2[km2];
	    ffp += c1 * c2 * blm(l1, -m1, l, m, l2, m2) * (m1 & 1 ? -1 : 1);
	    l1 = kml[kmn1];
	    l2 = kml[kmn2];
	    m1 = kmm2[kmn1];
	    m2 = kmm2[kmn2];
	    c1 = cleb2[kmn1];
	    c2 = cleb2[kmn2];
	    ffm += c1 * c2 * blm(l1, -m1, l, m, l2, m2) * (m1 & 1 ? -1 : 1);
	  }
	  if(fabs(ffp) < EMACH){
	    ffp = 0;
	  }
	  if(fabs(ffm) < EMACH){
	    ffm = 0;
	  }
	  fp(km1, km2) = ffp;
	  fm(km1, km2) = ffm;
	}
      }
      fp = sqrt(4 * M_PI) * fp;
      fm = sqrt(4 * M_PI) * fm;
      p.vp = p.vp + fp * v;
      p.vm = p.vm + fm * v;
    }
    // Skalieren
    p.vp = (1 + p.scale) / 2 * p.vp - (1 - p.scale) / 2 * rb * p.vp * rb;
    p.vm = (1 + p.scale) / 2 * p.vm - (1 - p.scale) / 2 * rb * p.vm * rb;
    break;
  case POT_NONMAG_SPHERE_SIC:
    // Non-magnetic spherical, SIC
    p.ptype = POT_NONMAG_SPHERE_SIC;

    if(p.nsic == 0){
      log.set_log_level(log.WARNING);
      log << "SIC not included! nsic = 0." << endl;
      log << "Changing potential type to " << POT_NONMAG_SPHERE << "."<< endl;
      log.set_log_level(log.MESSAGE);
      p.ptype = POT_NONMAG_SPHERE;
    }
    p.sic = new psic[p.nsic + 1];
    
    // The LDA potential
    inp >> p.sic[0].v.setsize(p.n);
    p.sic[0].b.setsize(p.n) = 0;
    
    // Write potentials
    log << "V: ";
    log << setprecision(6) << p.sic[0].v(0) << " ... ";
    log << setprecision(6) << p.sic[0].v(p.n - 1) << endl;

    // Copy the LDA potential to the usual structure (with index 0)
    p.v = p.sic[0].v;
    p.b = p.sic[0].b;

    // The SIC potentials
    // isic = 0 refers to the LSDA potential
    for(isic = 1; isic <= p.nsic; isic++){
      psic &pl = p.sic[isic];
      
      inp >> pl.l >> pl.m;
      check_lm(pl.l, pl.m);

      inp >> pl.v.setsize(p.n);
      pl.b.setsize(p.n) = 0;
      
      // Write potentials
      log << "V: " << pl.l << " " << pl.m << ": ";
      log << setprecision(6) << pl.v(0) << " ... ";
      log << setprecision(6) << pl.v(p.n - 1) << endl;
    }
    
    break;
  case POT_MAG_SPHERE_SIC:
    // Magnetic spherical, SIC
    if(fabs(p.scale) < EMACH){
      log.set_log_level(log.WARNING);
      log << "scale = 0." << endl;
      log << "Changing potential type to " << POT_NONMAG_SPHERE_SIC << "."<< endl;
      log.set_log_level(log.MESSAGE);
      p.ptype = POT_NONMAG_SPHERE_SIC;
    }
    else{
      p.ptype = POT_MAG_SPHERE_SIC;
    }

    if(p.nsic == 0){
      log.set_log_level(log.WARNING);
      log << "SIC not included! nsic = 0." << endl;
      log << "Changing potential type to " << POT_MAG_SPHERE << "."<< endl;
      log.set_log_level(log.MESSAGE);
      p.ptype = POT_MAG_SPHERE;
    }
    p.sic = new psic[p.nsic + 1];


    // The LSDA potential
    // Read spin-up potentials
    inp >> vu.setsize(p.n);
    // Raed spin-down potential
    inp >> vd.setsize(p.n);
    
    p.sic[0].v = 0.5 * (vu + vd);
    p.sic[0].b = 0.5 * p.scale * (vu - vd);
    
    // Write potential
    log << "V: ";
    log << setprecision(6) << p.sic[0].v(0) << " ... ";
    log << setprecision(6) << p.sic[0].v(p.n - 1) << endl;
    log << "B: ";
    log << setprecision(6) << p.sic[0].b(0) << " ... ";
    log << setprecision(6) << p.sic[0].b(p.n - 1) << endl;

    // Copy the LSDA potential to the usual structure
    p.v = p.sic[0].v;
    p.b = p.sic[0].b;

    // The SIC potentials; isic = 0 refers to the LSDA potential
    for(isic = 1; isic <= p.nsic; isic++){
      psic &pl = p.sic[isic];

      // Read the quantum numbers for the SIC levels
      inp >> pl.l >> pl.m;
      
      check_lm(pl.l, pl.m);

      // Read spin-up potentials
      inp >> vu.setsize(p.n);
      // Read spin-down potential
      inp >> vd.setsize(p.n);
      
      pl.v = 0.5 * (vu + vd);
      pl.b = 0.5 * p.scale * (vu - vd);

      // Write potential
      log << "V: " << pl.l << " " << pl.m << ": ";
      log << setprecision(6) << pl.v(0) << " ... ";
      log << setprecision(6) << pl.v(p.n - 1) << endl;
      log << "B: " << pl.l << " " << pl.m << ": ";
      log << setprecision(6) << pl.b(0) << " ... ";
      log << setprecision(6) << pl.b(p.n - 1) << endl;
    }

    break;
  case POT_NONMAG_SPHERE_U:
    // Non-magnetic spherical, (pseudo) LDA+U
    p.ptype = POT_NONMAG_SPHERE_U;

    if(p.nsic == 0){
      log.set_log_level(log.WARNING);
      log << "LDA+U not included! nu = 0." << endl;
      log << "Changing potential type to " << POT_NONMAG_SPHERE << "."<< endl;
      log.set_log_level(log.MESSAGE);
      p.ptype = POT_NONMAG_SPHERE;
    }
    p.sic = new psic[p.nsic + 1];
    
    // The LDA potential
    inp >> p.sic[0].v.setsize(p.n);
    p.sic[0].b.setsize(p.n) = 0;
    
    // Write potentials
    log << "V: ";
    log << setprecision(6) << p.sic[0].v(0) << " ... ";
    log << setprecision(6) << p.sic[0].v(p.n - 1) << endl;

    // Copy the LDA potential to the usual structure
    p.v = p.sic[0].v;
    p.b = p.sic[0].b;

    // The LDA+U potentials
    // iu = 0 refers to the LSDA potential
    for(iu = 1; iu <= p.nsic; iu++){
      psic &pl = p.sic[iu];
      
      // u ist the energy shift (in Hartree)
      inp >> pl.kappa >> u;

      pl.v.setsize(p.n) = p.sic[0].v + u * p.r;
      pl.b.setsize(p.n) = 0;
      
      // Write potentials
      log << "V: " << pl.kappa << " " << u * HART << ": ";
      log << setprecision(6) << pl.v(0) << " ... ";
      log << setprecision(6) << pl.v(p.n - 1) << endl;
    }
    
    break;
  case POT_MAG_SPHERE_U:
    // Magnetic spherical, (pseudo) LSDA+U
    if(fabs(p.scale) < EMACH){
      p.ptype = POT_NONMAG_SPHERE_U;
    }
    else{
      p.ptype = POT_MAG_SPHERE_U;
    }

    if(p.nsic == 0){
      log.set_log_level(log.WARNING);
      log << "LSDA+U not included! nu = 0." << endl;
      log << "Changing potential type to " << POT_MAG_SPHERE << "."<< endl;
      log.set_log_level(log.MESSAGE);
      p.ptype = POT_MAG_SPHERE;
    }
    p.sic = new psic[p.nsic + 1];


    // The LSDA potential
    // Read spin-up potentials
    inp >> vu.setsize(p.n);
    // Read spin-down potential
    inp >> vd.setsize(p.n);
    
    p.sic[0].v = 0.5 * (vu + vd);
    p.sic[0].b = 0.5 * p.scale * (vu - vd);
    
    // Write potential
    log << "V: ";
    log << setprecision(6) << p.sic[0].v(0) << " ... ";
    log << setprecision(6) << p.sic[0].v(p.n - 1) << endl;
    log << "B: ";
    log << setprecision(6) << p.sic[0].b(0) << " ... ";
    log << setprecision(6) << p.sic[0].b(p.n - 1) << endl;

    // Copy the LSDA potential to the usual structure
    p.v = p.sic[0].v;
    p.b = p.sic[0].b;

    // The SIC potentials; isic = 0 refers to the LSDA potential
    for(iu = 1; iu <= p.nsic; iu++){
      psic &pl = p.sic[iu];

      // Read the quantum numbers for the SIC levels
      inp >> pl.kappa >> pl.mu2 >> u;

      check_kappa(pl.kappa, pl.mu2);

      // Shift the potentials by U
      pl.v = p.sic[0].v + u * p.r;
      pl.b = p.sic[0].b;

      // Write potential
      log << "V: " << pl.kappa << " " << pl.mu2 << " " << u * HART << ": ";
      log << setprecision(6) << pl.v(0) << " ... ";
      log << setprecision(6) << pl.v(p.n - 1) << endl;
      log << "B: " << pl.kappa << " " << pl.mu2 << ": ";
      log << setprecision(6) << pl.b(0) << " ... ";
      log << setprecision(6) << pl.b(p.n - 1) << endl;
    }

    break;
  default:
    break;
  }

  log << endl;
  
  // Error handling
  if(!inp){
    cerr << "ERROR from atomics: Error while reading '" << name << "'" << endl;
    myexit(1);
  }
  // Close potential file
  inp.close();
}


