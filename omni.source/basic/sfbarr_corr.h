// surfpot.h
//
// Functions for corrugated smooth surface barrier
//

#ifndef _SFBARR_CORR_
#define _SFBARR_CORR_

#include "crystal.h"
#include "beam.h"

// Read the file with the V_g(z)
void read_surf_pot(csurfpot &sp);

// Reflection at a smooth corrugated surface barrier
void sfbarrcorr(state &s, beamset &b, crystal &c);

#endif
