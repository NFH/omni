// xmatkc.C
//
// enthaelt das Unterprogramm zur Berechnung der Strukturkonstanten
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#include <cmath>

#include "complex.h"
#include "cmatrix.h"
#include "vector2.h"
#include "vector3.h"
#include "intmath.h"
#include "sfunc.h"

#include "constants.h"

#include "crystal.h"

#include "clebgn.h"

// Number of rings for convergence check
#define NRINGS 20

#define VERBOSITY 0

using namespace std;

// Speicher fuer die Parameter elmc im Unterprogramm xmatk

double		*elmc;

// Struktur zur Kommunikation zwischen xmatkc und dlm1c und dlm2c

struct xmatpc{
  complex		kappa;
  complex		kapsq;
  complex		alpha;
  complex		rta;
  complex		*dlm;
  complex		*pref;
  double		*denom;
  complex		*agk;
  complex		*xpm;
  complex		*gkn;
  complex		*gkc;
  complex		*ddd;
  complex		*ylma;
};

// Programm zur Tabellierung der Clebsch-Gordonartigen Koeffizienten ELM fuer
// das Unterprogramm xmatk
// elm enthaelt nach Rueckkehr global diese Tabelle

void elmgc()

{
  double	alm;
  int	l1, m1, l2, m2, l3, m3;
  int	lm;
  int	k;

  // Speicher fuer die elm (wird erst bei Programmende wieder freigegeben)
  elmc = new double[(((12 * lmax + 34) * lmax + 34) * lmax + 15) * (lmax + 2) * (lmax + 1) / 30];
  // Fange beim nullten Element an
  k = 0;
  // Speichere alle nichtverschwindenden Werte
  for(l2 = 0; l2 <= lmax; l2++){
    for(m2 = -l2; m2 <= l2; m2++){
      for(l3 = 0; l3 <= lmax; l3++){
	for(m3 = -l3; m3 <= l3; m3++){
	  // Damit steht m1 fest
	  m1 = m3 - m2;
	  // Bestimme untere Grenze fuer l1
	  lm = max(iabs(m1), iabs(l3 - l2));
	  // Fange bei der oberen Grenze an
	  for(l1 = l2 + l3; l1 >= lm; l1 -= 2){
	    alm = 4 * M_PI * blm(l1, m1, l2, m2, l3, -m3);
	    // JH Added paranthese around "(l2 - l1 - l3) / 2 - m2" in migration to gcc 4.3
	    elmc[k] = (((l2 - l1 - l3) / 2 - m2) & 1) ? -alm : alm;
	    // naechster Wert
	    k += 1;
	  }
	}
      }
    }
  }
}

// Unterprogramm zur Summation der dlm1
// p enthaelt Parameter
// q enthaelt den aktuellen k-Vektor
// sk enthaelt den Verschiebungsvektor
// der Beitrag dieses Gittervektors wird in dlm aufsummiert

void dlm1c(xmatpc &p, vector2 q, vector3 sk)

{
  double		ac, acsq, b, tal;
  complex		gp, gpsq;
  complex		xpk, gk, gkk, acc;
  complex		cf, zz, cz, z2, cz2, cx;
  complex		w1, w2;
  int		i;
  int		k, kk;
  int		l, m;
  int		n, nn, s;

  // Zunaechst werden vier kleine Felder initialisiert
  // xpm[m] enthaelt xpk^|m|
  // agk[i] enthaelt (ac/kappa)^i
  // gkcij[j] enthaelt (kappa*cijz)^(2n-s)
  // gkn[n] enthaelt (gk/kappa)^(2n-1)*gam<n,z>
  acsq = q * q;
  gpsq = p.kapsq - acsq;
  ac = sqrt(acsq);
  gp = sqrt(gpsq);
  if(ac > EMACH){
    xpk = cmplx(q.x, q.y) / ac;
    gk = ac / p.kappa;
    gkk = gpsq / p.kapsq;
  }
  else{
    xpk = 0;
    gk = 0;
    gkk = 1;
  }
  p.xpm[0] = 1;
  p.agk[0] = 1;
  p.gkc[0] = 1;
  for(i = 1; i <= 2 * lmax; i++){
    p.xpm[i] = xpk * p.xpm[i - 1];
    p.agk[i] = gk * p.agk[i - 1];
    p.gkc[i] = p.kappa * sk.z * p.gkc[i - 1];
  }
  cf = p.kappa / gp;
  zz = -p.alpha * gkk;
  cz = cmplx(0, -1) * sqrt(-zz);
  z2 = gpsq * sk.z * sk.z;
  cz2 = sqrt(z2);
  if(real(cz) < 0 && real(zz) > 150){
    cx = 0;
    p.ddd[0] = 2 * sqrt(M_PI);
    if(imag(zz) > 0){
      p.ddd[0] *= exp(cmplx(0, -2*imag(zz)));
    }
    for(i = 1; i <= 2 * lmax; i++){
      cx /= zz;
      b = 1 / (0.5 - i);
      p.ddd[i] = b * (p.ddd[i - 1] - cx * cz);
    }
  }
  else{
    cx = exp(-zz + z2 / (4 * zz));
    w1 = cerf(cmplx(0, 1) * cz - cz2 / (2 * cz));
    w2 = cerf(cmplx(0, 1) * cz + cz2 / (2 * cz));
    p.ddd[0] = sqrt(M_PI) * cx * (w1 + w2) / 2;
    if(sk.z == 0){
      for(i = 1; i <= 2 * lmax; i++){
	cx /= zz;
	b = 1 / (0.5 - i);
	p.ddd[i] = b * (p.ddd[i - 1] - cx * cz);
      }
    }
    else{
      p.ddd[1] = cmplx(0, 1) * sqrt(M_PI) * cx * (w1 - w2) / cz2;
      for(i = 2; i <= 2 * lmax; i++){
	cx /= zz;
	b = 1.5 - i;
	p.ddd[i] = 4 * (b * p.ddd[i - 1] - p.ddd[i - 2] + cx * cz) / z2;
      }
    }
  }
  p.gkn[0] = cf * p.ddd[0];
  for(i = 1; i <= 2 * lmax; i++){
    cf *= gkk;
    p.gkn[i] = cf * p.ddd[i];
  }
  // Und nun wird der Beitrag des k-Vektors q in dlm summiert
  k = 0;
  kk = 0;
  for(l = 0; l <= 2 * lmax; l++){
    for(m = 0; m <= l; m++){
      acc = 0;
      nn = l - m;
      if(ac > EMACH){
	for(n = 0; n <= nn; n++){
	  for(s = nn; s >= n; s-= 2){
	    if(s <= n + n){
	      acc += p.denom[k] * p.agk[l - s] * p.gkn[n] * p.gkc[n + n - s];
	      k = k + 1;
	    }
	  }
	}
      }
      else{
	if(m == 0){
	  for(n = l; 2 * n >= l; n--){
	    acc += p.gkn[n] * p.gkc[n + n - l] / (fac[l - n] * fac[n + n - l]);
	  }
	}
      }
      tal = q * sk;
      acc *= cmplx(cos(tal), sin(tal)) * p.pref[kk];
      n = l * (l + 1);
      p.dlm[n - m] += acc * p.xpm[m];
      if(m != 0 && ac > EMACH){
	p.dlm[n + m] += acc / p.xpm[m];
      }
      kk += 1;
    }
  }
}

// Unterprogramm zur Summation der dlm2
// p enthaelt Parameter
// q enthaelt k-parallel
// r enthaelt den aktuellen r-Vektor
// sk enthaelt den Verschiebungsvektor
// der Beitrag dieses Vektors wird in dlm aufsummiert

void dlm2c(xmatpc &p, vector2 q, vector2 r, vector3 sk)

{
  double		b, c;
  double		ar, ad, al;
  complex		cff, ctt, stt;
  complex		aa, ab, xpa;
  complex		sd, kant, knsq, acc;
  complex		z, zz, w, ww;
  complex		u, u1, u2;
  complex		cp, cf;
  int		n;
  int		l, m;

  // Tabelliere die Kugelflaechenfuntkionen
  // Berechne zunaechst die Argumente
  c = r * r;
  ar = sqrt(c + sk.z * sk.z);
  if(c > EMACH){
    b = sqrt(c);
    cff = cmplx(r.x, r.y) / b;
  }
  else{
    b = 0;
    cff = 1;
  }
  ctt = sk.z / ar;
  stt = b / ar;
  sphrm(p.ylma, ctt, stt, cff, 2 * lmax);
  // Das Integral u wird durch eine Rekursionsformel ueber l gewonnen
  // u1 und u2 enthalten die Startwerte fuer l=-1 und l=0
  ad = q * (r - sk);
  sd = polar(1, -ad);
  kant = ar * p.kappa / 2;
  knsq = kant * kant;
  z = cmplx(0, 1) * kant / p.rta;
  zz = p.rta - z;
  z = p.rta + z;
  ww = cerf(-zz);
  w = cerf(z);
  aa = cmplx(0, sqrt(M_PI) / 2) * (ww - w);
  ab = (sqrt(M_PI) / 2) * (ww + w);
  xpa = exp(p.alpha - knsq / p.alpha);
  u1 = aa * xpa;
  u2 = ab * xpa / kant;
  // Und nun wird der Beitrag des Vektors r in dlm summiert
  al = -0.5;
  cp = p.rta;
  cf = 1;
  for(l = 0; l <= 2 * lmax; l++){
    acc = p.pref[0] * u2 * cf * sd;
    for(m = 0; m <= l; m++){
      n = l * (l + 1);
      p.dlm[n + m] += acc * p.ylma[n - m];
      if(m != 0){
	p.dlm[n - m] += acc * p.ylma[n + m];
      }
      acc = -acc;
    }
    al += 1;
    cp /= p.alpha;
    u = (al * u2 - u1 + cp * xpa) / knsq;
    u1 = u2;
    u2 = u;
    cf *= -kant;
  }
}

// Programm zur Berechnung der dlm's
// c enthaelt die Einheitszelle
// p enthaelt Parameter
// q enthaelt k-parallel
// sk enthaelt den Verschiebungsvektor
// nndlm Groesse von dlm

void dlmgc(crystal &c, xmatpc &p, vector2 q, vector3 sk, int nndlm)

{
  double		al;
  int		i, s, i1, i2;
  int		k, kk;
  int		l, m;
  int		n, nn;
  double		p1, p2;
  complex		cf, cp;
  double		test, test1, test2;

  // Speicher fuer die Unterprogramm dlm1c und dlm2c
  p.pref = new complex[(2 * lmax + 1) * (lmax + 1)];
  p.denom = new double[(lmax + 1) * (lmax + 1) * (lmax + 2) * (lmax + 3) / 6];
  p.agk = new complex[2 * lmax + 1];
  p.xpm = new complex[2 * lmax + 1];
  p.gkn = new complex[2 * lmax + 1];
  p.gkc = new complex[2 * lmax + 1];
  p.ddd = new complex[2 * lmax + 1];
  p.ylma = new complex[(2 * lmax + 1) * (2 * lmax + 1)];
  // Berechne die Separationskonstante
  p.alpha = c.ar * p.kapsq / (4 * M_PI);
  al = abs(p.alpha);
  if(exp(al)*EMACH > 5E-5){
    al = log(5E-5 / EMACH);
    p.alpha = (real(p.kapsq) >= 0) ? al : -al;
  }
  p.rta = sqrt(p.alpha);
  // Nun berechne dlm1. Zunaechst wird Vorfaktor p1 in pref fuer l+|m| tabelliert.
  // (Also (0,0), (1,0), (1,1), (2,0), (2,1), ...) gleichzeitig wird der Faktor f1
  // in denom tabelliert.
  k = 0;
  kk = 0;
  p1 = -2 / c.ar;
  p2 = -1;
  cf = cmplx(0, 1) / p.kappa;
  for(l = 0; l <= 2 * lmax; l++){
    p1 /= 2;
    p2 += 2;
    cp = cf;
    for(m = 0; m <= l; m++){
      p.pref[kk] = cp * p1 * sqrt(p2 * fac[l + m] * fac[l - m]);
      kk += 1;
      cp = cmplx(0, 1) * cp;
      nn = l - m;
      for(n = 0; n <= nn; n++){
	for(s = nn; s >= n; s-= 2){
	  if(s <= n + n){
	    p.denom[k] = 1 / (fac[n + n - s] * fac[ s - n] * fac[(l - m - s) / 2] * fac[(l + m - s) / 2]);
	    k = k + 1;
	  }
	}
      }
    }
  }
  // Summiere nun die Beitraege der einzelnen reziproken Gittervektoren
  // Beginne mit dem Ursprung
  dlm1c(p, q, sk);
  // Berechne Startwert fuer Konvergenztest
  test1 = 0;
  for(i = 0; i < nndlm; i++){
    test1 += norm(p.dlm[i]);
  }
  // Die folgende Summation geschieht jeweils ueber Parallelogramme um den
  // Ursprung, wobei die Kantenlaenge jeweils um 2 erhoeht wird.
  i1 = 1;
  do{
    // Schleife ueber alle Punkte einer Seite
    for(i2 = -i1; i2 < i1; i2++){
      // Summiere die vier Seiten
      dlm1c(p, q + i1 * c.b1 + i2 * c.b2, sk);
      dlm1c(p, q - i2 * c.b1 + i1 * c.b2, sk);
      dlm1c(p, q - i1 * c.b1 - i2 * c.b2, sk);
      dlm1c(p, q + i2 * c.b1 - i1 * c.b2, sk);
    }
    // Konvergenztest
    test2 = 0;
    for(i = 0; i < nndlm; i++){
      test2 += norm(p.dlm[i]);
    }
    test = fabs((test2 - test1) / test1);
#if(VERBOSITY >= 1)
    cout << "dlmgc(): Dlm1 convergence. " << test << " " << i1 - 1<< endl;
#endif

    test1 = test2;
    i1 += 1;
  }
  while(test > EMACH && i1 <= NRINGS);
  if(test > EMACH){
    cout << "dlmgc(): Dlm1's did not converge for " << NRINGS << " rings. test =" << test << endl;
  }
  // Zum Abschluss multipliziere die dlm1 mit (-1)^((m+|m|)/2)
  for(l = 1; l <= 2 * lmax; l++){
    for(m = 1; m <= l; m += 2){
      n = l * (l + 1) + m;
      p.dlm[n] = -p.dlm[n];
    }
  }
  // Nun berechne dlm2. Zunaechst wird Vorfaktor p2 in pref "tabelliert".
  p.pref[0] = -p.kappa / (2 * sqrt(M_PI));
  // Beginne mit dem Ursprung
  dlm2c(p, q, sk, sk);
  // Summiere nun die Beitraege der einzelnen Gittervektoren.
  // Die folgende Summation geschieht jeweils ueber Parallelogramme um den
  // Ursprung, wobei die Kantenlaenge jeweils um 2 erhoeht wird.
  i1 = 1;
  do{
    // Schleife ueber alle Punkte einer Seite
    for(i2 = -i1; i2 < i1; i2++){
      // Summiere die vier Seiten
      dlm2c(p, q, sk + i1 * c.r1 + i2 * c.r2, sk);
      dlm2c(p, q, sk - i2 * c.r1 + i1 * c.r2, sk);
      dlm2c(p, q, sk - i1 * c.r1 - i2 * c.r2, sk);
      dlm2c(p, q, sk + i2 * c.r1 - i1 * c.r2, sk);
    }
    // Konvergenztest
    test2 = 0;
    for(i = 0; i < nndlm; i++){
      test2 += norm(p.dlm[i]);
    }
    test = fabs((test2 - test1) / test1);
#if(VERBOSITY >= 1)
    cout << "dlmgc(): Dlm2 convergence. " << test << " " << i1 << endl;
#endif

    test1 = test2;
    i1 += 1;
  }
  while(test > EMACH && i1 <= NRINGS);
  if(test > EMACH){
    cout << "dlmgc(): Dlm2's did not converge for " << NRINGS << " rings. test =" << test << endl;
  }
  // Speicher freigeben
  delete[] p.pref;
  delete[] p.denom;
  delete[] p.agk;
  delete[] p.xpm;
  delete[] p.gkn;
  delete[] p.gkc;
  delete[] p.ddd;
  delete[] p.ylma;
}

// Programm zur Berechnung der Strukturkonstanten mit Beruecksichtigung eines
// Verschiebungsvektors (der Fall |sk|=0 wird von xmatk behandelt)
// S enthaelt bei Rueckkehr die relativistische Strukturkonstante
// e enthaelt die komplexe Energie
// c enthaelt die Einheitszelle
// q enthaelt k-parallel
// sk enthaelt den Verschiebungsvektor

void xmatkc(complexmatrix &S, complex e, crystal &c, vector2 q, vector3 sk)

{
  int		i, j, k;
  int		l1, m1, l2, m2, l3, m3;
  int		lm, ln;
  complex		alm;
  int		lm1, lm2, lm1p, lm2p;
  double		clb1, clb2, clb1p, clb2p;
  int		nndlm;
  xmatpc		p;		// zur Kommunikation
  complexmatrix	X;		// nichtrelativistische Strukturkonstante

  // Berechne Groesse der dlm
  nndlm = (2 * lmax + 1) * (2 * lmax + 1);
  // Speicher fuer das Unterprogramm xmatkc
  p.dlm = new complex[nndlm];
  // Loesche die dlm
  for(i = 0; i < nndlm; i++){
    p.dlm[i] = 0;
  }
  // Einige Konstanten
  p.kapsq = 2 * e + e * e / CLIGHT2;
  p.kappa = sqrt(p.kapsq);
  // Nun berechne die dlm's
  dlmgc(c, p, q, sk, nndlm);
  // Nun berechne die nichtrelativistische Strukturkonstante in x
  X.setsize(lmmax, lmmax) = 0;
  // Fange beim nullten Element an
  k = 0;
  i = 0;
  for(l2 = 0; l2 <= lmax; l2++){
    for(m2 = -l2; m2 <= l2; m2++){
      j = 0;
      for(l3 = 0; l3 <= lmax; l3++){
	for(m3 = -l3; m3 <= l3; m3++){
	  // Damit steht m1 fest
	  m1 = m3 - m2;
	  // Bestimme untere Grenze fuer l1
	  lm = max(iabs(m1), iabs(l3 - l2));
	  // und summiere
	  alm = 0;
	  // Startindex
	  ln = (l2 + l3) * (l2 + l3 + 1) - m1;
	  // Fange bei der oberen Grenze an
	  for(l1 = l2 + l3; l1 >= lm; l1 -= 2){
	    alm += elmc[k] * p.dlm[ln];
	    // naechster Wert
	    ln -= 4 * l1 - 2;
	    k += 1;
	  }
	  X(i, j) = alm;
	  j += 1;
	}
      }
      i += 1;
    }
  }
  // Setze Groesse der relativistischen Strukturkonstanten
  S.setsize(kmmax, kmmax) = 0;
  // Berechne jetzt die relativistische Strukturkonstante
  for(i = 0; i < kmmax; i++){
    lm1 = kmlm1[i];
    lm2 = kmlm2[i];
    clb1 = cleb1[i];
    clb2 = cleb2[i];
    for(j = 0; j < kmmax; j++){
      lm1p = kmlm1[j];
      lm2p = kmlm2[j];
      clb1p = cleb1[j];
      clb2p = cleb2[j];
      S(i, j) = clb1 * clb1p * X(lm1, lm1p) + clb2 * clb2p * X(lm2, lm2p);
    }
  }
  // Speicher freigeben
  delete[] p.dlm;
}

#undef NRINGS
#undef VERBOSITY
