// representation.C
//
// Transformation from omni to Durham representation
//
#include <cmath>

#include "complex.h"
#include "cmatrix.h"
#include "csmatrix.h"

#include "job.h"
#include "state.h"
#include "crystal.h"

#include "clebgn.h"


// Transform a single-site t-matrix from omni to Durham representation
void omni2durham_t(complexmatrix &T, complex k){

  T =  (cmplx(0, 1) / k) * T;

}
void omni2durham_t2(complexsupermatrix &Tin, complexsupermatrix &Tout, complex k){

  Tout =  (cmplx(0, 1) / k) * Tin;

}


// Transform a slab t-matrix from omni to Durham representation
void omni2durham_slab_t(sslab &ss, cslab &cs){

  int atom;

  for(atom = 0; atom < cs.natom; atom++){
    omni2durham_t(ss.T(atom, atom), ss.k);
  }
}


// Transform an atomic Green function from omni to Durham representation
void omni2durham_gf(complexsupermatrix &Tau, state &s, crystal &c, int layer, int layerp){

  complexsupermatrix Tmat, Tmatp, Vmat;

  slayer &sl = s.layer[layer];
  sslice &sc = *sl.slice;
  sslab  &ss = *sc.slab;

  slayer &slp = s.layer[layerp];
  sslice &scp = *slp.slice;
  sslab  &ssp = *scp.slab;

  // Right-hand-side t-matrices (omni -> Durham, see note)
  // Tmat  = (cmplx(0, 1) / ss.k)  * ss.T;
  // Tmatp = (cmplx(0, 1) / ssp.k) * ssp.T;
  omni2durham_t2(ss.T,  Tmat,  ss.k);
  omni2durham_t2(ssp.T, Tmatp, ssp.k);

  // GF matrix 
  Vmat = sl.V[layerp];


  // Multiple-scattering contribution
  Tau = Tmatp * Vmat * Tmat;

  // Site-diagonal contribution
  if(layer == layerp){
    Tau = Tau + Tmat;
  }
}



// Transform a Green function from omni to Durham representation, real-space mode
void omni2durham_gf_rs(complexsupermatrix &Tau, state &s, crystal &c, int layer, int cell, int layerp, int cellp){

  complexsupermatrix Tmat, Tmatp, Vmat;

  slayer &sl = s.layer[layer];
  scell  &se = sl.cell[cell];
  sslice &sc = *sl.slice;
  sslab  &ss = *sc.slab;

  slayer &slp = s.layer[layerp];
  sslice &scp = *slp.slice;
  sslab  &ssp = *scp.slab;

  clayer &clp = c.layer[layerp];

  // Right-hand-side t-matrices (omni -> Durham, see note)
  // Tmat  = (cmplx(0, 1) / ss.k)  * ss.T;
  // Tmatp = (cmplx(0, 1) / ssp.k) * ssp.T;
  omni2durham_t2(ss.T,  Tmat,  ss.k);
  omni2durham_t2(ssp.T, Tmatp, ssp.k);

  // GF matrix 
  Vmat = se.V[clp.ncelloffset + cellp];


  // Multiple-scattering contribution
  Tau = Tmatp * Vmat * Tmat;

  // Site-diagonal contribution
  if(layer == layerp && cell == cellp){
    Tau = Tau + Tmat;
  }
}



// Correct the single-site t-matrices at negative energies (for transport)
void correct_t(complexsupermatrix &T, double energy, crystal &c, int layer){

  int km, km2;
  int atom;

  complex lfactor, rfactor;

  complexmatrix Tp;

  // Determine the correction factors, ...
  clayer &cl = c.layer[layer];
  cslice &cc = *cl.slice;
  cslab  &cs = *cc.slab;

  // ..., the r.h.s. and the l.h.s. factor
  if(energy <  -cs.v0){
    lfactor = cmplx(0.0, 1.0);
    rfactor = cmplx(0.0, 1.0);
  }
  else if(energy <  0){
    lfactor = cmplx(0.0,  1.0);
    rfactor = cmplx(0.0, -1.0);
  }
  else{
    lfactor = cmplx(1.0, 0.0);
    rfactor = cmplx(1.0, 0.0);
  }

  // Correct at negative energies (otherwise lfactor = rfactor = 1).
  // For all atoms
  if(energy <  0){
    for(atom = 0; atom < cs.natom; atom++){
      // Pick out the t-matrix of one atom, ...
      Tp = T(atom, atom);
      
      // For all (kappa, mue)
      for(km = 0; km < kmmax; km++){
	// For all (kappa, mue)
	for(km2 = 0; km2 < kmmax; km2++){
	  Tp(km, km2) = pow(lfactor, kml[km]) *  Tp(km, km2) * pow(rfactor, kml[km2]);
	}
      }
      
      // ... and write it back
      T(atom, atom) = Tp;
    }
  }
}


// Correct the Green function at negative energies (for transport)
void correct_gf(complexsupermatrix &V, double energy, crystal &c, int layer, int layerp){

  int km, km2;
  int atom, atomp;

  complex lfactor, rfactor;

  complexmatrix Vp;

  // Determine the correction factors, ...
  clayer &cl = c.layer[layer];
  cslice &cc = *cl.slice;
  cslab  &cs = *cc.slab;

  // ..., first the l.h.s. factor, ....
  if(energy <  -cs.v0){
    lfactor = cmplx(0.0, 1.0);
  }
  else if(energy <  0){
    lfactor = cmplx(0.0,  1.0);
  }
  else{
    lfactor = cmplx(1.0, 0.0);
  }

  // ..., then the r.h.s. factor
  clayer &clp = c.layer[layerp];
  cslice &ccp = *clp.slice;
  cslab  &csp = *ccp.slab;

  if(energy <  -csp.v0){
    rfactor = cmplx(0.0, 1.0);
  }
  else if(energy <  0){
    rfactor = cmplx(0.0, -1.0);
  }
  else{
    rfactor = cmplx(1.0, 0.0);
  }


  // Correct at negative energies (otherwise lfactor = rfactor = 1).
  if(energy <  0){
    // For all atoms
    for(atom = 0; atom < cs.natom; atom++){
      // For all atoms
      for(atomp = 0; atomp < csp.natom; atomp++){
	// Pick out the V matrix of one atom, ...
	Vp = V(atomp, atom);
	
	// ... correct it, ...
	// For all (kappa, mue)
	for(km = 0; km < kmmax; km++){
	  // For all (kappa, mue)
	  for(km2 = 0; km2 < kmmax; km2++){
	    Vp(km, km2) = pow(lfactor, kml[km]) *  Vp(km, km2) * pow(rfactor, kml[km2]);
	  }
	}
	
	// ... and write it back
	V(atomp, atom) = Vp;
      }
    }
  }
}




// Transform a Green function from omni to Durham representation, with
// correction (for transport)
void omni2durham_gf_corr(complexsupermatrix &Tau, state &s, crystal &c, int layer, int layerp){

  complexsupermatrix Tmat, Tmatp, Vmat;

  slayer &sl = s.layer[layer];
  sslice &sc = *sl.slice;
  sslab  &ss = *sc.slab;

  slayer &slp = s.layer[layerp];
  sslice &scp = *slp.slice;
  sslab  &ssp = *scp.slab;

  // Right-hand-side t-matrices (omni -> Durham, see note)
  // Tmat  = (cmplx(0, 1) / ss.k)  * ss.T;
  // Tmatp = (cmplx(0, 1) / ssp.k) * ssp.T;
  omni2durham_t2(ss.T,  Tmat,  ss.k);
  omni2durham_t2(ssp.T, Tmatp, ssp.k);

  // Correct the single-site t-matrices
  correct_t(Tmat,  s.ev.re, c, layer);
  correct_t(Tmatp, s.ev.re, c, layerp);

  // Correct the GF matrix 
  Vmat = sl.V[layerp];
  correct_gf(Vmat, s.ev.re, c, layerp, layer);


  // Multiple-scattering contribution
  Tau = Tmatp * Vmat * Tmat;

  // Site-diagonal contribution
  if(layer == layerp){
     Tau = Tau + Tmat;
  }
}


// Transform a Green function from omni to Durham representation, with
// correction (for transport), real space
void omni2durham_gf_rs_corr(complexsupermatrix &Tau, state &s, crystal &c, int layer, int cell, int layerp, int cellp){

  complexsupermatrix Tmat, Tmatp, Vmat;

  slayer &sl = s.layer[layer];
  scell  &se = sl.cell[cell];
  sslice &sc = *sl.slice;
  sslab  &ss = *sc.slab;

  slayer &slp = s.layer[layerp];
  sslice &scp = *slp.slice;
  sslab  &ssp = *scp.slab;

  clayer &clp = c.layer[layerp];

  // Right-hand-side t-matrices (omni -> Durham, see note)
  Tmat  = (cmplx(0, 1) / ss.k)  * ss.T;
  Tmatp = (cmplx(0, 1) / ssp.k) * ssp.T;

  // Correct the single-site t-matrices
  correct_t(Tmat,  s.ev.re, c, layer);
  correct_t(Tmatp, s.ev.re, c, layerp);

  // Correct the GF matrix 
  Vmat = se.V[clp.ncelloffset + cellp];
  correct_gf(Vmat, s.ev.re, c, layerp, layer);


  // Multiple-scattering contribution
  Tau = Tmatp * Vmat * Tmat;

  // Site-diagonal contribution
  if(layer == layerp && cell == cellp){
    Tau = Tau + Tmat;
  }
}
