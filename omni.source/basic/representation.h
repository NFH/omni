// representation.C
//
// Calculation of phase shifts
//

#ifndef _REPRESENTATION_
#define _REPRESENTATION_

#include "complex.h"
#include "cmatrix.h"
#include "csmatrix.h"

#include "job.h"
#include "state.h"
#include "crystal.h"

// Transform a single-site t-matrix from omni to Durham representation
void omni2durham_t(complexmatrix &T, complex k);
void omni2durham_t2(complexsupermatrix &Tin, complexsupermatrix &Tout, complex k);

// Transform a slab t-matrix from omni to Durham representation
void omni2durham_slab_t(sslab &ss, cslab &cs);

// Transform an atomic Green function from omni to Durham representation
void omni2durham_gf(complexsupermatrix &Tau, state &s, crystal &c, int layer, int layerp);

// Transform a Green function from omni to Durham representation, real-space mode
void omni2durham_gf_rs(complexsupermatrix &Tau, state &s, crystal &c, int layer, int cell, int layerp, int cellp);

// Correct the single-site t-matrices at negative energies (for transport)
void correct_t(complexsupermatrix &T, double energy, crystal &c, int layer);

// Transform a Green function from omni to Durham representation, with
// correction (for transport)
void omni2durham_gf_corr(complexsupermatrix &Tau, state &s, crystal &c, int layer, int layerp);

// Transform a Green function from omni to Durham representation, with
// correction (for transport), real space
void omni2durham_gf_rs_corr(complexsupermatrix &Tau, state &s, crystal &c, int layer, int cell, int layerp, int cellp);

#endif
