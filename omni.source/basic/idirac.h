// idirac.h
//
// enthaelt Prototypen der Funktionen aus idirac.C
//

#ifndef _IDIRAC_
#define _IDIRAC_

#include "complex.h"
#include "cmatrix.h"
#include "potential.h"
#include "waves.h"

void idirac(potential &p, complex e, int kp, int mu2, double x, wavefunction &w, double revb, int itr, int ninf, int nturn);
void idiracm(potential &p, complex e, int kp, int mu2, double x, wavefunction &w, double revb, int itr, int ninf, int nturn);

#endif
