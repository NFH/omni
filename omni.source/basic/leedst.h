// leedst.h
//
// enthaelt Prototypen der in leedst.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _LEEDST_
#define _LEEDST_

#include "job.h"
#include "state.h"
#include "beam.h"
#include "crystal.h"

void leedst(job &j, state &u, beamset &b, crystal &c);

#endif
