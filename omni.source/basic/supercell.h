// supercell.h
//

#ifndef _SUPERCELL_
#define _SUPERCELL_

#include "crystal.h"
#include "job.h"

void sc_mod_crystal(job &j, crystal &c, crystal &c2);

#endif
