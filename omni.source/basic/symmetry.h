// symmetry.h
//
// enthaelt Prototypen der in symmetry.C definierten Funktionen
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _SYMMETRY_
#define _SYMMETRY_

#include "job.h"

void symmetry(job &j);


#endif
