// dirac.h
//
// enthaelt Prototypen der Funktionen aus dirac.C
//
// Autor: 1995 Dipl. Phys. Thomas Scheunemann
//        Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _DIRAC_
#define _DIRAC_

#include "potential.h"
#include "state.h"

void diracqw(selem &e, potential &p, double x, int flagw);
void dirac(selem &e, potential &p, double x, int flagw);
void diracm(selem &e, potential &p, double x, int flagw);

#endif
