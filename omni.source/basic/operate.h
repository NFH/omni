//
// Point group operations
//
// Juergen Henk -- 12/5/1997
//
// Pointgroups: 1, 2, 3, 4, m, 2mm, 3m, 4mm

#ifndef _OPERATE_
#define _OPERATE_

#include "vector2.h"

// Number of elements
#define NELEM1   1
#define NELEM2   2
#define NELEM3   3
#define NELEM4   4
#define NELEMM   2
#define NELEM2MM 4
#define NELEM3M  6
#define NELEM4MM 8

vector2 operate1(vector2 vec, int element);
vector2 operate2(vector2 vec, int element);
vector2 operate3(vector2 vec, int element);
vector2 operate4(vector2 vec, int element);
vector2 operatem(vector2 vec, int element);
vector2 operate2mm(vector2 vec, int element);
vector2 operate3m(vector2 vec, int element);
vector2 operate4mm(vector2 vec, int element);

#endif
