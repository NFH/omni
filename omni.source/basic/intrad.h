// intrad.h
//
// enthaelt Prototypen der in intrad.C definierten Funktionen
//
// Autor: 1999 Dipl. Phys. Thomas Scheunemann
//        Gerhard Mercator Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#ifndef _INTRAD_
#define _INTRAD_

#include "state.h"
#include "crystal.h"

void intrad(selem &se, celem &ce);
void intradm(selem &se, celem &ce);
void intradfp(selem &se, celem &ce);

#endif
