// ifbarr.h
//
// enthaelt die Prototypen der in ifbarr.C definierten Funktionen
//

#ifndef _IFBARR_ 
#define _IFBARR_


#include "state.h"
#include "beam.h"
#include "crystal.h"

void ifbarr(crystal &c, state &s, beamset &b, complexmatrix &Ntpp, complexmatrix &Ntpm, complexmatrix &Ntmp, complexmatrix &Ntmm);

#endif
