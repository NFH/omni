// atomics.h
//
// Enthaelt Prototypen der in atomics.C definierten Funktionen
//

#ifndef _TABLES_
#define _TABLES_

#include <iostream>
#include <vector>

#include "job.h"
#include "table.h"

void bartable(const job &j, complextable &t, char *name);
void readktable(const job &j, std::vector<value> &kl, char *name, vector2 &b1, vector2 &b2, int kmode);
void genktable(const job &j, std::vector<value> &kl, double k1min, double k1max, double deltak1, 
               double k2min, double k2max, double deltak2,
               vector2 &b1, vector2 &b2);
void genkcarttable(const job &j, std::vector<value> &kl, double k1min, double k1max, double deltak1, 
		   double k2min, double k2max, double deltak2);
void genklincarttable(const job &j, std::vector<value> &kl, double kminx, double kminy, double kmax, double kmaxy, int nsteps);
void readatable(const job &j, std::vector<value> &kl, char *name);
void genatable(const job &j, std::vector<value> &kl, double tmin, double tmax, double deltat, 
               double pmin, double pmax, double deltap);
void genatilttable(const job &j, std::vector<value> &kl, double thetapmin, double thetapmax, double thetapstep);
void genetable(const job &j, std::vector<value> &el, double emin, double emax, double edel);
void readetable(const job &j, std::vector<value> &el, char *name);
void readetable2(const job &j, std::vector<value> &el, char *name);
void checketable(std::vector<value> &el);
void writektable(std::vector<value> &ktable);
void genanglephtable(const job &j, std::vector<value> &al, double a1min, double a1max, double deltaa1, 
               double a2min, double a2max, double deltaa2);

#endif
