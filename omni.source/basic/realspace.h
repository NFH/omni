//
// realspace.h
//
// Functions for real-space modes

#ifndef _REALSPACE_
#define _REALSPACE_

#include "job.h"
#include "actual.h"
#include "state.h"
#include "crystal.h"

double Distance_rs(crystal &c, int layer, int cell, int atom, int layerp, int cellp, int atomp);
void   Print_Positionsrs(job &j, crystal &c);
double Print_Distancers(crystal &c, int layer, int cell, int atom, int layerp, int cellp, int atomp);
void   SaveMemoryGFrs(job &j, state &s, crystal &c, int startmin, int startmax);
void   SaveMemoryGFrs2(job &j, state &s, crystal &c, int l1, int l2, int l3, int l4);
void   InitCell(job &j, state &s, crystal &c);
void   InitCellT(job &j, state &s, crystal &c);
void   CellV(job &j, actual &a, state &s, crystal &c, int mode);
void   DisplaceTmat(const crystal &c, state &s);


#endif
