// rdirac.h
//
// Dirac equation solver for regular solutions
//

#ifndef _RDIRAC_
#define _RDIRAC_ 

#include "complex.h"
#include "cmatrix.h"
#include "potential.h"
#include "waves.h"

void rdirac(potential &p, complex e, int kp, int mu2, double x, wavefunction &w, double revb, int itr, int icr);
void rdiracm(potential &p, complex e, int kp, int mu2, double x, wavefunction &w, double revb, int itr, int icr);

#endif
