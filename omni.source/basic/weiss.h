// Calculation of the Weiss field; for relativistic DLM
// See Dissertation of Buruzs, TU Vienna, page 19, eq. (2.55).
// Integration over the unit sphere is performed by Gaussian
// integration; see K. Atkinson, J. Austral. Math. Soc. (Series B) 23
// (1982) 332.

#ifndef _WEISS_
#define _WEISS_

#include <iostream>

#include "crystal.h"
#include "job.h"
#include "state.h"

void weissDLM(job &j, actual &a, state &s, crystal &c, state &s2, int ref_mag, std::ostream &out);

#endif
