// blochSC.h
//
// Function for Bloch-wave transmission within a supercell
//

#ifndef _BLOCHSC_
#define _BLOCHSC_

#include <iostream>

#include "job.h"
#include "beam.h"
#include "crystal.h"
#include "state.h"

void blochSC(actual &a, job &j, state &sl, state &sr, beamset &b, crystal &c, std::ostream &os, double &SumTpp_spec, double &SumTpm_spec, double &SumTmp_spec, double &SumTmm_spec, double &SumTpp_diff, double &SumTpm_diff, double &SumTmp_diff, double &SumTmm_diff);

#endif
