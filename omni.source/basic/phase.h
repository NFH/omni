// phase.h
//
// Calculation of phase shifts
//

#ifndef _PHASE_
#define _PHASE_

#include <iostream>

#include "job.h"
#include "state.h"
#include "crystal.h"

void phase(actual &a, state &s, crystal &c, std::ostream &os);

#endif
