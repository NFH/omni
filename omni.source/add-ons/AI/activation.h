//
// Activation functions
//
double activation_function(double x, int fstyle);

// Derivative of the activation functions
double activation_function_deriv(double x, int fstyle);

// Inverse activation functions
double activation_function_inv(double x, int af_style);
