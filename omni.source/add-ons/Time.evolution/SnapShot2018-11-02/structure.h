//
// Structure definitions
//

#include "complex.h"
#include "vector2.h"

// Job parameters
struct job{
  int nlevels;
  int ncells1;
  int ncells2;
  int boundary;

  int nsites;

  double center;
  double width;
  double omega;
  double amplitude;
  int    npertubed;
  int   *perturbed_cell1;
  int   *perturbed_cell2;
  int   *perturbed_site;

  double temperature;
  double mue_system;
  double mue_bath;
  double gamma;

  double tmin;
  double tmax;
  double tstep;
  int    tintegrator;

  int out_control;
};

// Sites
struct site{
  vector2  position;

  int      nlevels;
  complex *energies;
  complex *hoppings;
};

// Cells
struct cell{
  vector2 position;

  int   nsites;
  site *sites;
};

// Chain
struct chain{
  int ncells1;
  int ncells2;
  int nsites;
  int nlevels;

  double  lattice_constant;
  vector2 lattice_vec1;
  vector2 lattice_vec2;

  cell *cells;
};
