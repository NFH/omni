\documentclass[11pt,a4paper]{scrartcl}

\usepackage[pdftex]{graphicx}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{times}
\usepackage{units}
\usepackage{caption2}
\usepackage{wasysym}
\usepackage[ngerman]{babel}

\input{Defines}
\input{Layout}

\begin{document}

\title{Time evolution}
\author{Jürgen Henk}

\maketitle

\section{Preliminaries}
The normalized eigenstates of a ground state Hamiltonian $\operator{H}_{0}$ obey
\begin{align*}
 \operator{H}_{0} \Ket{i} & = \epsilon_{i} \Ket{i}.
\end{align*}
The temporal evolution of the density operator, defined by its matrix elements
\begin{align*}
 \rho_{mn} & \equiv \Ket{m} \Bra{n},
\end{align*}
is given by
\begin{align*}
 \partial_{t} \operator{\rho} & = \cone [\operator{\rho}, \operator{H}_{0}].
\end{align*}
This leads to the equations of motion
\begin{align*}
 \partial_{t} \rho_{mn} & = -\cone (\epsilon_{m} - \epsilon_{n}) \rho_{mn}
\end{align*}
with solutions
\begin{align*}
 \rho_{mn}(t) & = \rho_{mn}(0) \ \euler^{-\cone (\epsilon_{m} - \epsilon_{n}) t}.
\end{align*}
Diagonal elements ($m = n$) are constant. The above result follows as well from the time evolution of the eigenstates, given by the evolution operator
\begin{align*}
 \operator{U}_{0}(t) & \equiv \exp(-\cone \operator{H}_{0} t)
\end{align*}
and 
\begin{align*}
 \operator{\rho}(t) & = \operator{U}_{0}(t) \, \operator{\rho}(0) \, \operator{U}_{0}^{\dagger}(t).
\end{align*}

\section{Perturbation and occupation numbers}
At time $t = 0$, an initial state is prepared by
\begin{align*}
 \rho_{mn}(0) & = p_{mn}(0) \Ket{m} \Bra{n}.
\end{align*}
The weights $p_{mn}(0)$ are interpreted as occupations numbers and thus read
\begin{align*}
 p_{mn}(0) & = \delta_{mn} f(\epsilon_{n}).
\end{align*}
At zero temperature, the Fermi-Dirac distribution $f(\epsilon)$ leads to
\begin{align*}
 p_{mn}(0) & = \delta_{mn} \cdot
 \begin{cases}
  1 & \epsilon_{n} \leq \epsilon_{\mathrm{F}} \\
  0 & \epsilon_{n} > \epsilon_{\mathrm{F}} \\
 \end{cases}.
\end{align*}

At $t > 0$, the perturbation $\operator{V}(t)$ is switched on, leading to the Hamiltonian
\begin{align*}
 \operator{H}(t) & = \operator{H}_{0} + \operator{V}(t).
\end{align*}
The time evolution of the density operator then reads
\begin{align*}
 \partial_{t} \operator{\rho}(t) & = \cone \left[ \operator{\rho}(t), \operator{H}(t) \right]
 \\
 & = \cone \left[ \operator{\rho}(t), \operator{H}_{0} \right]
 + \cone \left[ \operator{\rho}(t), \operator{V}(t) \right].
\end{align*}
For the matrix elements we arrive at
\begin{align*}
 \partial_{t} \rho_{mn}(t) = \cone \left[ \rho_{mn}(t), \operator{H}_{0} \right]
 + \cone \left[ \rho_{mn}(t), \operator{V}(t) \right]
\end{align*}
or
\begin{align*}
 \Ket{m} \partial_{t}  p_{mn}(t) \Bra{n} = & \cone \left[ \Ket{m} p_{mn}(t) \Bra{n}, \operator{H}_{0} \right]
 + \cone \left[ \Ket{m} p_{mn}(t) \Bra{n}, \operator{V}(t) \right]
 \\
 = & - \cone (\epsilon_{m} - \epsilon_{n}) \Ket{m} p_{mn}(t) \Bra{n}
 \\
  & + \cone \Ket{m} p_{mn}(t) \Bra{n | \operator{V}(t)}
    - \cone \Ket{\operator{V}(t) | m} p_{mn}(t) \Bra{n}.
\end{align*}
Multiplying from right with $\Bra{k}$ and from left with $\Ket{l}$ and summing over $m$ and $n$ yields
\begin{align*}
 \sum_{mn} \Braket{k | m} \partial_{t}  p_{mn}(t) \Braket{n | l} 
 = & - \cone (\epsilon_{m} - \epsilon_{n}) \sum_{mn} \Braket{k | m} p_{mn}(t) \Braket{n | l}
 \\
  & + \cone \sum_{mn} \Braket{k | m} p_{mn}(t) \Braket{n | \operator{V}(t) | l}
 \\
   & - \cone \sum_{mn} \Braket{k | \operator{V}(t) | m} p_{mn}(t) \Braket{n | l}.
\end{align*}
Since the eigenstates are orthonormal, it follows
\begin{align*}
 \partial_{t}  p_{kl}(t) 
 = & - \cone (\epsilon_{k} - \epsilon_{l}) p_{kl}(t)
 \\
  & + \cone \sum_{n} p_{kn}(t) \Braket{n | \operator{V}(t) | l}
    - \cone \sum_{m} \Braket{k | \operator{V}(t) | m} p_{ml}(t).
\end{align*}
With the perturbation matrix elements
\begin{align*}
 V_{nl}(t) & \equiv \Braket{n | \operator{V}(t) | l}
\end{align*}
we arrive at the equations of motion
\begin{align*}
 \partial_{t}  p_{kl}(t) 
 & = - \cone (\epsilon_{k} - \epsilon_{l}) p_{kl}(t)
 + \cone \sum_{n} p_{kn}(t) V_{nl}(t) - \cone \sum_{m} V_{km}(t) p_{ml}(t).
\end{align*}
The last two terms induce transitions between the eigenstates.

\paragraph{Check~1.} For $\operator{V}(t) \equiv 0$, we arrive at the time evolution for the ground state Hamiltonian; see above. $\square$

\paragraph{Check~2.} The density operator and the perturbation $\operator{V}(t)$ are hermitian. Thus
\begin{align*}
 p_{kl}(t) = p_{lk}^{\star}(t)
\end{align*}
and 
\begin{align*}
 V_{kl}(t) = V_{lk}^{\star}(t).
\end{align*}
With these, the equation of motion for a diagonal element becomes
\begin{align*}
 \partial_{t}  p_{kk}(t) 
 & = - \cone (\epsilon_{k} - \epsilon_{k}) p_{kk}(t)
 + \cone \sum_{n} p_{kn}(t) V_{nk}(t) - \cone \sum_{m} V_{km}(t) p_{mk}(t)
 \\
 & = \cone \sum_{n} p_{kn}(t) V_{nk}(t) - \cone \sum_{n} V_{kn}(t) p_{nk}(t)
 \\
 & = \cone \sum_{n} \left( p_{kn}(t) V_{nk}(t) - V_{kn}(t) p_{nk}(t) \right) 
 \\
 & = - 2 \sum_{n} \Im \left( p_{kn}(t) V_{nk}(t) \right),
\end{align*}
meaning that $\rho_{kk}(t)$ is real. The equation of motion for a complex-conjugate off-diagonal element
\begin{align*}
 \partial_{t}  p_{kl}^{\star}(t) 
 & = + \cone (\epsilon_{k} - \epsilon_{l}) p_{kl}^{\star}(t)
 - \cone \sum_{n} p_{kn}^{\star}(t) V_{nl}^{\star}(t) + \cone \sum_{m} V_{km}^{\star}(t) p_{ml}^{\star}(t)
\end{align*}
becomes
\begin{align*}
 \partial_{t}  p_{kl}^{\star}(t) 
 & = - \cone (\epsilon_{l} - \epsilon_{k}) p_{lk}(t)
 - \cone \sum_{n} p_{nk}(t) V_{ln}(t) + \cone \sum_{m} V_{mk}(t) p_{lm}(t)
 \\
 & = - \cone (\epsilon_{l} - \epsilon_{k}) p_{lk}(t)
 - \cone \sum_{m} p_{mk}(t) V_{lm}(t) + \cone \sum_{n} V_{nk}(t) p_{ln}(t)
 \\
 & = \partial_{t}  p_{lk}(t),
\end{align*}
meaning that the density operator stays hermitian. $\square$

\paragraph{Example: two-level system.} The system is characterized by two eigenstates of $\operator{H}_{0}$, $\ket{1}$ and $\ket{2}$, with energies $\epsilon_{1} \leq \epsilon_{\mathrm{F}} < \epsilon_{2}$. The equations of motion read
\begin{align*}
 \partial_{t}  p_{11}(t) 
 = & \cone \left[ p_{12}(t) V_{21}(t) - V_{12}(t) p_{21}(t) \right],
 \\ 
 \partial_{t}  p_{12}(t) 
 = & - \cone (\epsilon_{1} - \epsilon_{2}) p_{12}(t)
 \\
 & + \cone \left[ p_{11}(t) V_{12}(t) - V_{11}(t) p_{12}(t) + p_{12}(t) V_{22}(t) - V_{12}(t) p_{22}(t) \right],
 \\
 \partial_{t}  p_{21}(t) 
 = & - \cone (\epsilon_{2} - \epsilon_{1}) p_{21}(t)
 \\
 & + \cone \left[ p_{21}(t) V_{11}(t) - V_{21}(t) p_{11}(t) + p_{22}(t) V_{21}(t) - V_{22}(t) p_{21}(t) \right],
 \\
 \partial_{t}  p_{22}(t) 
 = & \cone \left[ p_{21}(t) V_{12}(t) - V_{21}(t) p_{12}(t) \right].
\end{align*}
Exploiting hermiticity, one writes for the diagonal elements
\begin{align*}
 \partial_{t}  p_{11}(t) 
 = & -2 \Im \left[ p_{12}(t) V_{21}(t) \right],
 \\
 \partial_{t}  p_{22}(t) 
 = & - 2 \Im \left[ p_{21}(t) V_{12}(t) \right].
\end{align*}
Their sum is interpreted as the total number $N$ of occupied (populated) eigenstates, which obeys
\begin{align*}
 \partial_{t} N(t) & = \partial_{t}  p_{11}(t) + \partial_{t}  p_{22}(t)
 \\
 & = -2 \Im \left[ p_{12}(t) V_{21}(t) \right] 
     -2 \Im \left[ p_{21}(t) V_{12}(t) \right]
 \\
 & = -2 \Im \left[ p_{12}(t) V_{21}(t) \right] 
     + 2 \Im \left[ p_{12}(t) V_{21}(t) \right]
 \\
 & = 0,
\end{align*}
that is $N(t)$ constant. And as shown above, the off-diagonal elements are complex-conjugate to each other:
\begin{align*}
 \partial_{t}  p_{12}(t) 
 & = \cone \left\{ V_{12}(t) \left[ p_{11}(t)  - p_{22}(t) \right] - \left[ \epsilon_{1} + V_{11}(t) - \epsilon_{2} - V_{22}(t) \right] p_{12}(t) \right\},
 \\
 \partial_{t}  p_{21}(t) 
 & = - \cone \left\{ V_{21}(t) \left[ p_{11}(t) - p_{22}(t) \right] -  \left[ \epsilon_{1} + V_{11}(t)  - \epsilon_{2} - V_{22}(t) \right] p_{21}(t) \right\},
\end{align*}

In matrix form, the equation of motion reads
\begin{align*}
- \cone \partial_{t}
 \begin{pmatrix}
   p_{11} & p_{12} \\
   p_{21} & p_{22} 
 \end{pmatrix}
 =  &
 \begin{pmatrix}
  p_{11} & p_{12} \\
  p_{21} & p_{22} 
 \end{pmatrix}
 \begin{pmatrix}
  \epsilon_{1} + V_{11} & V_{12} \\
  V_{21} & \epsilon_{2} +V_{22} 
 \end{pmatrix}
 \\ 
 & - 
 \begin{pmatrix}
  \epsilon_{1} + V_{11} & V_{12} \\
  V_{21} & \epsilon_{2} + V_{22} 
 \end{pmatrix}
 \begin{pmatrix}
  p_{11} & p_{12} \\
  p_{21} & p_{22} 
 \end{pmatrix}.
\end{align*}
With the matrix elements of the Hamiltonian
\begin{align*}
 H_{kl} & = \Braket{k | \operator{H} | l} 
 \\
 & = 
 \Braket{k | \operator{H}_{0} | l} 
 + 
 \Braket{k | \operator{V} | l} 
 \\
 & = 
 \epsilon_{l} \, \delta_{kl} + V_{kl},
\end{align*}
we arrive at
\begin{align*}
- \cone \partial_{t}
 \begin{pmatrix}
   p_{11} & p_{12} \\
   p_{21} & p_{22} 
 \end{pmatrix}
 =  &
 \begin{pmatrix}
  p_{11} & p_{12} \\
  p_{21} & p_{22} 
 \end{pmatrix}
 \begin{pmatrix}
  H_{11} & H_{12} \\
  H_{21} & H_{22} 
 \end{pmatrix}
 - 
 \begin{pmatrix}
  H_{11} & H_{12} \\
  H_{21} & H_{22} 
 \end{pmatrix}
 \begin{pmatrix}
  p_{11} & p_{12} \\
  p_{21} & p_{22} 
 \end{pmatrix},
\end{align*}
and in short-hand notation
\begin{align*}
- \cone \partial_{t} \mat{P}
 & = \mat{P} \mat{H} - \mat{H} \mat{P}
 \\
 & = \left[ \mat{P},  \mat{H} \right].
\end{align*}

The time-dependent eigenvalues and -states of the occupation (or population) matrix $\mat{P}$ are calculated from
\begin{align*}
  \begin{pmatrix}
   \pi - p_{11} & - p_{12} \\
    - p_{21} & \pi - p_{22}
  \end{pmatrix}
  \begin{pmatrix}
    \alpha \\ \beta
  \end{pmatrix}
  & = 0.
\end{align*}
The solutions of the characteristic polynomial
\begin{align*}
 ( \pi - p_{11} ) (\pi - p_{22}) - | p_{12} |^{2} = 0
\end{align*}
read
\begin{align*}
 \pi_{\pm} & =  \frac{p_{11} +  p_{22}}{2} \pm \sqrt{ \frac{(p_{11} -  p_{22})^{2}}{4} + | p_{12} |^{2}}
\end{align*}
 or
\begin{align*}
 \pi_{\pm} & = \frac{1}{2} \left( p_{11} +  p_{22} \pm \sqrt{ (p_{11} -  p_{22})^{2} + 4 | p_{12} |^{2}}  \right).
\end{align*}
$\pi_{+} + \pi_{-} = 1$, that is the number of initially occupied states (which is constant, as shown above). 
NB: Without perturbation, $p_{11}(t) = 1$, $p_{22}(t) = 0$, and $p_{12}(t) = 0$ yield $\pi_{+}(t) = 1$ and $\pi_{-}(t) = 0$ for all times $t$, that is a stationary situation. This suggests that the eigenvectors of $\mat{P}(t)$ can be viewed as transition states.

\paragraph{Linear chain of two-level systems.} We consider a linear chain of $N$ sites, with a two-level system at each of these sites. The Hamiltonian for coupling these levels reads
\begin{align*}
\operator{H} & = \sum_{n = 1}^{N} \sum_{\alpha = 1}^{2} \epsilon_{\alpha} c_{n \alpha}^{\dagger} c_{n \alpha} + 
\sum_{n, m} \sum_{\alpha, \beta} t_{\alpha \beta} c_{n \alpha}^{\dagger} c_{m \beta}
\end{align*}
in tight-binding form ($n$ site index, $\alpha$ level index). The density operator not reads
\begin{align*}
  \operator{\rho}_{n \alpha, m \beta} & =
  \Ket{n \alpha} p_{n \alpha, m \beta} \Bra{m \beta}.
\end{align*}
The perturbation can be site-dependent, $V = V_{n \alpha, m \beta}(t)$ with $n = m$ but $\alpha \not= \beta$.

\section{Coupling to a bath -- open systems}
The Lindblad master equation describes the evolution of various types of open quantum systems, for example a system weakly coupled to a Markovian reservoir. The Hamiltonian in the Lindblad master equation is not necessarily equal to that of the bare system Hamiltonian but may also incorporate effective unitary dynamics arising from the interaction of the system with the environment.

A heuristic derivation begins with an open quantum system and converts it into Lindblad form by making the Markovian assumption and expanding in small time. A more physically motivated standard treatment covers three common types of derivations of the Lindbladian starting from a Hamiltonian acting on both the system and environment: the weak coupling limit (described in detail below), the low density approximation, and the singular coupling limit. Each of these relies on specific physical assumptions regarding, e.g., correlation functions of the environment. For example, in the weak coupling limit derivation, one typically assumes that (\i) correlations of the system with the environment develop slowly, (\i\i) excitations of the environment caused by system decay quickly, and (\i\i\i) terms which are oscillating fast when compared to the system's timescale of interest can be neglected. These three approximations are called Born, Markov, and rotating wave, respectively.

The weak-coupling limit derivation assumes a quantum system with a finite number of degrees of freedom coupled to a bath containing an infinite number of degrees of freedom. The system and bath each possess a Hamiltonian written in terms of operators acting only on the respective subspace of the total Hilbert space. These Hamiltonians govern the internal dynamics of the uncoupled system and of the bath. There is a third Hamiltonian that contains products of system and bath operators, thus coupling system and bath. The most general form of this Hamiltonian is
\begin{align*}
	\operator{H} & = \operator{H}_{\mathrm{S}} + \operator{H}_{\mathrm{B}} + \operator{H}_{\mathrm{BS}}.
\end{align*}
The dynamics of the entire system can be described by the Liouville equation of motion
\begin{align*}
 \partial_{t}{\chi } & = -\cone [\operator{H}, \chi ].
\end{align*}
This equation, containing an infinite number of degrees of freedom, is impossible to solve analytically, except in very particular cases. What's more, under certain approximations, the bath degrees of freedom need not be considered, and an effective master equation can be derived in terms of the system density matrix
\begin{align*}
 \operator{\rho} &  \equiv \tr_{\mathrm{B}} \chi.
\end{align*}
The problem can be analysed more easily by moving into the interaction picture, defined by the unitary transformation
\begin{align*}
  \tilde{M} & = U \, M \, U^{\dagger},
\end{align*}
where $M$ is an arbitrary operator, and
\begin{align*}
  U & = \euler^{\cone(H_{\mathrm{S}} + H_{\mathrm{B}}) t}.
\end{align*}
It is straightforward to confirm that the Liouville equation becomes
\begin{align*}
 \partial_{t} \tilde{\chi} & = -\cone [ \tilde{H}_{\mathrm{BS}}, \tilde{\chi}],
\end{align*}
in which the Hamiltonian
\begin{align*}
 \tilde{H}_{\mathrm{BS}} & = \euler^{\cone (H_{\mathrm {S}} + H_{\mathrm{B}}) t} H_{\mathrm{BS}} \euler^{-\cone (H_{\mathrm{S}} +H_{\mathrm {B}}) t}
\end{align*}
is explicitly time dependent. This equation can be integrated directly to give
\begin{align*}
\tilde{\chi}(t) & = \tilde{\chi}(0) - \cone \int_{0}^{t} [\tilde{H}_{\mathrm{BS}}(t'), \tilde{\chi }(t')] \, \mathrm{d}t'.
\end{align*}
This implicit equation for $\tilde{\chi }$ can be substituted back into the Liouville equation to obtain the exact differo-integral equation
\begin{align*}
\partial_{t} \tilde{\chi} & = -\cone [\tilde{H}_{\mathrm{BS}}, \tilde{\chi}(0)] - \int_{0}^{t} [\tilde{H}_{\mathrm{BS}}(t), [\tilde{H}_{\mathrm{BS}}(t'), \tilde {\chi}(t')]] \,\mathrm{d}t'.
\end{align*}
We proceed with the derivation by assuming the interaction is initiated at $t = 0$; at that time there are no correlations between the system and the bath. This implies that the initial condition is factorable as
\begin{align*}
\chi(0) = \rho(0) \, R_{0},
\end{align*}
where $R_{0}$ is the density operator of the bath initially. Since the system and bath Hamiltonians act on different Hilbert subspaces, they commute, and thus in the interaction picture we can write
\begin{align*}
\tilde{\chi} &= \euler^{\cone H_{\mathrm{S}} t} \rho \,\euler^{-\cone H_{\mathrm{S}} t} \, \euler ^{\cone H_{\mathrm{B}} t} R \euler^{-\cone H_{\mathrm{B}} t}, 
\end{align*}
tracing over the bath degrees of freedom,
\begin{align*}
 \tr_{R} \tilde{\chi} = \tilde{\rho}.
\end{align*}
Tracing over the bath degrees of freedom of the aforementioned differo-integral equation yields
\begin{align*}
\partial_{t} \tilde{\rho} & = - \int_{0}^{t} \tr_{R} \{ [\tilde{H}_{\mathrm{BS}}(t), [\tilde{H}_{\mathrm{BS}}(t'), \tilde{\chi}(t')]] \} \,\mathrm{d}t'.
\end{align*} 
This equation is exact for the time dynamics of the system's density matrix but requires full knowledge of the dynamics of the bath degrees of freedom. A simplifying assumption, called the Born approximation, rests on the largeness of the bath and the relative weakness of the coupling, which is to say the coupling of the system to the bath should not significantly alter the bath eigenstates. In this case the full density matrix is factorable for all times as
\begin{align*}
\tilde{\chi}(t) & =\tilde{\rho}(t) \, R_{0}.
\end{align*}
The master equation becomes
\begin{align*}
\partial_{t}{\tilde{\rho}}= - \int_{0}^{t} \tr_{R} [\tilde{H}_{\mathrm{BS}}(t), [\tilde{H}_{\mathrm{BS}}(t'), \tilde{\rho}(t') R_0]]  \,\mathrm{d}t'.
\end{align*}
The equation is now explicit in the system's degrees of freedom but is very difficult to solve. A final assumption is the Born-Markov approximation that the time derivative of the density matrix depends only on its current state and not on its past. This assumption is valid under fast bath dynamics, wherein correlations within the bath are lost extremely quickly; it amounts to replacing $\rho(t') \rightarrow \rho(t)$ on the right hand side of the equation:
\begin{align*}
\partial_{t} \tilde{\rho} & = - \int_{0}^{t} \tr_{R}  [\tilde{H}_{\mathrm{BS}}(t), [\tilde{H}_{\mathrm{BS}}(t'), \tilde{\rho}(t) R_0]]\} \,\mathrm{d}t'.
\end{align*}
If the interaction Hamiltonian is assumed to have the form
\begin{align*}
H_{\mathrm{BS}} & = \sum_{i} \alpha_{i}\Gamma_{i}
\end{align*}
for system operators $\alpha_{i}$ and bath operators $\Gamma_{i}$, the master equation becomes
\begin{align*}
\partial_{t} \tilde{\rho} & = - \sum_{ij} \int_{0}^{t} \tr_{R} \{ [\alpha_{i}(t) \Gamma_{i}(t), [\alpha_{j}(t') \Gamma_{j}(t'), \tilde{\rho}(t) \, R_{0}]] \} \,\mathrm{d}t' .
\end{align*}
which can be expanded as
\begin{align*}
\partial_{t} \tilde{\rho} = - \sum_{ij} \int_{0}^{t} &
\left( \alpha_{i}(t) \alpha_{j}(t') \rho(t) - \alpha_{j}(t') \rho(t) \alpha_{i}(t) \right)
\langle \Gamma_{i}(t) \Gamma_{j}(t') \rangle 
 \\
& + \left( \rho(t) \alpha_{j}(t') \alpha_{i}(t) - \alpha_{i}(t) \rho(t) \alpha_{j}(t') \right)
\langle \Gamma_{j}(t') \Gamma_{i}(t)\rangle \,\mathrm{d}t' .
\end{align*}
The expectation values $\langle \Gamma_{i} \Gamma_{j} \rangle = \tr \{ \Gamma_{i} \Gamma_{j} R_{0} \}$ are with respect to the bath degrees of freedom. By assuming rapid decay of these correlations --- ideally $\langle \Gamma_{i}(t) \Gamma_{j}(t') \rangle \propto \delta(t, t')$ --- we achieve from
\begin{align*}
\partial_{t} \tilde{\rho} = - \sum_{ij} \int_{0}^{t} &
\left( \alpha_{i}(t) \alpha_{j}(t') \rho(t) - \alpha_{j}(t') \rho(t) \alpha_{i}(t) \right)
\delta(t, t')
\\
& + \left( \rho(t) \alpha_{j}(t') \alpha_{i}(t) - \alpha_{i}(t) \rho(t) \alpha_{j}(t') \right)
\delta(t, t') \,\mathrm{d}t'
\end{align*}
that (factor of $1/2$ from taking the $\delta$-distribution at the upper boundary $t$ and renaming summation indices)
\begin{align*}
\partial_{t} \tilde{\rho} & = \frac{1}{2} \sum_{ij} 
\left(  2 \alpha_{i}(t) \rho(t) \alpha_{j}(t) - \alpha_{i}(t) \alpha_{j}(t) \rho(t) + 
 - \rho(t) \alpha_{i}(t) \alpha_{j}(t) \right)
 \\
  & = \sum_{ij} 
 \left( \alpha_{i}(t) \rho(t) \alpha_{j}(t) - \frac{1}{2} \left\{ \alpha_{i}(t) \alpha_{j}(t),  \rho(t) \right\} \right),
\end{align*}
with the anticommutator $\{ a, b\} = a b + b a$. Going back to the Schr\"odinger picture, we arrive at
\begin{align*}
\partial_{t} \rho
& = -\cone [H, \rho] + \sum_{ij} 
\left( \alpha_{i}(t) \rho(t) \alpha_{j}(t) - \frac{1}{2} \left\{ \alpha_{i}(t) \alpha_{j}(t),  \rho(t) \right\} \right).
\end{align*}
with renaming $H_{\mathrm{S}}$ to $H$.

We now assume the system has $N$ states $\Ket{n}$. The density operator then takes the form
\begin{align*}
\rho & = \sum_{nm} \Ket{n} \rho_{nm} \Bra{m}
\end{align*}
and we are dealing with $N^{2}$ operators of the form $\Ket{n} \Bra{m}$, some of which are hermitian conjugate to each other. From these operators we construct an orthonormal basis $\{ A_{n} \}$, $n = 1, \ldots, N^{2}$, of the Hilbert space operators; without loss of generality we may assume further that $A_{N^{2}} \propto 1$. This implies that all other $A$'s are traceless.

The operators $\alpha_{i}$ are now expressed in the basis $\{ A_{n} \}$,
\begin{align*}
\alpha_{i} = \sum_{n} c_{i n} A_{n},
\end{align*}
leading to 
\begin{align*}
\partial_{t} \rho
& = -\cone [H, \rho] + \sum_{ij} \sum_{nm} c_{in} c_{jm}
\left( A_{n} \rho A_{m} - \frac{1}{2} \left\{ A_{m} A_{n},  \rho \right\} \right).
\end{align*}
The operator basis contains both $A_{n}$ and $A_{n}^{\dagger}$ (note that $\rho$ is hermitian and so has to be the right-hand side of the above equation). Thus, be rearranging the summation over $m$ we arrive at
\begin{align*}
\partial_{t} \rho
& = -\cone [H, \rho] + \sum_{nm} \left( \sum_{ij}  c_{in} c_{jm}^{\star} \right)
\left( A_{n} \rho A_{m}^{\dagger} - \frac{1}{2} \left\{ A_{m}^{\dagger} A_{n},  \rho \right\} \right).
\end{align*}
We define the hermitian matrix $\mat{h}$ by
\begin{align*}
h_{nm} & = \sum_{ij} c_{in} c_{jm}^{\star},
\end{align*}
which gives
\begin{align*}
\partial_{t} \rho
& = -\cone [H, \rho] + \sum_{nm} h_{nm}
\left( A_{n} \rho A_{m}^{\dagger} - \frac{1}{2} \left\{ A_{m}^{\dagger} A_{n},  \rho \right\} \right).
\end{align*}
$\mat{h}$ is positive semidefinite.

Further, 
\begin{align*}
\partial_{t} \rho
& = -\cone [H, \rho] + \sum_{nm}^{N^{2} - 1} h_{nm}
\left( A_{n} \rho A_{m}^{\dagger} - \frac{1}{2} \left\{ A_{m}^{\dagger} A_{n},  \rho \right\} \right).
\end{align*}
We delete the columns and rows for $\mat{h}$ that have index $N^{2}$.

Since $\mat{h}$ is positive semidefinite, it can be diagonalized with a unitary transformation $\mat{u}$:
\begin{align*}
\mat{u}^{\dagger} \mat{h} \mat{u} & = 
\begin{bmatrix}
 \gamma_{1} & 0 & \cdots & 0 \\
 0 & \gamma_{2} & \cdots & 0\\ 
 \vdots & \vdots & \ddots & \vdots \\
 0 & 0 & \cdots & \gamma_{{N^{2}-1}}
 \end{bmatrix},
\end{align*}
in which the eigenvalues $\gamma_{i}$ are non-negative. If we define another orthonormal operator basis by
\begin{align*}
L_{i} & \equiv \sum _{j = 1}^{N^{2} - 1} A_{j} u_{ji},
\end{align*}
we can rewrite Lindblad equation in diagonal form:
\begin{align*}
\partial_{t} \rho
& = -\cone [H, \rho] + \sum_{nm}^{N^{2} - 1} h_{nm}
\left( A_{n} \rho A_{m}^{\dagger} - \frac{1}{2} \left\{ A_{m}^{\dagger} A_{n},  \rho \right\} \right)
\\
 & = - \cone [H, \rho ]
+ \sum _{n m i} u_{ji}  \gamma _{i} u_{mi}^{\star}
\left( A_{n} \rho A_{m}^{\dagger}  - \frac {1}{2} \left\{ A_{m}^{\dagger} A_{n}, \rho \right\} \right)
\\
 & = - \cone [H, \rho ]
+ \sum _{i} \gamma _{i}
\left( \sum _{nm} A_{n} u_{ni} \rho A_{m}^{\dagger} u_{mi}^{\star} - \frac {1}{2} \left\{ \sum _{nm}  A_{m}^{\dagger} u_{mi}^{\star} A_{n} u_{ni}, \rho \right\} \right),
\end{align*}
with the result
\begin{align*}
  \partial_{t} \rho & = - \cone [H, \rho ]
  + \sum _{i=1}^{N^{2}-1} \gamma _{i}
  \left( L_{i} \rho L_{i}^{\dagger} - \frac {1}{2} \left\{ L_{i}^{\dagger} L_{i}, \rho \right\} \right).
\end{align*}
The new operators $L_{i}$ are commonly called the Lindblad or jump operators of the system. 

\paragraph{Example.} For a single jump operator $F$ and no unitary evolution, the Lindblad superoperator, acting on the density matrix $\rho$, is
\begin{align*}
 \mathcal{D}[F](\rho) & =F \rho F^{\dagger} - \frac{1}{2} \left( F^{\dagger} F \rho + \rho F^{\dagger} F\right).
\end{align*}
Such a term is found in the Lindblad equation in quantum optics, where it can express absorption or emission of photons from a reservoir.

If one wants to have both absorption and emission, one would need a jump operator for each. This leads to the Lindblad equation describing the damping of a quantum harmonic oscillator coupled to a thermal bath, with jump operators
\begin{align*}
 F_{1} & = a, & \gamma_{1} & = \frac{\gamma}{2} \left(\overline{n} + 1 \right),
\\
 F_{2} & = a^{\dagger}, & \gamma_{2} & = \frac{\gamma}{2} \overline{n}
\end{align*}
$\overline{n}$ is the mean number of excitations in the reservoir damping the oscillator and $\gamma$ is the decay rate. If we add additional unitary evolution generated by the quantum harmonic oscillator Hamiltonian $\omega_{c} a^{\dagger } a$ with frequency $\omega_{c}$, we obtain
\begin{align*}
\partial_{t} \rho & = -\cone [ \omega_{c} a^{\dagger } a, \rho] + \mathcal{D}[F_{1}](\rho) + \mathcal{D}[F_{2}](\rho).
\end{align*}

\paragraph{Example: application to the two-level system.}
We now translate these ideas to the two-level system
\begin{align*}
  H & = \epsilon_{1} \Ket{1} \Bra{1} + \epsilon_{2} \Ket{2} \Bra{2} + V(t)
\end{align*}
discussed above and with the density operator
\begin{align*}
\rho & = \sum_{ij} \Ket{i} p_{ij} \Bra{j}.
\end{align*}
The Hamilton dynamics
\begin{align*}
\partial_{t} \rho & = - \cone [H, \rho]
\end{align*}
with perturbation $V(t)$ has been discussed before.

The jump operators $F_{1} = \Ket{1} \Bra{2}$ and $F_{2} = F_{1}^{\dagger} = \Ket{2} \Bra{1}$ give the Lindbladians
\begin{align*}
 L_{1} & = \mathcal{D}[F_{1}](\rho)
 \\
  & = \Ket{1} \sum_{ij} \Braket{2 | i} p_{ij} \Braket{j | 2} \Bra{1}  - \frac{1}{2} \left( \Ket{2} \sum_{ij} \Braket{2 | i} p_{ij} \Bra{j} + \sum_{ij} \Ket{i} p_{ij} \Braket{j | 2} \Bra{2} \right)
 \\
& = \Ket{1} p_{22} \Bra{1}  - \frac{1}{2} \sum_{i} \left( \Ket{2} p_{2i} \Bra{i} + \Ket{i} p_{i2} \Bra{2} \right)
\\
& = \Ket{1} p_{22} \Bra{1} - \Ket{2} p_{22} \Bra{2} 
- 
\frac{1}{2} 
\left( \Ket{2} p_{21} \Bra{1} + \Ket{1} p_{12} \Bra{2} \right)
\end{align*}
and
\begin{align*}
L_{2} & = \mathcal{D}[F_{2}](\rho)
\\
 & = \Ket{2} \sum_{ij} \Braket{1 | i} p_{ij} \Braket{j | 1} \Bra{2} - \frac{1}{2} \left( \Ket{1} \sum_{ij} \Braket{1 | i} p_{ij} \Bra{j} + \sum_{ij} \Ket{i} p_{ij} \Braket{j | 1} \Bra{1} \right)
\\
& = \Ket{2} p_{11} \Bra{2} - \frac{1}{2} \sum_{i} \left( \Ket{1}  p_{1i} \Bra{i} +  \Ket{i} p_{i1} \Bra{1} \right)
\\
& = \Ket{2} p_{11} \Bra{2}  - \Ket{1} p_{11} \Bra{1}  - \frac{1}{2} \left( \Ket{1} p_{12} \Bra{2} + \Ket{2} p_{21} \Bra{1} \right)
\end{align*}
can be written in matrix form:
\begin{align*}
\mat{L}_{1} & = 
\begin{pmatrix}
 p_{22} & -\frac{1}{2} p_{12} \\
 -\frac{1}{2} p_{21} & -p_{22}
\end{pmatrix}
\end{align*}
and
\begin{align*}
\mat{L}_{2} & = 
\begin{pmatrix}
-p_{11} & -\frac{1}{2} p_{12} \\
-\frac{1}{2} p_{21} & p_{11}
\end{pmatrix}
\end{align*}
(recall that both matrices are traceless). We thus end up with the time evolution 
\begin{align*}
\partial_{t} \mat{P}
& = \cone \left( \mat{P} \mat{H} - \mat{H} \mat{P} \right) 
+ \gamma_{1} \mat{L}_{1}
+ \gamma_{2} \mat{L}_{2}
\end{align*}
with the transition rates $\gamma_{1} \geq 0$ and $\gamma_{2} \geq 0$. It is evident that $\mat{L}_{1}$ increases the occupation of the lower state $\Ket{1}$ and decreases (by the same amount) the occupation of the higher level $\Ket{2}$; this is de-excitation proportional to the occupation of the higher level. On the contrary, $\mat{L}_{2}$ decreases the occupation of the lower state $\Ket{1}$ and increases (by the same amount) the occupation of the higher level $\Ket{2}$; this is excitation proportional to the occupation of the lower level.

To mimick a bosonic heat bath (phonons, magnons), the transition rates can be chosen as
\begin{align*}
 \gamma_{1} & = \gamma \, f_{\mathrm{BE}}(\epsilon_{1}, \mu, T), & \Ket{2} \to \Ket{1},
  \\
 \gamma_{2} & = \gamma \, f_{\mathrm{BE}}(\epsilon_{2}, \mu, T), & \Ket{1} \to \Ket{2},
\end{align*}
that is by a product of a general transistion rate (or system-bath coupling strength) $\gamma$ and the Bose-Einstein distribution function (bath occupation) at the energy of the systems' final state (that is the initial energy level of the bath) of the transition (jump) at the specified temperature $T$. 

If $\gamma_{1} = \gamma_{2}$, then
\begin{align*}
\partial_{t} p_{11} & \propto p_{22} -p_{11}, 
 \\
\partial_{t} p_{22} & \propto p_{11} -p_{22}, 
\end{align*}
which would lead to equal occupation of both levels. \qed

\paragraph{Example continued: transition between two levels in the presence of more levels.}
The Hamiltonian now reads
\begin{align*}
 H & = \sum_{i = 1}^{N} \epsilon_{i} \Ket{i}\Bra{i} + V(t),
\end{align*}
with $N \geq 2$. We assume that the jump operators act only on the (deliberately) chosen states $\Ket{1}$ and $\Ket{2}$, as before:
$F_{1} = \Ket{1} \Bra{2}$ and $F_{2} = F_{1}^{\dagger} = \Ket{2} \Bra{1}$. The Lindbladians then become
\begin{align*}
L_{1} & = \mathcal{D}[F_{1}](\rho)
\\
& = F_{1} \rho F_{1}^{\dagger} - \frac{1}{2} \left(F_{1}^{\dagger} F_{1} \rho + \rho F_{1}^{\dagger} F_{1} \right)
\\
& = \Ket{1} \sum_{ij} \Braket{2 | i} p_{ij} \Braket{j | 2} \Bra{1}  - \frac{1}{2} \left( \Ket{2} \sum_{ij} \Braket{2 | i} p_{ij} \Bra{j} + \sum_{ij} \Ket{i} p_{ij} \Braket{j | 2} \Bra{2} \right)
\\
& = \Ket{1} p_{22} \Bra{1}  - \frac{1}{2} \sum_{i} \left( \Ket{2} p_{2i} \Bra{i} + \Ket{i} p_{i2} \Bra{2} \right)
\end{align*}
and 
\begin{align*}
L_{2} & = \Ket{2} \sum_{ij} \Braket{1 | i} p_{ij} \Braket{j | 1} \Bra{2}  - \frac{1}{2} \left( \Ket{1} \sum_{ij} \Braket{1 | i} p_{ij} \Bra{j} + \sum_{ij} \Ket{i} p_{ij} \Braket{j | 1} \Bra{1} \right)
\\
& = \Ket{2} p_{11} \Bra{2}  - \frac{1}{2} \sum_{i} \left( \Ket{1} p_{1i} \Bra{i} + \Ket{i} p_{i1} \Bra{1} \right).
\end{align*}
This means that the occupation of all other states is affected by transitions between states $\Ket{1}$ and $\Ket{2}$. In matrix form
\begin{align*}
\mat{L}_{1} & = 
\begin{pmatrix}
p_{22} & -\frac{1}{2} p_{12} &  0                   & 0 & \cdots & 0 \\
-\frac{1}{2} p_{21} & -p_{22} & -\frac{1}{2} p_{23} & -\frac{1}{2} p_{24} & \cdots & -\frac{1}{2} p_{2N} \\
         0   & -\frac{1}{2} p_{32} & 0 & 0 & \cdots &   0 \\
         0   & -\frac{1}{2} p_{42} & 0 & 0 & \cdots &   0 \\
         \vdots   & \vdots     & \vdots  &  \vdots & \cdots &   \vdots \\
         0   & -\frac{1}{2} p_{N2} & 0 & 0 & \cdots &   0 \\
\end{pmatrix}
\end{align*}
and
\begin{align*}
\mat{L}_{2} & = 
\begin{pmatrix}
-p_{11} & -\frac{1}{2} p_{12} &  -\frac{1}{2} p_{13}  & -\frac{1}{2} p_{14} & \cdots & -\frac{1}{2} p_{1N} \\
-\frac{1}{2} p_{21} & p_{11} & 0   & 0  & \cdots & 0  \\
 -\frac{1}{2} p_{31} & 0 & 0 & 0 & \cdots &   0 \\
 -\frac{1}{2} p_{41} & 0 & 0 & 0 & \cdots &   0 \\
  \vdots     & \vdots  &  \vdots & \vdots & \cdots &   \vdots \\
 -\frac{1}{2} p_{N1} & 0 & 0 & 0 & \cdots &   0 \\
\end{pmatrix}.
\end{align*}

For jump operators $F_{1} = \Ket{n} \Bra{m}$ and $F_{2} = \Ket{m} \Bra{n}$ the Lindbladians read
\begin{align*}
L_{1} & = \Ket{n} p_{mm} \Bra{n}  - \frac{1}{2} \sum_{i} \left( \Ket{m} p_{mi} \Bra{i} + \Ket{i} p_{im} \Bra{m} \right),
\\
L_{2} & = \Ket{m} p_{nn} \Bra{m}  - \frac{1}{2} \sum_{i} \left( \Ket{n} p_{ni} \Bra{i} + \Ket{i} p_{in} \Bra{n} \right).
\end{align*}


\paragraph{Example continued: application to the two-level system}
In the preceding example, $\Ket{1}$ and $\Ket{2}$ are eigenstates of the Hamilonian $H_{0}$,
\begin{align*}
H_{0} \Ket{i} & = \epsilon_{i} \Ket{i}, \quad i = 1, 2.
\end{align*}
We now consider a change of the basis of the Hilbert space mediated by a time-independent unitary transformation
\begin{align*}
\Ket{i} & = \sum_{j} \Ket{j}' U_{ji} , \quad i = 1, 2,
\\
\Bra{i} & = \sum_{j} U_{ij}^{\star} \Bra{j}', \quad i = 1, 2.
\end{align*}
The density operator then becomes
\begin{align*}
\rho & = \sum_{ij} \Ket{i} p_{ij} \Bra{j}
= \sum_{ij} \sum_{mn} \Ket{m}' U_{mi} p_{ij} U_{jn}^{\star} \Bra{n}'
= \sum_{mn} \Ket{m}' \left(\sum_{ij}  U_{mi} p_{ij} U_{jn}^{\star} \right) \Bra{n}'
\\
& = \sum_{mn} \Ket{m}' p_{mn}' \Bra{n}'.
\end{align*}
Thus,
\begin{align*}
\mat{P}' & = \mat{U} \mat{P} \mat{U}^{\dagger}
\end{align*}
or
\begin{align*}
\mat{P} & = \mat{U}^{\dagger} \mat{P}' \mat{U}.
\end{align*}
The same relations hold for any operator. The time evolution
\begin{align*}
\partial_{t} \mat{P}
& = \cone \left( \mat{P} \mat{H} - \mat{H} \mat{P} \right),
\end{align*}
with $\mat{H} = \mat{H}_{0} + \mat{V}(t)$ and $\mat{H}' = \mat{U} \mat{H} \mat{U}^{\dagger}$, becomes
\begin{align*}
\partial_{t} \mat{U}^{\dagger} \mat{P}' \mat{U} & = \cone \left( \mat{U}^{\dagger} \mat{P}' \mat{U} \mat{U}^{\dagger} \mat{H}' \mat{U} - \mat{U}^{\dagger} \mat{H}' \mat{U} \mat{U}^{\dagger} \mat{P}' \mat{U} \right),
\\
\mat{U}^{\dagger} (\partial_{t} \mat{P}') \mat{U} & = \cone \left( \mat{U}^{\dagger} \mat{P}' \mat{H}' \mat{U} - \mat{U}^{\dagger} \mat{H}' \mat{P}' \mat{U} \right),
\\
\mat{U}^{\dagger} (\partial_{t} \mat{P}') \mat{U} & = \cone \mat{U}^{\dagger} \left(  \mat{P}' \mat{H}' - \mat{H}' \mat{P}' \right) \mat{U} ,
\\
\partial_{t} \mat{P}' & = \cone \left(  \mat{P}' \mat{H}' - \mat{H}' \mat{P}' \right).
\end{align*}

The Lindblad superoperator
\begin{align*}
\mathcal{D}[F](\rho) & =F \rho F^{\dagger} - \frac{1}{2} \left( F^{\dagger} F \rho + \rho F^{\dagger} F\right).
\end{align*}
for the new jump operator $F_{1}' = \Ket{1}' \Bra{2}'$ gives the new Lindbladian
\begin{align*}
L_{1}' & = \Ket{1}' p_{22}' \Bra{1}' - \Ket{2}' p_{22}' \Bra{2}'
- 
\frac{1}{2} 
\left( \Ket{2}' p_{21}' \Bra{1}' + \Ket{1}' p_{12}' \Bra{2}' \right),
\end{align*}
that is the old one but for a transition between the new (primed) basis states. Similarly,
\begin{align*}
L_{2}' & = \Ket{2}' p_{11}' \Bra{2}'  - \Ket{1}' p_{11}' \Bra{1}'  - \frac{1}{2} \left( \Ket{1}' p_{12}' \Bra{2}' + \Ket{2}' p_{21}' \Bra{1}' \right).
\end{align*}
The time evolution then becomes
\begin{align*}
\partial_{t} \mat{P}'
& = \cone \left( \mat{P}' \mat{H}' - \mat{H}' \mat{P}' \right) 
+ \gamma_{1}' \mat{L}_{1}'
+ \gamma_{2}' \mat{L}_{2}'
\end{align*}
with the transition rates $\gamma_{1}' \geq 0$ and $\gamma_{2}' \geq 0$.

If one would describe transitions between the eigenstates in the new basis, then
\begin{align*}
L_{1} = & \Ket{1} \Braket{2 | \rho | 2} \Bra{1}  - \frac{1}{2} \left( \Ket{2}  \Bra{2} \rho + \rho \Ket{2} \Bra{2} \right)
\\
 = & \sum_{ijkl} \Ket{i}' U_{i1} U_{2j}^{\star} \Braket{j' | \rho | k'} U_{k2} U_{1l}^{\star} \Bra{l}'
\\ &  - \frac{1}{2} \sum_{ij} \left( \Ket{i}' U_{i2} U_{2j}^{\star}  \Bra{j}' \rho + \rho \Ket{i}' U_{i2} U_{2j}^{\star} \Bra{j}' \right)
\\
= & \sum_{ijkl} \Ket{i}' U_{i1} U_{2j}^{\star} P_{j k}' U_{k2} U_{1l}^{\star} \Bra{l}'
\\ &  - \frac{1}{2} \sum_{ijk} \left( \Ket{i}' U_{i2} U_{2j}^{\star}  P_{j k}' \Bra{k}' +  \Ket{k}' P_{k i}' U_{i2} U_{2j}^{\star} \Bra{j}' \right).
\end{align*}

\appendix


\section{Time evolution of bulk systems in tight-binding formulation}
Consider a bulk system with Bloch states in tight-binding form,
\begin{align*}
\Ket{n\vec{k}} & = \sum_{\alpha} c_{n \alpha}(\vec{k}) \Ket{\alpha \vec{k}},
\end{align*}
in which $n$ is the band index and $\vec{k}$ the wavevector. These states are eigenstates of the unperturbed Hamiltonian with energies $\epsilon_{n}(\vec{k})$. $\Ket{\alpha \vec{k}}$ is the Bloch function
\begin{align*}
\Ket{\alpha \vec{k}} & = \frac{1}{\sqrt{N}} \sum_{l} \Braket{\vec{R}_{l} | \vec{k}} \Ket{\alpha l},
\end{align*}
which is an expansion into local orbitals with multi-index $\alpha$ (shell, spherical harmonic, and spin orientation) at lattice site $\vec{R}_{l}$. $\Ket{\vec{k}}$ is a plane wave, with phase factor $\Braket{\vec{R}_{l} | \vec{k}} = \exp(\cone \vec{k} \cdot \vec{R}_{l})$, and $N$ is the number of lattice sites.

In the initial configuration, all Bloch states are occupied according to the Fermi-Dirac distribution $f(\epsilon - \epsilon_{\mathrm{F}}; T)$ at temperature $T$. More precisely, the density operator $\operator{\rho}(\vec{k}; 0)$ at time $t = 0$ reads
\begin{align*}
\rho_{mn}(\vec{k}; 0) & = 
\Ket{m \vec{k}} p_{mn}(\vec{k}; 0) \Bra{n \vec{k}},
\end{align*}
with the occupations given by
\begin{align*}
p_{mn}(\vec{k}; 0) & = \delta_{mn} \, f(\epsilon_{m}(\vec{k}) - \epsilon_{\mathrm{F}}; T).
\end{align*}

Now consider a time-dependent and lattice-periodic local perturbation, $\operator{V}(\vec{r}; \vec{t}) = \operator{V}(\vec{r} + \vec{R}; \vec{t})$ for all times $t$.\footnote{This scenario applies to a laser pulse whose spot size is much larger than a cell.} Its matrix elements 
\begin{align*}
\Braket{\alpha \vec{k} | \operator{V}(\vec{r}; t) | \beta \vec{k}'}
& = 
\frac{1}{N}
\sum_{lm} 
\Braket{\vec{k} | \vec{R}_{l}} \Braket{\vec{R}_{m} | \vec{k}'} \Braket{\alpha l | \operator{V}(\vec{r}; t) | \beta m}
\end{align*}
contain the explicit spatial dependence in the orbital matrix elements (last term), which complicates working in reciprocal space. Therefore, we perform a Fourier transformation of the perturbation:
\begin{align*}
\operator{V}(\vec{r}; \vec{t})
& =
\sum_{\vec{G}} v(\vec{G}; t) \Braket{\vec{r} | \vec{G}},
\\
v(\vec{Q}; \vec{t})
& = 
\frac{1}{\Omega}
\int_{\Omega} V(\vec{r}; t) \Braket{\vec{G} | \vec{r}} \,\mathrm{d}r^{3}
\end{align*}
($\vec{G}$ vector of the reciprocal lattice, $\Omega$ cell volume). This yields for the orbital matrix elements
\begin{align*}
\Braket{\alpha l | \operator{V}(\vec{r}; t) | \beta m} 
& = 
\sum_{\vec{G}} v(\vec{G}; t) 
\int \phi_{\alpha l}^{\star}(\vec{r} - \vec{R}_{l}) \,\euler^{\cone \vec{G} \cdot \vec{r}} \phi_{\beta m}(\vec{r} - \vec{R}_{m})
\,\mathrm{d}r^{3}
\\
& = 
\sum_{\vec{G}} v(\vec{q}; t) 
\int \phi_{\alpha l}^{\star}(\vec{r}) \,\euler^{\cone \vec{G} \cdot (\vec{r} + \vec{R}_{l})} \phi_{\beta m}(\vec{r}  + \vec{R}_{l} - \vec{R}_{m})
\,\mathrm{d}r^{3}
\\
& = 
\sum_{\vec{G}} v(\vec{q}; t) 
\,\euler^{\cone \vec{G} \cdot \vec{R}_{l}}
\int \phi_{\alpha l}^{\star}(\vec{r}) \,\euler^{\cone \vec{G} \cdot \vec{r}} \phi_{\beta m}(\vec{r}  + \vec{R}_{l} - \vec{R}_{m})
\,\mathrm{d}r^{3}
\\
& = 
\sum_{\vec{G}} v(\vec{q}; t) 
\int \phi_{\alpha l}^{\star}(\vec{r}) \,\euler^{\cone \vec{G} \cdot \vec{r}} \phi_{\beta m}(\vec{r}  + \vec{R}_{l} - \vec{R}_{m})
\,\mathrm{d}r^{3}
\end{align*}

Defining the matrix elements
\begin{align*}
V_{nl}(\vec{k}; t) & \equiv \Braket{n \vec{k} | \operator{V}(t) | l \vec{k}}
\\
& = \sum_{\alpha \beta}
c_{n \alpha}^{\star}(\vec{k})  \Braket{\alpha \vec{k} | \operator{V}(t) | \beta \vec{k}} c_{l \beta}(\vec{k})
\end{align*}
for a momentum-conserving perturbation we have the equations of motion
\begin{align*}
\partial_{t}  p_{nl}(\vec{k}; t) 
= & - \cone (\epsilon_{n}(\vec{k}) - \epsilon_{l}(\vec{k})) p_{nl}(\vec{k};t)
\\
& + \cone \sum_{m} p_{nm}(\vec{k}; t) V_{ml}(\vec{k}; t) - \cone \sum_{m} V_{nm}(\vec{k}; t) p_{ml}(\vec{k}; t).
\end{align*}
The last two terms induce transitions between the Bloch states.

\end{document}