//
// Functions for the Lindbladian
//

#include "complex.h"
#include "cmatrix.h"
#include "distribution.h"

#include "structure.h"
#include "index.h"

// If uncommented, include the Pauli principle for transitions
//#define PAULI

// Update the Lindblad jump operators L
// -- OBS: Only for two-level systems
void update_Lindbladian(job &jp, cluster &c, complexmatrix &L, complexmatrix &P, complexmatrix &H){

  int iup, idn, ipup, ipdn, jup, jdn;
  int icell, icell1, icell2, icell3, isite;

  static int flag = 0;

  complex e, gamma;

  complexmatrix L1, L2;

#ifdef PAULI
  if(flag == 0){
    cout << "MESSAGE from update_Lindbladian(): using Pauli update" << endl;
    flag = 1;
  }
#else
  if(flag == 0){
    cout << "MESSAGE from update_Lindbladian(): using standard update" << endl;
    flag = 1;
  }
#endif

  // Initialize the Lindbladians
  // Factor 2 for spin
  L1.setsize(2 * c.nlevels, 2 * c.nlevels);
  L2.setsize(2 * c.nlevels, 2 * c.nlevels);

  // De-excitation
  // -- Initialise the Lindbladian
  L1 = 0.0;
  // -- For all cells
  for(icell1 = 0; icell1 < c.ncells1; icell1++){
    for(icell2 = 0; icell2 < c.ncells2; icell2++){
      for(icell3 = 0; icell3 < c.ncells3; icell3++){
        icell = cell_index(icell1, icell2, icell3, c);

        // -- For all sites of this cell
        for(isite = 0; isite < c.cells[icell].nsites; isite++){
		  // Spin-up - spin-up
          iup  = orbital_index(icell1, icell2, icell3, isite, 0, c);
          ipup = orbital_index(icell1, icell2, icell3, isite, 1, c);

          // Energy difference of the two levels
          e = abs(H(ipup, ipup) - H(iup, iup));

          // Coupling strength * (occupation of the bath at that energy difference + 1)
          gamma = jp.gamma * (BoseEinstein(e, jp.mue_bath, jp.temperature) + 1);
#ifdef PAULI
          gamma *= (1.0 - P(iup, iup)) * P(ipup, ipup);
#endif
          L1(iup, iup) = L1(iup, iup) + gamma * P(ipup, ipup);

          for(jup = 0; jup <  c.nlevels; jup++){
            L1(ipup, jup)  = L1(ipup, jup)  - 0.5 * gamma * P(ipup, jup);
            L1(jup,  ipup) = L1(jup,  ipup) - 0.5 * gamma * P(jup,  ipup);
          }

    	  // Spin-dn - spin-dn
          idn  = orbital_index(icell1, icell2, icell3, isite, 0, c) + c.nlevels;
          ipdn = orbital_index(icell1, icell2, icell3, isite, 1, c) + c.nlevels;

          // Energy difference of the two levels
          e = abs(H(ipdn, ipdn) - H(idn, idn));

          // Coupling strength * (occupation of the bath at that energy difference + 1)
          gamma = jp.gamma * (BoseEinstein(e, jp.mue_bath, jp.temperature) + 1);
#ifdef PAULI
          gamma *= (1.0 - P(idn, idn)) * P(ipdn, ipdn);
#endif
          L1(idn, idn) = L1(idn, idn) + gamma * P(ipdn, ipdn);

          for(jdn = c.nlevels; jdn < 2 * c.nlevels; jdn++){
            L1(ipdn, jdn)  = L1(ipdn, jdn)  - 0.5 * gamma * P(ipdn, jdn);
            L1(jdn,  ipdn) = L1(jdn,  ipdn) - 0.5 * gamma * P(jdn,  ipdn);
          }
        }
      }
    }
  }

  // Excitation
  // -- Initialise the Lindbladian
  L2 = 0.0;
  // -- For all cells
  for(icell1 = 0; icell1 < c.ncells1; icell1++){
    for(icell2 = 0; icell2 < c.ncells2; icell2++){
      for(icell3 = 0; icell3 < c.ncells3; icell3++){
        icell = cell_index(icell1, icell2, icell3, c);

        // -- For all sites of this cell
        for(isite = 0; isite < c.cells[icell].nsites; isite++){
          // Spin-up - spin-up
		  iup  = orbital_index(icell1, icell2, icell3, isite, 0, c);
          ipup = orbital_index(icell1, icell2, icell3, isite, 1, c);

          // Energy difference of the two levels
          e = abs(H(ipup, ipup) - H(iup, iup));

          // Coupling strength * occupation of the bath at that energy difference
          gamma = jp.gamma * BoseEinstein(e, jp.mue_bath, jp.temperature);
#ifdef PAULI
          gamma *= (1.0 - P(ipup, ipup)) * P(iup, iup);
#endif

          L2(ipup, ipup) = L2(ipup, ipup) + gamma * P(iup, iup);

          for(jup = 0; jup < c.nlevels; jup++){
            L2(iup, jup) = L2(iup, jup) - 0.5 * gamma * P(iup, jup);
            L2(jup, iup) = L2(jup, iup) - 0.5 * gamma * P(jup, iup);
          }

          // Spin-dn - spin-dn
		  idn  = orbital_index(icell1, icell2, icell3, isite, 0, c) + c.nlevels;
          ipdn = orbital_index(icell1, icell2, icell3, isite, 1, c) + c.nlevels;

          // Energy difference of the two levels
          e = abs(H(ipdn, ipdn) - H(idn, idn));

          // Coupling strength * occupation of the bath at that energy difference
          gamma = jp.gamma * BoseEinstein(e, jp.mue_bath, jp.temperature);
#ifdef PAULI
          gamma *= (1.0 - P(ipdn, ipdn)) * P(idn, idn);
#endif

          L2(ipdn, ipdn) = L2(ipdn, ipdn) + gamma * P(idn, idn);

          for(jdn = c.nlevels; jdn < 2 * c.nlevels; jdn++){
            L2(idn, jdn) = L2(idn, jdn) - 0.5 * gamma * P(idn, jdn);
            L2(jdn, idn) = L2(jdn, idn) - 0.5 * gamma * P(jdn, idn);
          }
        }
      }
    }
  }

  // Sum up
  L = L1 + L2;
}

#undef PAULI
