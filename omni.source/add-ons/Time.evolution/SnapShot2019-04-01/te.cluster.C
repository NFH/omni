//
// Time evolution - 3D cluster w/ tight binding model
//
// 2019-03-19 JH
//

#include <iostream>
using namespace std;

#include <fstream>
#include <iomanip>
#include <math.h>
#include <string>

#include "platform.h"

#include "cmatrix.h"
#include "constants.h"
#include "complex.h"
#include "distribution.h"

#include "structure.h"
#include "helper.h"
#include "index.h"
#include "hamiltonian.h"
#include "lindbladian.h"
#include "perturbation.h"

// Uncomment if eigenvalues should be printed
#define EIGEN

// Read job parameters
void read_parameters(job &jp){

  int iperturbed;

  char buf[160];

  ifstream inp;

  cout << endl;
  cout << "MESSAGE from read_parameters(): reading job parameters from file te.cluster.jp.in ... " << endl;
  inp.open("te.cluster.jp.in");

  // Get the pulse shape
  inp >> jp.center >> jp.width >> jp.omega >> jp.amplitude;
  inp.getline(buf, sizeof(buf));
  // -- in atomic time units
  jp.center /= AU_TIME;
  jp.width  /= AU_TIME;
  // -- in atomic time units and angular frequency
  jp.omega  *= 2.0 * M_PI * AU_TIME;
  // -- in Hartree
  jp.amplitude /= AU_ENERGY;
  cout << "pulse shape: center = " << jp.center * AU_TIME << " fs";
  cout << " // width = " << jp.width * AU_TIME << " fs";
  cout << " // omega = " << jp.omega / (2.0 * M_PI * AU_TIME) << " / fs";
  cout << " (" << jp.omega / AU_TIME << " / fs angular)";
  cout << " (" << jp.omega  * AU_ENERGY / (2.0 * M_PI) << " eV)";
  cout << " // amplitude = " << jp.amplitude * AU_ENERGY << " eV" << endl;
  // Get the perturbed sites
  // --- Read the number of perturbed sites
  inp >> jp.nperturbed;
  inp.getline(buf, sizeof(buf));
  cout << "number of perturbed sites = " << jp.nperturbed << endl;
  jp.perturbed_cell1 = new int[jp.nperturbed];
  jp.perturbed_cell2 = new int[jp.nperturbed];
  jp.perturbed_cell3 = new int[jp.nperturbed];
  jp.perturbed_site  = new int[jp.nperturbed];
  // -- Read the indices of the perturbed sites
  for(iperturbed = 0; iperturbed < jp.nperturbed; iperturbed++){
    inp >> jp.perturbed_cell1[iperturbed] >> jp.perturbed_cell2[iperturbed] >> jp.perturbed_cell3[iperturbed] >> jp.perturbed_site[iperturbed];
    inp.getline(buf, sizeof(buf));
    cout << "perturbed site " << iperturbed << ":";
    cout << " cell " << jp.perturbed_cell1[iperturbed] << " " << jp.perturbed_cell2[iperturbed] << " " << jp.perturbed_cell3[iperturbed];
    cout << " site " << jp.perturbed_site[iperturbed] << endl;
  }
  cout << endl;

  // Get the temperature (in K) and chemical potentials (in eV)
  inp >> jp.temperature >> jp.mue_system >> jp.mue_bath >> jp.init_flag;
  inp.getline(buf, sizeof(buf));
  // -- in Hartree
  jp.temperature *= BLTZ;
  jp.mue_system  /= AU_ENERGY;
  jp.mue_bath    /= AU_ENERGY;
  cout << "occupation: temperature = " << jp.temperature / BLTZ << " K";
  cout << " (" << jp.temperature * AU_ENERGY << " eV)";
  cout << " // chemical potential system = " << jp.mue_system * AU_ENERGY << " eV";
  cout << " // chemical potential bath = "   << jp.mue_bath   * AU_ENERGY << " eV" << endl;
  // Get the init flag
  switch (jp.init_flag){
    case 0:
      cout << "initialising occupation from distribution function." << endl;
      break;
    case 1:
      cout << "initialising occupation from file te.occupation.matrix.in" << endl;
      break;
    default:
      cerr << "ERROR: wrong initialisation flag: " << jp.init_flag << endl;
      myexit(1);
  }

  // Get the coupling strength system-bath
  inp >> jp.gamma;
  inp.getline(buf, sizeof(buf));
  cout << "coupling system-bath: coupling strength = " << jp.gamma << endl;

  // Get the time range (in fs)
  inp >> jp.tmin >> jp.tmax >> jp.tstep >> jp.tintegrator;
  inp.getline(buf, sizeof(buf));
  // -- in atomic time units
  jp.tmin  /= AU_TIME;
  jp.tmax  /= AU_TIME;
  jp.tstep /= AU_TIME;
  cout << "evolution: integration from tmin = " << jp.tmin * AU_TIME << " fs";
  cout << " to tmax =  " << jp.tmax * AU_TIME << " fs";
  cout << " in tstep = " << jp.tstep * AU_TIME << " fs";
  cout << " using method " << jp.tintegrator;
  switch(jp.tintegrator){
    case 0:
      cout << " (Euler)";
      break;
    case 1:
      cout << " (Heun)";
      break;
    case 2:
      cout << " (explicit midpoint)";
      break;
    case 3:
      cout << " (implicit midpoint)";
      break;
    default:
      cerr << " ERROR: wong!";
      myexit(1);
      break;
  }
  cout << endl << endl;

  // Get the output control parameter
  inp >> jp.out_control;
  cout << "output control: writing every " << jp.out_control << "-th data set to disk" << endl;

  inp.close();

  cout << "MESSAGE from read_parameters(): parameters read." << endl;
  cout << endl;
}



// Initialize the cluster
void init_cluster(cluster &c, job &jp){

  int icell, icell1, icell2, icell3, isite, itb_params, nsites, norbitals, i_orb, sum_level;

  int *orbital_types;

  vector3 displacement;

  char buf[160];

  ifstream inp;

  cout << "MESSAGE from init_cluster(): initialising the cluster ..." << endl;

  // Set the energy levels and hoppings for all sites
  cout << "MESSAGE from init_cluster(): reading geometry from file te.cluster.geo.in ..." << endl;
  inp.open("te.cluster.geo.in");

  // Get the number of cells, sites, boundary conditions
  inp >> c.ncells1 >> c.ncells2 >> c.ncells3 >> nsites >> c.boundary1 >> c.boundary2 >> c.boundary3;
  inp.getline(buf, sizeof(buf));
  cout << "number of cells in 1-direction = " << c.ncells1;
  cout << " // number of cells in 2-direction = " << c.ncells2;
  cout << " // number of cells in 3-direction = " << c.ncells3;
  // Total number of cells
  c.ncells = c.ncells1 * c.ncells2 * c.ncells3;
  cout << " // total number of cells = " << c.ncells  << endl;

  // Create the cells
  c.cells = new cell[c.ncells];

  cout << "boundary condition in 1-direction = " << c.boundary1;
  switch(c.boundary1){
  // Closed (ring)
   case 1:
     cout << " (ring)";
     break;
   // Chain
   case 0:
     cout << " (chain)";
     break;
   default:
     cerr << " ERROR: wrong!" << endl;
     myexit(1);
     break;
   }
   cout << " // boundary condition in 2-direction = " << c.boundary2;
   switch(c.boundary2){
   // Closed (ring)
    case 1:
      cout << " (ring)";
      break;
    // Chain
    case 0:
      cout << " (chain)";
      break;
    default:
      cerr << " ERROR: wrong!" << endl;
      myexit(1);
      break;
    }
    cout << " // boundary condition in 3-direction = " << c.boundary3;
    switch(c.boundary3){
    // Closed (ring)
     case 1:
       cout << " (ring)";
       break;
     // Chain
     case 0:
       cout << " (chain)";
       break;
     default:
       cerr << " ERROR: wrong!" << endl;
       myexit(1);
       break;
     }
  cout << endl;
  // Check number of cells
  if(c.ncells1 < 1){
    cerr << "ERROR: number of cells too small: " << c.ncells1 << endl;
    cerr << "ERROR: ncells1 should be >= 1." << endl;
    myexit(1);
  }
  if(c.ncells2 < 1){
    cerr << "ERROR: number of cells too small: " << c.ncells2 << endl;
    cerr << "ERROR: ncells2 should be >= 1." << endl;
    myexit(1);
  }
  if(c.ncells3 < 1){
    cerr << "ERROR: number of cells too small: " << c.ncells3 << endl;
    cerr << "ERROR: ncells3 should be >= 1." << endl;
    myexit(1);
  }

  // Number of sites
  cout << "number of sites per cell = " << nsites;
  // Check number of sites
  if(nsites < 1){
    cerr << "ERROR: number of sites too small: " << nsites << endl;
    cerr << "ERROR: nsites should be >= 1." << endl;
    myexit(1);
  }
  // Set the total number of sites in the cluster
  c.nsites  = nsites * c.ncells;
  cout << " // total number of sites = " << c.nsites << endl;

  // Get the lattice constant
  inp >> c.lattice_constant;
  inp.getline(buf, sizeof(buf));
  cout << "lattice constant = " << c.lattice_constant << " bohr" << endl;

  // Get the lattice vectors
  inp >> c.lattice_vec1.x  >> c.lattice_vec1.y >> c.lattice_vec1.z;
  inp.getline(buf, sizeof(buf));
  inp >> c.lattice_vec2.x  >> c.lattice_vec2.y >> c.lattice_vec2.z;
  inp.getline(buf, sizeof(buf));
  inp >> c.lattice_vec3.x  >> c.lattice_vec3.y >> c.lattice_vec3.z;
  inp.getline(buf, sizeof(buf));
  c.lattice_vec1 = c.lattice_constant * c.lattice_vec1;
  c.lattice_vec2 = c.lattice_constant * c.lattice_vec2;
  c.lattice_vec3 = c.lattice_constant * c.lattice_vec3;
  cout << "lattice vector 1 = (" << c.lattice_vec1.x << ", " <<  c.lattice_vec1.y  << ", " <<  c.lattice_vec1.z << ") bohr" << endl;
  cout << "lattice vector 2 = (" << c.lattice_vec2.x << ", " <<  c.lattice_vec2.y  << ", " <<  c.lattice_vec2.z << ") bohr" << endl;
  cout << "lattice vector 3 = (" << c.lattice_vec3.x << ", " <<  c.lattice_vec3.y  << ", " <<  c.lattice_vec3.z << ") bohr" << endl;

  // Set the positions of all cells
  for(icell1 = 0; icell1 < c.ncells1; icell1++){
    for(icell2 = 0; icell2 < c.ncells2; icell2++){
      for(icell3 = 0; icell3 < c.ncells3; icell3++){
        // Index of this cell
        icell = cell_index(icell1, icell2, icell3, c);

        // Set the (equidistant) cell positions
        c.cells[icell].position = icell1 * c.lattice_vec1 + icell2 * c.lattice_vec2  + icell3 * c.lattice_vec3;

        cout << " cell "  << setw(3) << icell1 << setw(3) << icell2 << setw(3) << icell3 << " (" << icell <<") at (" << c.cells[icell].position.x << ", ";
        cout << c.cells[icell].position.y << ", ";
        cout << c.cells[icell].position.z << ") bohr:" << endl;

        // Initialize the sites of this cell
        c.cells[icell].nsites = nsites;
        c.cells[icell].sites  = new site[c.cells[icell].nsites];
      }
    }
  }

  // Setting up the sites
  for(isite = 0; isite < nsites; isite++){
    inp >> displacement.x >> displacement.y >> displacement.z;
    inp.getline(buf, sizeof(buf));

    // Displacement
    displacement = c.lattice_constant * displacement;
    cout << "site " << isite << ": displacement = (" << displacement.x << ", " << displacement.y << ", " << displacement.z << ") bohr :";

    check_displacement(displacement, c.lattice_vec1, c.lattice_vec2, c.lattice_vec3);

    inp >> norbitals;
    inp.getline(buf, sizeof(buf));

    // Orbitals
    orbital_types = new int[norbitals];
    for(i_orb = 0; i_orb < norbitals; i_orb++){
      inp >> orbital_types[i_orb];
    }
    inp.getline(buf, sizeof(buf));
    cout << " number of orbitals = " << norbitals << " : orbital types = ";
    for(i_orb = 0; i_orb < norbitals; i_orb++){
      switch(orbital_types[i_orb]){
        case 0:
          cout << "s ";
          break;
        case 1:
          cout << "px ";
          break;
        case 2:
          cout << "py ";
          break;
        case 3:
          cout << "pz ";
          break;
        default:
          cerr << endl << "ERROR from init_cluster(): wrong orbital type. " << orbital_types[i_orb] << endl;
          exit(1);
          break;
      }
    }
    cout << endl;

    // Set this site in all cells
    cout << "site positions:" << endl;
    for(icell1 = 0; icell1 < c.ncells1; icell1++){
      for(icell2 = 0; icell2 < c.ncells2; icell2++){
        for(icell3 = 0; icell3 < c.ncells3; icell3++){
          // Index of this cell
          icell = cell_index(icell1, icell2, icell3, c);

          c.cells[icell].sites[isite].position = c.cells[icell].position + displacement;
          cout << "  site " << setw(3) << isite << " (" << site_index(icell1, icell2, icell3, isite, c) << ") at (" << c.cells[icell].sites[isite].position.x << ", ";
          cout << c.cells[icell].sites[isite].position.y << ", ";
          cout << c.cells[icell].sites[isite].position.z << ") bohr" << endl;

          c.cells[icell].sites[isite].norbitals = norbitals;
          c.cells[icell].sites[isite].orbitals  = new orbital[norbitals];

          for(i_orb = 0; i_orb < norbitals; i_orb++){
            c.cells[icell].sites[isite].orbitals[i_orb].type = orbital_types[i_orb];
          }
        }
      }
    }
  }

  // Set the number of levels in each cell and of the entire cluster
  // -- the number of levels is identical in each cell; so pick cell "0"
  sum_level = 0;
  for(isite = 0; isite < c.cells[0].nsites; isite++){
    sum_level += c.cells[0].sites[isite].norbitals;
  }
  cout << "MESSAGE from init_cluster(): each cell contains " << sum_level << " level(s)";
  for(icell = 0; icell < c.ncells; icell++){
    c.cells[icell].nlevels = sum_level;
  }
  c.nlevels = c.ncells * sum_level;
  cout << " // the cluster contains " << c.nlevels << " level(s)." << endl;

  cout << "MESSAGE from init_cluster(): geometry read and initialised." << endl;
  inp.close();

  cout << "MESSAGE from init_cluster(): reading tight binding parameters from te.cluster.tb.in ..." << endl;
  inp.open("te.cluster.tb.in");

  inp >> c.ntb_params;
  inp.getline(buf, sizeof(buf));
  c.tb_params = new tb_param[c.ntb_params];
  for(itb_params = 0; itb_params < c.ntb_params; itb_params++){
    inp >> c.tb_params[itb_params].from_site;
    inp >> c.tb_params[itb_params].from_orbital;
    inp >> c.tb_params[itb_params].to_cell1;
    inp >> c.tb_params[itb_params].to_cell2;
    inp >> c.tb_params[itb_params].to_cell3;
    inp >> c.tb_params[itb_params].to_site;
    inp >> c.tb_params[itb_params].to_orbital;
    inp >> c.tb_params[itb_params].energy;
    inp >> c.tb_params[itb_params].type;
    inp.getline(buf, sizeof(buf));
    c.tb_params[itb_params].energy /= AU_ENERGY;
    cout << " TB parameter " << itb_params << ":";
    cout << " from site " << c.tb_params[itb_params].from_site;
    cout << "  level " << c.tb_params[itb_params].from_orbital;
    cout << " to (relative) cell " << c.tb_params[itb_params].to_cell1;
    cout << " " << c.tb_params[itb_params].to_cell2;
    cout << " " << c.tb_params[itb_params].to_cell3;
    cout << " site " << c.tb_params[itb_params].to_site;
    cout << " level " << c.tb_params[itb_params].to_orbital;
    cout << " with " << c.tb_params[itb_params].energy * AU_ENERGY << " eV with type ";
    switch(c.tb_params[itb_params].type){
      case 0:
        cout << "on-site s" << endl;
        break;
      case 1:
        cout << "on-site p" << endl;
        break;
      case 2:
        cout << "ss-sigma" << endl;
        break;
      case 3:
        cout << "sp-sigma" << endl;
        break;
      case 4:
        cout << "ps-sigma" << endl;
        break;
      case 5:
        cout << "pp-sigma" << endl;
        break;
      case 6:
        cout << "pp-pi" << endl;
        break;
      default:
        cerr << endl;
        cerr << "ERROR from init_cluster(): wrong type specified: " << c.tb_params[itb_params].type << endl;
        exit(1);
        break;
    }
    // Some error checking
    if(c.tb_params[itb_params].to_cell1 < 0){
      cerr << "ERROR from init_cluster(): to_cell1 negative: " << c.tb_params[itb_params].to_cell1 << endl;
      exit(1);
    }
    if(c.tb_params[itb_params].to_cell1 > c.ncells1){
      cerr << "ERROR from init_cluster(): to_cell1 too large: " << c.tb_params[itb_params].to_cell1 << endl;
      exit(1);
    }
    if(c.tb_params[itb_params].to_cell2 < 0){
      cerr << "ERROR from init_cluster(): to_cell2 negative: " << c.tb_params[itb_params].to_cell2 << endl;
      exit(1);
    }
    if(c.tb_params[itb_params].to_cell2 > c.ncells2){
      cerr << "ERROR from init_cluster(): to_cell2 too large: " << c.tb_params[itb_params].to_cell2 << endl;
      exit(1);
    }
    if(c.tb_params[itb_params].to_cell3 < 0){
      cerr << "ERROR from init_cluster(): to_cell3 negative: " << c.tb_params[itb_params].to_cell3 << endl;
      exit(1);
    }
    if(c.tb_params[itb_params].to_cell3 > c.ncells3){
      cerr << "ERROR from init_cluster(): to_cell3 too large: " << c.tb_params[itb_params].to_cell3 << endl;
      exit(1);
    }
  }

  cout << "MESSAGE from init_cluster(): tight binding parameters read." << endl;

  inp.close();

  cout << "MESSAGE from init_cluster(): cluster initialised." << endl;
}


// Initialize the population matrix
// -- The occupation is set according to the Fermi-Dirac distrubution
void init_population(job &jp, cluster &c, complexmatrix &P, complexmatrix &H){

  int i, icell, icell1, icell2, icell3, isite, iorbital;

  complex e;

  // Initialise the population matrix
  P = 0.0;

  // For all cells
  for(icell1 = 0; icell1 < c.ncells1; icell1++){
    for(icell2 = 0; icell2 < c.ncells2; icell2++){
      for(icell3 = 0; icell3 < c.ncells3; icell3++){
        // Index of this cell
        icell = cell_index(icell1, icell2, icell3, c);

        // For all sites of this cell
        for(isite = 0; isite < c.cells[icell].nsites; isite++){
          // For all orbitals of this site
          for(iorbital = 0; iorbital < c.cells[icell].sites[isite].norbitals; iorbital++){
            i = orbital_index(icell1, icell2, icell3, isite, iorbital, c);
            // Get the energy of this level
            e = real(H(i, i));

            // Occupation according to the fermionic distribution
            P(i,i) = FermiDirac(e, jp.mue_system, jp.temperature);
          }
        }
      }
    }
  }

  // Print the initialised values
  cout << "MESSAGE from init_population(): initial occupations:" << endl;
  for(icell1 = 0; icell1 < c.ncells1; icell1++){
    for(icell2 = 0; icell2 < c.ncells2; icell2++){
      for(icell3 = 0; icell3 < c.ncells3; icell3++){
        icell = cell_index(icell1, icell2, icell3, c);

        for(isite = 0; isite < c.cells[icell].nsites; isite++){
          for(iorbital = 0; iorbital < c.cells[icell].sites[isite].norbitals; iorbital++){
            i = orbital_index(icell1, icell2, icell3, isite, iorbital, c);
            cout << "Cell " << setw(3) << icell;
            cout << " site " << setw(3) << isite;
            cout << " level " << setw(3) << iorbital << " (" << i << "): ";
            cout << " energy = " << setw(3) << real(H(i,i)) * AU_ENERGY << " eV";
            cout << " // occupation = " << setprecision(6)  << real(P(i,i));
            cout << endl;
          }
        }
      }
    }
  }
  cout << "MESSAGE from init_population(): population initialised." << endl;
}


// Main program
int main(int argc, char **argv){

  job jp;

  cluster c;

  int icell, icell1, icell2, icell3, isite, iorbital, i, j;
  int out_count;

  double sumP, sumPH, t, totalE;

  complexmatrix H0, Ht, Vt, P, Ptemp, PH, L;
  complexmatrix Ft, Ftplus;

  complexdiagmatrix Pdummy;

  ofstream *out_energy, *out_occupation, out_pulse, out_totals;

  std::string s;

  // Read job parameters and initialize the cluster
  read_parameters(jp);
  init_cluster(c, jp);
  write_cluster(c);

  cout << endl;
  cout << "MESSAGE from main(): evolution of a system with " << c.nlevels << " orbitals of " << c.nsites << " sites in " << c.ncells << " cells" << endl;
  cout << endl;

  // Initialize the Hamiltonian
  // -- H0 ist the time-independent part, V is the time-dependent perturbation, Ht = H0 + Vt
  H0.setsize(c.nlevels, c.nlevels);
  init_Hamiltonian(jp, c, H0);
#ifdef EIGEN
  compute_eval(c, H0);
#endif
  // check_mue_bath(jp, c, H0);

  Vt.setsize(c.nlevels, c.nlevels);
  Vt = 0.0;
  Ht.setsize(c.nlevels, c.nlevels);
  Ht = 0.0;

  // Initialize the Lindbladian
  L.setsize(c.nlevels, c.nlevels);
  L = 0.0;

  // Initialize the population matrices
  // -- only the lowest level is occupied
  P.setsize(c.nlevels, c.nlevels);
  switch (jp.init_flag){
    case 0:
      cout << "MESSAGE from main(): initialising occupation from distribution function." << endl;
      init_population(jp, c, P, H0);
      break;
    case 1:
      cout << "MESSAGE from main(): initialising occupation from file te.occupation.matrix.in" << endl;
      read_occupation(c, P);
      break;
    default:
      cerr << "ERROR from main(): wrong initialization flag: " << jp.init_flag << endl;
      myexit(1);
  }

  Pdummy.setsize(c.nlevels);
  Pdummy = 0.0;
  PH.setsize(c.nlevels, c.nlevels);
  PH = 0.0;
  Ptemp.setsize(c.nlevels, c.nlevels) = 0;
  Ft.setsize(c.nlevels, c.nlevels);
  Ftplus.setsize(c.nlevels, c.nlevels);

  // Initialize the output streams
  // -- One file per site
  out_energy     = new ofstream[c.nsites];
  out_occupation = new ofstream[c.nsites];
  for(isite = 0; isite < c.nsites; isite++){
    set_filename_energy(c, s, isite);
    out_energy[isite].open(s);

    set_filename_occupation(c, s, isite);
    out_occupation[isite].open(s);
  }
  out_pulse.open("te.pulse.out");
  out_totals.open("te.totals.out");

  // Time evolution
  // -- For all times
  cout << endl;
  cout << "MESSAGE from main(): time evolution ... " << endl;
  // -- Initialize the loop counter
  out_count = 0;
  for(t = jp.tmin; t <= jp.tmax; t+= jp.tstep){
    // Solve the ODE (time evolution); result of this block is Ptemp;
    switch(jp.tintegrator){
      case 0:
        // Euler method
        // -- Set the Hamiltonian for time t
        update_Hamiltonian(jp, c, H0, Vt, t);
        Ht = H0 + Vt;
        update_Lindbladian(jp, c, L, P, Ht);
        // -- Compute the kernel for time t
        Ft = cmplx(0.0, 1.0) * (P * Ht - Ht * P) + L;
        // -- Euler step
        Ptemp = P + Ft * jp.tstep;
        break;
      case 1:
        // Heun method, see https://en.wikipedia.org/wiki/Heun%27s_method
        // -- Set the Hamiltonian for time t
        update_Hamiltonian(jp, c, H0, Vt, t);
        Ht = H0 + Vt;
        update_Lindbladian(jp, c, L, P, Ht);
        // -- Compute the kernel for time t
        Ft = cmplx(0.0, 1.0) * (P * Ht - Ht * P) + L;
        // -- Euler step (predictor step)
        Ptemp = P + Ft * jp.tstep;
        // -- Set the Hamiltonian for time t + tstep
        update_Hamiltonian(jp, c, H0, Vt, t + jp.tstep);
        Ht = H0 + Vt;
        update_Lindbladian(jp, c, L, Ptemp, Ht);
        // -- Compute the kernel for time t + tstep
        Ftplus = cmplx(0.0, 1.0) * (Ptemp * Ht - Ht * Ptemp) + L;
        // -- Heun step
        Ptemp = P + 0.5 * (Ft + Ftplus) * jp.tstep;
        break;
      case 2:
        // Explicit midpoint method, see https://en.wikipedia.org/wiki/Midpoint_method
        // -- Set the Hamiltonian for time t
        update_Hamiltonian(jp, c, H0, Vt, t);
        Ht = H0 + Vt;
        update_Lindbladian(jp, c, L, P, Ht);
        // -- Compute the kernel for time t
        Ft = cmplx(0.0, 1.0) * (P * Ht - Ht * P) + L;
        // -- Euler step (predictor step) with 0.5 tstep
        Ptemp = P + 0.5 * Ft * jp.tstep;
        // -- Set the Hamiltonian for time t + 0.5 tstep
        update_Hamiltonian(jp, c, H0, Vt, t + 0.5 * jp.tstep);
        Ht = H0 + Vt;
        update_Lindbladian(jp, c, L, Ptemp, Ht);
        // -- Compute the kernel for time t + tstep
        Ftplus = cmplx(0.0, 1.0) * (Ptemp * Ht - Ht * Ptemp) + L;
        // -- Euler step
        Ptemp = P + Ftplus * jp.tstep;
        break;
      case 3:
        // Implicit midpoint method, see https://en.wikipedia.org/wiki/Midpoint_method
        // -- Set the Hamiltonian for time t + 0.5 tstep
        update_Hamiltonian(jp, c, H0, Vt, t + 0.5 * jp.tstep);
        Ht = H0 + Vt;
        update_Lindbladian(jp, c, L, P, Ht);
        // -- Compute the kernel for time t + 0.5 tstep
        Ft = cmplx(0.0, 1.0) * (P * Ht - Ht * P) + L;
        // -- Implicit Euler step with 0.5 tstep
        Ptemp = P + 0.5 * Ft * jp.tstep;
        update_Lindbladian(jp, c, L, Ptemp,Ht);
        // -- Compute the kernel for the next step
        Ftplus = cmplx(0.0, 1.0) * (Ptemp * Ht - Ht * Ptemp) + L;
        // -- Euler step with 0.5 tstep
        Ptemp = Ptemp + 0.5 * Ftplus * jp.tstep;
        break;
      default:
        cerr << "ERROR: wrong integrator: " << jp.tintegrator << endl;
        myexit(1);
        break;
    }

    // Copy the new population matrix to the old one
    P = Ptemp;

    // Output to fstream
    if(out_count == jp.out_control || out_count == 0){
      // -- energy data
      PH = P * H0;

      // -- total energy
      totalE = 0;

      for(icell1 = 0; icell1 < c.ncells1; icell1++){
        for(icell2 = 0; icell2 < c.ncells2; icell2++){
          for(icell3 = 0; icell3 < c.ncells3; icell3++){
            icell = cell_index(icell1, icell2, icell3, c);
            for(isite = 0; isite < c.cells[icell].nsites; isite++){
              // Write time (in fs)
              i = site_index(icell1, icell2, icell3, isite, c);
              out_energy[i] << t * AU_TIME;
              // Partial trace (over levels of site isite)
              sumP  = 0.0;
              sumPH = 0.0;
              for(iorbital = 0; iorbital < c.cells[icell].sites[isite].norbitals; iorbital++){
                j = orbital_index(icell1, icell2, icell3, isite, iorbital, c);
                sumP  += real(P(j, j));
                sumPH += real(P(j, j)) * real(H0(j, j));
                // Add these data to the totals
                totalE += sumPH;
              }
              // Write energies (in eV)
              out_energy[i] << " " << sumP << " " << sumPH * AU_ENERGY << endl;
            }
            // -- occupation data
            for(isite = 0; isite < c.cells[icell].nsites; isite++){
              // Write time (in fs)
              i = site_index(icell1, icell2, icell3, isite, c);
              out_occupation[i] << t * AU_TIME;
              for(iorbital = 0; iorbital < c.cells[icell].sites[isite].norbitals; iorbital++){
                j = orbital_index(icell1, icell2, icell3, isite, iorbital, c);
  	             out_occupation[i] << " " << real(P(j, j));
                 if(real(P(j, j)) < 0.0){
                   cout << "WARNING from main(): negative occupation: " << real(P(j, j)) << endl;
                 }
               }
               out_occupation[i] << endl;
             }
           }
         }
       }
      // Write perturbation (in fs and eV) and totals
      out_pulse  << t * AU_TIME << " " << perturbation(t, jp.center, jp.width, jp.omega, jp.amplitude) * AU_ENERGY << endl;
      out_totals << t * AU_TIME << " " << totalE * AU_ENERGY << endl;

      // Reset the loop counter
      out_count = 0;
    }
    //Increase the loop counter
    out_count++;
  }

  // Write the complete occupation matrix
  write_occupation(c, P);

  for(isite = 0; isite < c.nsites; isite++){
    out_energy[isite].close();
    out_occupation[isite].close();
  }
  out_pulse.close();
  out_totals.close();

  cout << "MESSAGE from main(): done. " << endl;

}

#undef EIGEN
