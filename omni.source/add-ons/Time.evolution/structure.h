//
// Structure definitions
//

#include "complex.h"
#include "vector2.h"
#include "vector3.h"

// Spin quantum numbers
// Spin quantization axis is z
#define SPINUP 0
#define SPINDN 1

// Pulse shape
struct pulse_shape{
  double center;
  double width;
  double omega;
  double amplitude;
};

// Perturbation
struct perturbation{
  int     cell1;
  int     cell2;
  int     cell3;
  int     site;
  int     from_orbital;
  int     to_orbital;
  complex strength;
};

// Job parameters
struct job{
  // Pulse shape
  pulse_shape  ps;

  // Perturbations
  int           nperturbed;
  perturbation *perturbed;

  // Bath stuff
  double temperature;
  double mue_system;
  double mue_bath;
  double gamma;
  int    init_flag;

  // External magnetic field
  vector3 bmag;

  // Time stuff
  double tmin;
  double tmax;
  double tstep;
  int    tintegrator;

  // Output control
  int    out_control;
};

// Types of tight binding parameters
// 0 = on-site s
// 1 = on-site p
// 2 = ss_sigma
// 3 = sp_sigma
// 4 = ps_sigma
// 5 = pp_sigma
// 6 = pp_pi

// Tight binding read_parameters
struct tb_param{
  int     type;

  int     from_site;
  int     from_orbital;

  int     to_cell1;
  int     to_cell2;
  int     to_cell3;
  int     to_site;
  int     to_orbital;

  complex energy_up_up;
  complex energy_up_dn;
  complex energy_dn_up;
  complex energy_dn_dn;

  double  lambda;
};

// Types of orbitals
// type = 0 => s
// type = 1 => px
// type = 2 => py
// type = 3 => pz

// Orbitals
struct orbital{
  int type;
};

// Sites
struct site{
  vector3  position;

  int      norbitals;
  orbital *orbitals;
};

// Cells
struct cell{
  vector3 position;

  int     nsites;
  site   *sites;

  int     nlevels; // total number of levels in the cell
};

// Cluster
struct cluster{
  int       ncells1;
  int       ncells2;
  int       ncells3;
  int       ncells;  // total number of cells in the cluster
  int       nsites;  // total number of sites in the cluster
  int       nlevels; // total number of levels in the cluster (w/o spin)
  int       boundary1;
  int       boundary2;
  int       boundary3;

  double    lattice_constant;
  vector3   lattice_vec1;
  vector3   lattice_vec2;
  vector3   lattice_vec3;

  cell     *cells;

  int       ntb_params;
  tb_param *tb_params;
};
