//
// Function for the hamiltonian
//
#include "cmatrix.h"

// Initialize the Hamiltonian; only time-independent elements
void init_Hamiltonian(job &jp, cluster &c, complexmatrix &H);

// Update the Hamiltonian at time t
void update_Hamiltonian(job &jp, cluster &c, complexmatrix &H, complexmatrix &Vt, double t);

// Compute energy eigenvalues
void compute_eval(cluster &c, complexmatrix &H);
