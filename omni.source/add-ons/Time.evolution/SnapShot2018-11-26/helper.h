//
// Helper functions
//

#include <iostream>
using namespace std;

#include <fstream>
#include <iomanip>
#include <math.h>
#include <string>

#include "vector2.h"


// Check the displacement of a site
void check_displacement(vector2 &d, vector2 &a1, vector2 &a2);

// Set fstream name for energy data
void set_filename_energy(job &jp, std::string &s, int isite);

// Set fstream name for occupation data
void set_filename_occupation(job &jp, std::string &s, int isite);

// Compute temperature and chemical potential of a Fermi-Dirac distribution for a given 2-level occupation
// e1, e2 - energies of the 2-level system
// p1, p2 - corresponding level occupations, in [0, 1]
void compute_t_mue(double e1, double p1, double e2, double p2);

// Check the chemical potential of the mue_bath
void check_mue_bath(job &jp, cluster &c, complexmatrix &H0);

// Write cluster to ofstream
void write_cluster(cluster &c);
