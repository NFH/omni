//
// Index stuff
//

// Get the index of a cell
int cell_index(int cell1, int cell2, cluster &c);

// Get the index of a site
int site_index(int cell1, int cell2, int site, cluster &c);

// Get the index of a level
int level_index(int cell1, int cell2, int site, int level, cluster &c);
