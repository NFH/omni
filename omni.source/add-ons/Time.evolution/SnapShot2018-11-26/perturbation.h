//
// perturbation.h
//
// 2018-09-28 JH
//

// Perturbation - puls shaped
double perturbation(double x, double center, double width, double omega, double a);
