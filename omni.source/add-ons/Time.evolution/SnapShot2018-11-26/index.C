//
// Index stuff
//

#include "structure.h"

// Get the index of a cell
int cell_index(int cell1, int cell2, cluster &c){

  return cell1 * c.ncells2 + cell2;

}

// Get the index of a site
int site_index(int cell1, int cell2, int site, cluster &c){

  int icell, nsites;

  icell  = cell_index(cell1, cell2, c);
  nsites = c.cells[icell].nsites;

  return icell * nsites + site;

}

// Get the index of a level
int level_index(int cell1, int cell2, int site, int level, cluster &c){

  int icell, isite, nlevels;

  icell   = cell_index(cell1, cell2, c);
  isite   = site_index(cell1, cell2, site, c);
  nlevels = c.cells[icell].sites[site].nlevels;

  return isite * nlevels + level;

}
