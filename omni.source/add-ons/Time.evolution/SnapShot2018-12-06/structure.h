//
// Structure definitions
//

#include "complex.h"
#include "vector2.h"
#include "vector3.h"

// Job parameters
struct job{
  int    nlevels;
  int    ncells1;
  int    ncells2;
  int    ncells3;
  int    boundary1;
  int    boundary2;
  int    boundary3;

  int    nsites;

  double center;
  double width;
  double omega;
  double amplitude;
  int    npertubed;
  int   *perturbed_cell1;
  int   *perturbed_cell2;
  int   *perturbed_cell3;
  int   *perturbed_site;

  double temperature;
  double mue_system;
  double mue_bath;
  double gamma;
  int    init_flag;

  double tmin;
  double tmax;
  double tstep;
  int    tintegrator;

  int    out_control;
};

// Tight binding read_parameters
struct tb_param{
  int     from_site;
  int     from_level;

  int     to_cell1;
  int     to_cell2;
  int     to_cell3;
  int     to_site;
  int     to_level;

  complex energy;
};

// Sites
struct site{
  vector3  position;

  int      nlevels;
};

// Cells
struct cell{
  vector3 position;

  int     nsites;
  site   *sites;
};

// Cluster
struct cluster{
  int       ncells1;
  int       ncells2;
  int       ncells3;
  int       nsites;
  int       nlevels;

  double    lattice_constant;
  vector3   lattice_vec1;
  vector3   lattice_vec2;
  vector3   lattice_vec3;
  vector3  *displacements;

  cell     *cells;

  int       ntb_params;
  tb_param *tb_params;
};
