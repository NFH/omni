//
// Functions for the Lindbladian
//

#include "complex.h"
#include "cmatrix.h"
#include "distribution.h"

#include "structure.h"
#include "index.h"

// Update the Lindblad jump operators L
// -- OBS: Only for two-level systems
void update_Lindbladian(job &jp, cluster &c, complexmatrix &L, complexmatrix &P, complexmatrix &H){

  int i, icell, icell1, icell2, isite, j;

  complex e, gamma;

  complexmatrix L1, L2;

  // Initialize the Lindbladians
  L1.setsize(jp.nsites * jp.nlevels, jp.nsites * jp.nlevels);
  L2.setsize(jp.nsites * jp.nlevels, jp.nsites * jp.nlevels);

  // De-excitation
  L1 = 0.0;
  for(icell1 = 0; icell1 < c.ncells1; icell1++){
    for(icell2 = 0; icell2 < c.ncells2; icell2++){
      icell = cell_index(icell1, icell2, c);

      for(isite = 0; isite < c.cells[icell].nsites; isite++){
        i = level_index(icell1, icell2, isite, 0, c);

        // Energy of (the lower) level 0
        e = H(i,i);
        // Coupling strength * occupation of the bath at that energy
        gamma = jp.gamma * BoseEinstein(e, jp.mue_bath, jp.temperature);

        L1(i, i) = L1(i, i) + gamma * P(i+1, i+1);

        for(j = 0; j <  jp.nsites * jp.nlevels; j++){
          L1(i+1, j)   = L1(i+1, j)   - 0.5 * gamma * P(i+1, j);
          L1(j,   i+1) = L1(j,   i+1) - 0.5 * gamma * P(j,   i+1);
        }
      }
    }
  }

  // Excitation
  L2 = 0.0;
  for(icell1 = 0; icell1 < c.ncells1; icell1++){
    for(icell2 = 0; icell2 < c.ncells2; icell2++){
      icell = cell_index(icell1, icell2, c);

      for(isite = 0; isite < c.cells[icell].nsites; isite++){
        i = level_index(icell1, icell2, isite, 0, c);

        // Energy of (the higher) level 1
        e = H(i+1, i+1);
        // Coupling strength * occupation of the bath at that energy
        gamma = jp.gamma * BoseEinstein(e, jp.mue_bath, jp.temperature);

        L2(i+1, i+1) = L2(i+1, i+1) + gamma * P(i, i);

        for(j = 0; j < jp.nsites * jp.nlevels; j++){
          L2(i, j) = L2(i, j) - 0.5 * gamma * P(i, j);
          L2(j, i) = L2(j, i) - 0.5 * gamma * P(j, i);
        }
      }
    }
  }
  // Multiply with the de-excitation rate
  // L2 = jp.gamma2 * L2;

  // Sum up
  L = L1 + L2;
}
