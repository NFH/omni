//
// Functions for the Lindbladian
//

#include "complex.h"
#include "cmatrix.h"

#include "structure.h"
#include "index.h"

// Update the Lindblad jump operators L
// -- OBS: Only for two-level systems
void update_Lindbladian(job &jp, chain &c, complexmatrix &L, complexmatrix &P, complexmatrix &H){

  int i, icell, isite, j;

  complexmatrix L1, L2;

  // Initialize the Lindbladians
  L1.setsize(jp.nsite * jp.nlevel, jp.nsite * jp.nlevel);
  L2.setsize(jp.nsite * jp.nlevel, jp.nsite * jp.nlevel);

  // De-excitation
  L1 = 0.0;
  for(icell = 0; icell < c.ncell; icell++){
    for(isite = 0; isite < c.cells[icell].nsite; isite++){
      i = level_index(icell, isite, 0, c);

      L1(i, i) = L1(i, i) + P(i+1, i+1);

      for(j = 0; j <  jp.nsite * jp.nlevel; j++){
        L1(i+1, j)   = L1(i+1, j)   - 0.5 * P(i+1, j);
        L1(j,   i+1) = L1(j,   i+1) - 0.5 * P(j,   i+1);
      }
    }
  }
  // Multiply with the de-excitation rate
  L1 = jp.gamma1 * L1;

  // Excitation
  L2 = 0.0;
  for(icell = 0; icell < c.ncell; icell++){
    for(isite = 0; isite < c.cells[icell].nsite; isite++){
      i = level_index(icell, isite, 0, c);
      L2(i+1, i+1) = L2(i+1, i+1) + P(i, i);

      for(j = 0; j < jp.nsite * jp.nlevel; j++){
        L2(i, j) = L2(i, j) - 0.5 * P(i, j);
        L2(j, i) = L2(j, i) - 0.5 * P(j, i);
      }
    }
  }
  // Multiply with the de-excitation rate
  L2 = jp.gamma2 * L2;

  // Sum up
  L = L1 + L2;
}
