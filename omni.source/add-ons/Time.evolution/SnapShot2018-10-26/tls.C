//
// Two level system with dissipation
//
// 2018-09-28 JH
//

#include <iostream>
using namespace std;

#include <fstream>
#include <iomanip>
#include <math.h>
#include <string>

#include "platform.h"

#include "cmatrix.h"
#include "constants.h"
#include "complex.h"

#include "perturbation.h"

// Number of levels, always = 2
#define NLEVEL 2

// Define the parameters of the perturbation pulse
#define CENTER 5
#define WIDTH  1.0
#define OMEGA  1.0

// Dissipation rates
#define GAMMA1 0.01
#define GAMMA2 0.000

// Define the time interval, for evolution
#define TMIN   0.0
#define TMAX  100.0
#define TSTEP  0.001
// Define the ODE integrator
// 0 = Euler
// 1 = Heun
// 2 = explicit midpoint
// 3 = implicit midpoint
#define INTEGRATOR 3


// Initialize the Hamiltonian; only time-independent elements
void init_Hamiltonian(complexmatrix &H){

  int ilevel;

  H = 0;

  // -- Diagonal elements; constant throughout the calculation
  for(ilevel = 0; ilevel < NLEVEL; ilevel++){
     H(ilevel, ilevel) = cmplx(1.0, 0.0) * ilevel;
  }
}

// Update the Hamiltonian at time t
void update_Hamiltonian(complexmatrix &H, double t){

  int ilevel;

  for(ilevel = 0; ilevel < NLEVEL - 1; ilevel++){
    H(ilevel,    ilevel + 1)  = cmplx(0.0,  1.0) * perturbation(t, CENTER, WIDTH, OMEGA);
    H(ilevel + 1, ilevel)     = cmplx(0.0, -1.0) * perturbation(t, CENTER, WIDTH, OMEGA);
  }
}

// Update the Lindblad jump operators L
void update_Lindbladian(complexmatrix &L, complexmatrix &P){

  L(0, 0) =        GAMMA1 * P(1, 1) -       GAMMA2 * P(0,0);
  L(0, 1) = -0.5 * GAMMA1 * P(0, 1) - 0.5 * GAMMA2 * P(0, 1);
  L(1, 0) = -0.5 * GAMMA1 * P(1, 0) - 0.5 * GAMMA2 * P(1, 0);
  L(1, 1) =       -GAMMA1 * P(1, 1) +       GAMMA2 * P(0,0);

}


// Initialize the population matrix
void init_population(complexmatrix &P){

  P       = 0.0;
  P(0, 0) = 1.0;

}


// Main program
int main(int argc, char **argv){

  int ilevel;

  double t;

  complexmatrix H, P, Ptemp, L;
  complexmatrix Ft, Ftplus;

  complexdiagmatrix Pdummy;

  ofstream out_occupation, out_perturbation;

  std::string s;

  cout << endl;
  cout << "Two-level system with dissipation " << endl;
  cout << endl;
  // Check number of levels
  if(NLEVEL != 2){
    cerr << "NLEVEL should be >= 2." << endl;
    myexit(1);
  }

  // Initialize the Hamiltonian
  H.setsize(NLEVEL, NLEVEL);
  init_Hamiltonian(H);

  L.setsize(NLEVEL, NLEVEL);

  // Initialize the population matrices
  // -- only the lowest level is occupied
  P.setsize(NLEVEL, NLEVEL);
  init_population(P);
  Pdummy.setsize(NLEVEL);

  // Print the initialized values
  cout << "Energy levels and initial occupations:" << endl;
  for(ilevel = 0; ilevel < NLEVEL; ilevel++){
     cout << " Level " << setw(3) << ilevel;
     cout << " : "   << setprecision(6) << real(H(ilevel, ilevel));
     cout << " w/ " << setprecision(6)  << real(P(ilevel, ilevel));
     cout << endl;
   }
  cout << endl;

  Ptemp.setsize(NLEVEL, NLEVEL) = 0;
  Ft.setsize(NLEVEL, NLEVEL);
  Ftplus.setsize(NLEVEL, NLEVEL);

  // Initialize the output streams
  out_occupation.open("tls.occupation.out");
  out_perturbation.open("tls.perturbation.out");

  // Time evolution
  // -- Print the important parameters
  cout << "Perturbation:";
  cout <<" center @ " << CENTER;
  cout <<" width = " << WIDTH;
  cout <<" omega = " << OMEGA << endl;
  cout << "Dissipation: rate 1 = " << GAMMA1 << " rate 2 = " << GAMMA2 << endl;

  // -- For all times
  cout << endl;
  cout << "Time evolution from " << TMIN << " to " << TMAX << " with step width " << TSTEP << " and integrator ";
  switch(INTEGRATOR){
    case 0:
      cout << "Euler (0)";
      break;
    case 1:
      cout << "Heun (1)";
      break;
    case 2:
      cout << "Explicit midpoint (2)";
      break;
    case 3:
      cout << "Implicit midpoint (3)";
      break;
    default:
      cerr << "WRONG!";
      myexit(1);
      break;
  }
  cout << " ... ";

  for(t = TMIN; t <= TMAX; t+= TSTEP){
    // Solve the ODE (time evolution); result of this block is Ptemp;
    switch(INTEGRATOR){
      case 0:
        // Euler method
        // -- Set the Hamiltonian for time t
        update_Hamiltonian(H, t);
        update_Lindbladian(L, P);
        // -- Compute the kernel for time t
        Ft = cmplx(0.0, 1.0) * (P * H - H * P) + L;
        // -- Euler step
        Ptemp = P + Ft * TSTEP;
        break;
      case 1:
        // Heun method, see https://en.wikipedia.org/wiki/Heun%27s_method
        // -- Set the Hamiltonian for time t
        update_Hamiltonian(H, t);
        update_Lindbladian(L, P);
        // -- Compute the kernel for time t
        Ft = cmplx(0.0, 1.0) * (P * H - H * P) + L;
        // -- Euler step (predictor step)
        Ptemp = P + Ft * TSTEP;
        // -- Set the Hamiltonian for time t + tstep
        update_Hamiltonian(H, t + TSTEP);
        update_Lindbladian(L, Ptemp);
        // -- Compute the kernel for time t + tstep
        Ftplus = cmplx(0.0, 1.0) * (Ptemp * H - H * Ptemp) + L;
        // -- Heun step
        Ptemp = P + 0.5 * (Ft + Ftplus) * TSTEP;
        break;
      case 2:
        // Explicit midpoint method, see https://en.wikipedia.org/wiki/Midpoint_method
        // -- Set the Hamiltonian for time t
        update_Hamiltonian(H, t);
        update_Lindbladian(L, P);
        // -- Compute the kernel for time t
        Ft = cmplx(0.0, 1.0) * (P * H - H * P) + L;
        // -- Euler step (predictor step) with 0.5 tstep
        Ptemp = P + 0.5 * Ft * TSTEP;
        // -- Set the Hamiltonian for time t + 0.5 tstep
        update_Hamiltonian(H, t + 0.5 * TSTEP);
        update_Lindbladian(L, Ptemp);
        // -- Compute the kernel for time t + tstep
        Ftplus = cmplx(0.0, 1.0) * (Ptemp * H - H * Ptemp) + L;
        // -- Euler step
        Ptemp = P + Ftplus * TSTEP;
        break;
      case 3:
        // Implicit midpoint method, see https://en.wikipedia.org/wiki/Midpoint_method
        // -- Set the Hamiltonian for time t + 0.5 tstep
        update_Hamiltonian(H, t + 0.5 * TSTEP);
        update_Lindbladian(L, P);
        // -- Compute the kernel for time t + 0.5 tstep
        Ft = cmplx(0.0, 1.0) * (P * H - H * P) + L;
        // -- Implicit Euler step with 0.5 tstep
        Ptemp = P + 0.5 * Ft * TSTEP;
        // -- Compute the kernel for the next step
        update_Lindbladian(L, Ptemp);
        Ftplus = cmplx(0.0, 1.0) * (Ptemp * H - H * Ptemp) + L;
        // -- Euler step with 0.5 tstep
        Ptemp = Ptemp + 0.5 * Ftplus * TSTEP;
        break;
      default:
        cerr << "Wrong integrator: " << INTEGRATOR << endl;
        myexit(1);
        break;
    }

    // Copy the new population matrix to the old one
    P = Ptemp;

    // Output to fstream
    out_occupation << t;
    for(ilevel = 0; ilevel < NLEVEL; ilevel++){
      out_occupation << " " << real(P(ilevel, ilevel));
      }
    out_occupation << endl;

    out_perturbation << t << " " << perturbation(t, CENTER, WIDTH, OMEGA) << endl;
  }
  cout << " done." << endl << endl;

  out_occupation.close();
  out_perturbation.close();

  cout << "Done." << endl;

}
