//
// Index stuff
//

// Get the index of a level
int level_index(int cell, int site, int level, chain &c);

// Get the index of a site (for ofstreams)
int level_site(int cell, int site, chain &c);
