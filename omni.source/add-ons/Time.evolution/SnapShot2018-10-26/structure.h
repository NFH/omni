//
// Structure definitions
//

#include "complex.h"

// Job parameters
struct job{
  int nlevel;
  int ncell;
  int boundary;

  int nsite;

  double center;
  double width;
  double omega;
  double amplitude;
  int    npertubed;
  int   *perturbed_cell;
  int   *perturbed_site;

  double temperature;
  double mue;

  double gamma1;
  double gamma2;

  double tmin;
  double tmax;
  double tstep;
  int    tintegrator;

  int out_control;
};

// Sites
struct site{
  double position;

  int      nlevel;
  complex *energies;
  complex *hoppings;
};

// Cells
struct cell{
  double position;

  int   nsite;
  site *sites;
};

// Chain
struct chain{
  int ncell;
  int nlevel;

  int nsite;

  double lattice_constant;

  cell *cells;
};
