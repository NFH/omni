//
// Time evolution - Three-level system
//
// 2018-08-30 JH
//

#include <iostream>
using namespace std;

#include <fstream>
#include <iomanip>
#include <math.h>

#include "platform.h"

#include "cmatrix.h"
#include "constants.h"
#include "complex.h"

#define CENTER1 2.5
#define WIDTH1   0.5
#define OMEGA1  1.0

#define CENTER2 7.5
#define WIDTH2   0.5
#define OMEGA2  1.0

#define TMIN   0.0
#define TMAX  10.0
#define TSTEP  0.0001
  
// Perturbation - puls shaped
double lorentzian(double x, double mu, double gamma){
  
  double f, gamma2;
  
  gamma2 = 0.5 * gamma;
  
  f =  gamma2 / (pow(x - mu, 2) + pow(gamma2, 2)) / M_PI;
  
  return f;
}

double pulse(double x, double center, double width, double omega){
  
  double f;
  
  f = lorentzian(x, center, width) * cos(omega * (x - center));
  
  return f;
}

double perturbation(double x){
  
  double f;
  
  f = pulse(x, CENTER1, WIDTH1,  OMEGA1) + pulse(x, CENTER2, WIDTH2,  OMEGA2) ;
  
  return f;
}

// Main program
int main(int argc, char **argv){

  double t, tmin, tmax, tstep;
  double omega;
  
  complexmatrix     H, P, Ptemp;
  
  complexmatrix     Pevec;
  complexdiagmatrix Peval;
  
  ofstream out;
  
  // Initialize the Hamiltonian
  H.setsize(3,3);
  H = 0.0;
  H(0,0) = cmplx(0.0, 0.0);
  H(1,1) = cmplx(1.0, 0.0);
  H(2,2) = cmplx(2.0, 0.0);
  cout << H << endl;

  // Initialize the population matrices
  // -- only the lowest level is occupied
  P.setsize(3,3);
  P = 0.0;
  P(0,0) = 1.0;
  cout << P << endl;
  
  Ptemp.setsize(2, 2) = 0;
  //  Pevec.setsize(2, 2) = 0;
  //  Peval.setsize(2)    = 0;
  
  
  // Initialize the output stream
  out.open("te3.out");
 
  // Time evolution
  // -- For all times
  for(t = TMIN; t <= TMAX; t+= TSTEP){
    // Set the Hamiltonian for this time
    H(0,1) = cmplx(0.0,  1.0) * perturbation(t);
    H(1,0) = cmplx(0.0, -1.0) * perturbation(t);
    H(1,2) = cmplx(0.0,  1.0) * perturbation(t);
    H(2,1) = cmplx(0.0, -1.0) * perturbation(t);
    
    // Solve the ODE (time evolution)
    Ptemp = P + cmplx(0.0, 1.0) * (P * H - H * P) * TSTEP;
    
    // Eigenvalues and -vectors of the population matrix
    // eigen(Ptemp, Peval, Pevec);
    
    out << t << " " << abs(P(0,0)) << " " << abs(P(0,1)) << " " << " " << abs(P(0,2)) << " " << abs(P(1,1)) << " " << abs(P(1,2)) << " " << abs(P(2,2))  << " " << perturbation(t) << endl;
    
    // Copy the new population matrix to the old one
    P = Ptemp;
  }

  out.close();

}


