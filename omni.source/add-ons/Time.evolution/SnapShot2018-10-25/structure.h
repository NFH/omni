//
// Structure definitions
//

#include "complex.h"

// Job parameters
struct job{
  int nlevel;
  int nsite;
  int boundary;

  double center;
  double width;
  double omega;
  double amplitude;
  int   *perturbed;

  double temperature;
  double mue;

  double gamma1;
  double gamma2;

  double tmin;
  double tmax;
  double tstep;
  int    tintegrator;

  int out_control;
};

// Sites
struct site{
  double position;

  int nlevel;

  complex *energies;
  complex *hoppings;
};

// Chain
struct chain{
  int nsite;
  int nlevel;

  double lattice_constant;

  site *sites;
};
