//
// Function for the hamiltonian
//
#include "cmatrix.h"

// Initialize the Hamiltonian; only time-independent elements
void init_Hamiltonian(job &jp, chain &c, complexmatrix &H);

// Update the Hamiltonian at time t
void update_Hamiltonian(job &jp, complexmatrix &Vt, double t);
