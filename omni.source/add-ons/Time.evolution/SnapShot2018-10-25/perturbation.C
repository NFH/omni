//
// perturbation.C
//
// Defines the pulse shape (perturbation)
//
// 2018-10-18 JH
//

#include <math.h>

// Lorentzian distribution, with normalized area
double lorentzian(double x, double center, double gamma){

  double f, gamma2;

  gamma2 = 0.5 * gamma;

  f =  gamma2 / (pow(x - center, 2) + pow(gamma2, 2)) / M_PI;

  return f;
}

// Lorentzian distribution, with normalized maximum
double lorentzian2(double x, double center, double gamma){

  double f, gamma2;

  gamma2 = 0.5 * gamma;

  f = pow(gamma2, 2) / (pow(x - center, 2) + pow(gamma2, 2));

  return f;
}

// Pulse shape = Lorentzian * cos
double pulse(double x, double center, double width, double omega){

  double f;

  f = lorentzian(x, center, width) * cos(omega * (x - center));

  return f;
}

// Perturbation - puls shaped
double perturbation(double x, double center, double width, double omega, double amplitude){

  double f;

  f = amplitude * pulse(x, center, width,  omega);

  return f;
}
