//
// Functions for the Lindbladian
//

#include "complex.h"
#include "cmatrix.h"

//#include "structure.h"

// Update the Lindblad jump operators L
// -- OBS: Only for two-level systems
void update_Lindbladian(job &jp, complexmatrix &L, complexmatrix &P, complexmatrix &H);
