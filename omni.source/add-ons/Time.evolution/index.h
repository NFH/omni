//
// Index stuff
//

// Get the index of a cell
int cell_index(int cell1, int cell2, int cell3, cluster &c);

// Get the index of a site
int site_index(int cell1, int cell2, int cell3, int site, cluster &c);

// Get the index of an orbital (of all levels in the cluster)
int orbital_index(int cell1, int cell2, int cell3, int site, int orbital, cluster &c);
int orbital_index(int cell, int site, int orbital, cluster &c);
