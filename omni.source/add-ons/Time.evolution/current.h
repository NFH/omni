//
// Functions for currents
//
// JH 2019-09-11
//
#include "dmatrix.h"
#include "cmatrix.h"

// Compute the link current matrix
void link_current(cluster &c, doublematrix &Jlcurr, complexmatrix &P, complexmatrix &H);
