//
// analysis.h
//
// Analyse eigenstates of the Hamiltonian
//
#include "cmatrix.h"

// Compute the eigenvalues of the Hamiltonian matrix H
void compute_eval(cluster &c, complexmatrix &H);

// Compute the eigenvalues and -vectors of the Hamiltonian matrix H
void analyse_states(job & jp, cluster &c, complexmatrix &H);
