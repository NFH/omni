// job.h
//
// Task related definitions
//

#ifndef _JOB_
#define _JOB_

#include <vector>

#include "complex.h"
#include "cmatrix.h"
#include "vector3.h"
#include "jparallel.h"

#ifndef NULL
#define NULL    0
#endif

// Actual parameters
struct aatom{
  complex 	am, ap, az;	// Local electric field of this atom (for photoemission)
};

struct aslab{
  int 		natom;		// Number of atoms
  aatom 	*atom;		// Pointer to the atoms
  
  aslab();			// Constructor
  ~aslab();			// Destructor
};

struct actual{
  complex	e;		// Energy [in Hartree]
  double	theta;		// theta [in radians]
  double	phi;		// phi [in radians]
  vector2       q;              // k-parallel
  double        w;              // Weight of the k_parallel q
  complex       ax, ay, az;     // Electric field components (photon)
  double        thetaph;        // theta (photon)
  double        phiph;          // phi (photon)

  int 		nslab;		// Number of slabs
  aslab		*slab;		// Pointer to the slabs
  
  actual();			// Constructor
  actual(const actual &);	// Copy constructor
  actual &operator=(const actual &); // Assignment
  ~actual();			// Destructor
};



// Job definition
// We use substructures for each type of parameters
// - for real-space modes
struct jrs{
  char defectname[80];          // File name for defect definitions
  char clustername[80];         // File name for cluster definition
};

// - for SPLEED and SEE
struct jspleed{
  vector3	p0;		// Polarization of the incident beam
};

// - for photoemission
struct jpe{
  double	omega;		// Photon energy [in Hartree] (for PE)
  complex       dk;             // Dielectric constant (for PE)
  double        theta;          // Polar angle (sample tilting; for PE)
  double        phi;            // Azimuthal angle (sample tilting; for PE)
  double        phit;           // Azimuth of tilting axis (for PE)
//  double        thetaph;        // Polar angle photon (for PE)
//  double        phiph;          // Azimuthal angle photon (for PE)
  std::vector<value> angleph;   // Polar and azimuth mesh (for PE)
  complex       as;             // Photon electric field s-pol (for PE)
  complex       ap;             // Photon electric field p-pol (for PE)
  int           aflag;          // Photon angular flag (for PE)
};

// - for symmetry
struct jsymm{
  int		sym;		// Type of symmetry expansion (for SD)
  int		global;		// Flag for global or local frame (for SD)
  double	alpha;		// Intial z-rotation (for SD)
  vector3	rz;		// z-axis of configurational space (for SD)
  vector3	sz;		// z-axis for spin space (for SD)
  complexmatrix	S;		// Matrix for symmetry expansion (for SD)
  int		groups;		// Number of symmetry expansion (for SD)
  int		*group;		// Memory for symmetry expansions (for SD)
  jsymm();			// Constructor
  ~jsymm();			// Destructor
};

// - for 2BZ averaging
struct jkaverage{
  std::vector<value> klmean;  // k-par mesh (for 2BZ averaging)
  int           kmeanmode;      // Mode (for 2BZ averaging)
  int           kmeanstep;      // Step size (for 2BZ averaging)
  int           kmeanerrortype; // Error type (for 2BZ averaging)
  double        kmeanerror;     // Error (for 2BZ averaging)
  int           kmeanquad[4];   // Flags for used quadrants (for 2BZ averaging)
};

// - general parameters
struct jgeneral{
  int		spec;		// Mode
  int           ncrystal;       // Number of crystals
  int		nlayer;		// Number of layers
  double	eg;		// Energy radius (for plane-wave expansion)
  double	x;		// SOC factor
  int		dbl;		// Number of layer doublings (for Bloch states)
  int           dis;            // Disorder flag
  int           spherical;      // Charge-density flag
  double        xmin;           // Minimum eigenvalue (for complex bands)
  int           optpotu;        // Type of the optical potential, upper states
  int           optpotl;        // Type of the optical potential, lower states
  std::vector<value> el;        // Energy mesh
  int		kover;		// Flag for k_par mode
  std::vector<value> kl;        // k-par mesh
  double	t;		// Absolut temperature [in Kelvin]
};


struct job{
  jparallel     parallel;       // Parallelization parameters
  jgeneral      general;        // General parameters
  jkaverage     kaverage;       // For 2BZ averaging
  jkaverage     kaveragedis;    // For 2BZ averaging, disorder
  jpe           pe;             // For PE
  jspleed       spleed;         // For SPLEED and SEE 
  jsymm         symm;           // For symmetry
  jrs           rs;             // For real-space modes
};



// Constructors for actual objects
//inline aatom::aatom(){
//}

inline aslab::aslab(){
  atom = NULL;
}

inline actual::actual(){
  slab = NULL;
}

inline actual::actual(const actual &a){

  int 	i, j;
  
  // General stuff
  e 	= a.e;
  theta = a.theta;
  phi 	= a.phi;
  q 	= a.q;
  w	= a.w;
  ax	= a.ax;
  ay	= a.ay;
  az	= a.az;
  
  // Photoemission stuff
  thetaph	= a.thetaph;
  phiph		= a.phiph;
  nslab		= a.nslab;
  
  slab = new aslab[nslab];

  for(i = 0; i < nslab; i++){
     slab[i].natom = a.slab[i].natom;
     
     slab[i].atom = new aatom[slab[i].natom];
     
     for(j = 0; j < slab[i].natom; j++){
       slab[i].atom[j].ap = a.slab[i].atom[j].ap;
       slab[i].atom[j].am = a.slab[i].atom[j].am;
       slab[i].atom[j].az = a.slab[i].atom[j].az;
     }
  }
}

inline actual &actual::operator=(const actual &a){

  int 	i, j;
  
  // General stuff
  e 	= a.e;
  theta = a.theta;
  phi 	= a.phi;
  q 	= a.q;
  w	= a.w;
  ax	= a.ax;
  ay	= a.ay;
  az	= a.az;
  
  // Photoemission stuff
  thetaph	= a.thetaph;
  phiph		= a.phiph;
  nslab		= a.nslab;
  
  slab = new aslab[nslab];

  for(i = 0; i < nslab; i++){
     slab[i].natom = a.slab[i].natom;
     
     slab[i].atom = new aatom[slab[i].natom];

     for(j = 0; j < slab[i].natom; j++){
       slab[i].atom[j].ap = a.slab[i].atom[j].ap;
       slab[i].atom[j].am = a.slab[i].atom[j].am;
       slab[i].atom[j].az = a.slab[i].atom[j].az;
     }
  }
  
  return *this;
}


inline aslab::~aslab(){
  delete[] atom;
}

inline actual::~actual(){
  delete[] slab;
}


inline	jsymm::jsymm(){
  group = NULL;
}

inline	jsymm::~jsymm(){
  delete[] group;
}

#endif
