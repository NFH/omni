// matfunc.C
//
// enthaelt Ein- Ausgabe sowie Integrationsmethoden
// zur reellen Matrix-Funktionenklasse
//
// Autor: 1999 Dipl. Phys. Thomas Scheunemann
//        Gerhard Mercator Universitaet -GH- Duisburg
//        email: thomas@dagobert.uni-duisburg.de
//

#include <cstdlib>
#include <cstring>

#include "platform.h"

#include "dmatrix.h"
#include "function.h"
#include "dmatfunc.h"

using namespace std;

//
// Unaere Operatoren
//

doubletempmatrixfunction operator+(const doublematrixfunction &a)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublematrixfunction &a)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = -(*pa++);
  }
  return s;
}

doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &a)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++;
  }
  return s;
}

doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &a)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = -(*pa++);
  }
  return s;
}

//
// Binaere Operatoren
//

doubletempmatrixfunction operator*(const doublematrixfunction &a, const doublematrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * *pb++;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublematrixfunction &a, const doublematrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublematrixfunction &a, const doublematrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

doubletempmatrixfunction operator*(double a, const doublematrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a * *pb++;
  }
  return s;
}

doubletempmatrixfunction operator+(double a, const doublematrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a + *pb++;
  }
  return s;
}

doubletempmatrixfunction operator-(double a, const doublematrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a - *pb++;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublematrixfunction &a, double b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * b;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublematrixfunction &a, double b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + b;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublematrixfunction &a, double b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - b;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublefunction &a, const doublematrix &b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * b;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublematrix &a, const doublefunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a * *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &a, const doublediagmatrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempdiagmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &a, const doublediagmatrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempdiagmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &a, const doublediagmatrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempdiagmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator*(double a, const doublediagmatrixfunction &b)

{
  doubletempdiagmatrixfunction s(b.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a * *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator+(double a, const doublediagmatrixfunction &b)

{
  doubletempdiagmatrixfunction s(b.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a + *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator-(double a, const doublediagmatrixfunction &b)

{
  doubletempdiagmatrixfunction s(b.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a - *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &a, double b)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * b;
  }
  return s;
}

doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &a, double b)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + b;
  }
  return s;
}

doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &a, double b)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - b;
  }
  return s;
}

doubletempdiagmatrixfunction operator*(const doublefunction &a, const doublediagmatrix &b)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  double *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * b;
  }
  return s;
}

doubletempdiagmatrixfunction operator*(const doublediagmatrix &a, const doublefunction &b)

{
  doubletempdiagmatrixfunction s(b.n);
  doublediagmatrix *ps = s.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a * *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator*(const doublediagmatrix &a, const doublediagmatrixfunction &b)

{
  doubletempdiagmatrixfunction s(b.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a * *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator+(const doublediagmatrix &a, const doublediagmatrixfunction &b)

{
  doubletempdiagmatrixfunction s(b.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a + *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator-(const doublediagmatrix &a, const doublediagmatrixfunction &b)

{
  doubletempdiagmatrixfunction s(b.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a - *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &a, const doublediagmatrix &b)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * b;
  }
  return s;
}

doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &a, const doublediagmatrix &b)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + b;
  }
  return s;
}

doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &a, const doublediagmatrix &b)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - b;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublematrix &a, const doublediagmatrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a * *pb++;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublematrix &a, const doublediagmatrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a + *pb++;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublematrix &a, const doublediagmatrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a - *pb++;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublediagmatrixfunction &a, const doublematrix &b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * b;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublediagmatrixfunction &a, const doublematrix &b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + b;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublediagmatrixfunction &a, const doublematrix &b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - b;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublediagmatrix &a, const doublematrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a * *pb++;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublediagmatrix &a, const doublematrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a + *pb++;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublediagmatrix &a, const doublematrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a - *pb++;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublematrixfunction &a, const doublediagmatrix &b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * b;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublematrixfunction &a, const doublediagmatrix &b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + b;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublematrixfunction &a, const doublediagmatrix &b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - b;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublematrix &a, const doublematrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a * *pb++;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublematrix &a, const doublematrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a + *pb++;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublematrix &a, const doublematrixfunction &b)

{
  doubletempmatrixfunction s(b.n);
  doublematrix *ps = s.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = a - *pb++;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublematrixfunction &a, const doublematrix &b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * b;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublematrixfunction &a, const doublematrix &b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + b;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublematrixfunction &a, const doublematrix &b)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - b;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublematrixfunction &a, const doublediagmatrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * *pb++;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublematrixfunction &a, const doublediagmatrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublematrixfunction &a, const doublediagmatrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublediagmatrixfunction &a, const doublematrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * *pb++;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublediagmatrixfunction &a, const doublematrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublediagmatrixfunction &a, const doublematrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublematrixfunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * *pb++;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublematrixfunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublematrixfunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

doubletempmatrixfunction operator*(const doublefunction &a, const doublematrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  double *pa = a.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * *pb++;
  }
  return s;
}

doubletempmatrixfunction operator+(const doublefunction &a, const doublematrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  double *pa = a.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

doubletempmatrixfunction operator-(const doublefunction &a, const doublematrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  double *pa = a.v;
  doublematrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempdiagmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempdiagmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &a, const doublefunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempdiagmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  double *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator*(const doublefunction &a, const doublediagmatrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempdiagmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  double *pa = a.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ * *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator+(const doublefunction &a, const doublediagmatrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempdiagmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  double *pa = a.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ + *pb++;
  }
  return s;
}

doubletempdiagmatrixfunction operator-(const doublefunction &a, const doublediagmatrixfunction &b)

{
#ifdef DEBUG
  if(a.n != b.n){
    cerr << "ERROR from doubletempdiagmatrixfunction: Functions do not match\n";
    myexit(1);
  }
#endif
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  double *pa = a.v;
  doublediagmatrix *pb = b.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = *pa++ - *pb++;
  }
  return s;
}

//
// Funktionen
//

doubletempmatrixfunction inv(const doublematrixfunction &a)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = inv(*pa++);
  }
  return s;
}

doubletempmatrixfunction transp(const doublematrixfunction &a)

{
  doubletempmatrixfunction s(a.n);
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = transp(*pa++);
  }
  return s;
}

doubletempdiagmatrixfunction inv(const doublediagmatrixfunction &a)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = inv(*pa++);
  }
  return s;
}

doubletempdiagmatrixfunction transp(const doublediagmatrixfunction &a)

{
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  int i = s.n;
  while(--i >= 0){
    *ps++ = transp(*pa++);
  }
  return s;
}

//
// Integrationen
//

doubletempmatrixfunction integral(const doublematrixfunction &a, const doublematrix &b)

{
#ifdef DEBUG
  if(a.n <= 0){
    cerr << "ERROR from doubletempmatrixfunction: Invalid function\n";
    myexit(1);
  }
#endif
  doubletempmatrixfunction s(a.n);
  doublematrix sum = b;
  int i3 = (s.n - 1) / 3;
  int n3 = s.n - 3 * i3;
  doublematrix *ps = s.v;
  doublematrix *pa = a.v;
  doublematrix f0 = *pa++;
  doublematrix f1 = f0;
  doublematrix f2 = f0;
  doublematrix f3 = f0;
  *ps++ = sum;
  while(--i3 >= 0){
    f0 = f3;
    f1 = *pa++;
    f2 = *pa++;
    f3 = *pa++;
    sum = sum + (1.0 / 24.0) * (9 * f0 + 19 * f1 - 5 * f2 + f3);
    *ps++ = sum;
    sum = sum + (1.0 / 24.0) * (-f0 + 13 * f1 + 13 * f2 - f3);
    *ps++ = sum;
    sum = sum + (1.0 / 24.0) * (f0 - 5 * f1 + 19 * f2 + 9 * f3);
    *ps++ = sum;
  }
  switch(n3){
  case 1:
    break;
  case 2:
    f0 = f1;
    f1 = f2;
    f2 = f3;
    f3 = *pa++;
    sum = sum + (1.0 / 24.0) * (f0 - 5 * f1 + 19 * f2 + 9 * f3);
    *ps++ = sum;
    break;
  case 3:
    f0 = f2;
    f1 = f3;
    f2 = *pa++;
    f3 = *pa++;
    sum = sum + (1.0 / 24.0) * (-f0 + 13 * f1 + 13 * f2 - f3);
    *ps++ = sum;
    sum = sum + (1.0 / 24.0) * (f0 - 5 * f1 + 19 * f2 + 9 * f3);
    *ps++ = sum;
    break;
  }
  return s;
}

doublematrix integral(const doublematrixfunction &a)

{
#ifdef DEBUG
  if(a.n <= 0){
    cerr << "ERROR from doublematrix integral(): Invalid function\n";
    myexit(1);
  }
#endif
  doublematrix sum;
  doublematrix part;
  int i3 = (a.n - 1) / 3;
  int n3 = a.n - 3 * i3;
  doublematrix *pa = a.v;
  doublematrix f0 = *pa++;
  doublematrix f1 = f0;
  doublematrix f2 = f0;
  doublematrix f3 = f0;
  while(--i3 >= 0){
    f0 = f3;
    f1 = *pa++;
    f2 = *pa++;
    f3 = *pa++;
    part = (3.0 / 8.0) * (f0 + 3 * f1 + 3 * f2 + f3);
    if(used(sum)){
      sum = sum + part;
    }
    else{
      sum = part;
    }
  }
  switch(n3){
  case 1:
    break;
  case 2:
    f0 = f1;
    f1 = f2;
    f2 = f3;
    f3 = *pa++;
    part = (1.0 / 24.0) * (f0 - 5 * f1 + 19 * f2 + 9 * f3);
    if(used(sum)){
      sum = sum + part;
    }
    else{
      sum = part;
    }
    break;
  case 3:
    f0 = f2;
    f1 = f3;
    f2 = *pa++;
    f3 = *pa++;
    part = (1.0 / 3.0) * (f1 + 4 * f2 + f3);
    if(used(sum)){
      sum = sum + part;
    }
    else{
      sum = part;
    }
    break;
  }
  return sum;
}

doubletempdiagmatrixfunction integral(const doublediagmatrixfunction &a, const doublediagmatrix &b)

{
#ifdef DEBUG
  if(a.n <= 0){
    cerr << "ERROR from doubletempdiagmatrixfunction integral(): Invalid function\n";
    myexit(1);
  }
#endif
  doubletempdiagmatrixfunction s(a.n);
  doublediagmatrix sum = b;
  int i3 = (s.n - 1) / 3;
  int n3 = s.n - 3 * i3;
  doublediagmatrix *ps = s.v;
  doublediagmatrix *pa = a.v;
  doublediagmatrix f0 = *pa++;
  doublediagmatrix f1 = f0;
  doublediagmatrix f2 = f0;
  doublediagmatrix f3 = f0;
  *ps++ = sum;
  while(--i3 >= 0){
    f0 = f3;
    f1 = *pa++;
    f2 = *pa++;
    f3 = *pa++;
    sum = sum + (1.0 / 24.0) * (9 * f0 + 19 * f1 - 5 * f2 + f3);
    *ps++ = sum;
    sum = sum + (1.0 / 24.0) * (-f0 + 13 * f1 + 13 * f2 - f3);
    *ps++ = sum;
    sum = sum + (1.0 / 24.0) * (f0 - 5 * f1 + 19 * f2 + 9 * f3);
    *ps++ = sum;
  }
  switch(n3){
  case 1:
    break;
  case 2:
    f0 = f1;
    f1 = f2;
    f2 = f3;
    f3 = *pa++;
    sum = sum + (1.0 / 24.0) * (f0 - 5 * f1 + 19 * f2 + 9 * f3);
    *ps++ = sum;
    break;
  case 3:
    f0 = f2;
    f1 = f3;
    f2 = *pa++;
    f3 = *pa++;
    sum = sum + (1.0 / 24.0) * (-f0 + 13 * f1 + 13 * f2 - f3);
    *ps++ = sum;
    sum = sum + (1.0 / 24.0) * (f0 - 5 * f1 + 19 * f2 + 9 * f3);
    *ps++ = sum;
    break;
  }
  return s;
}

doublediagmatrix integral(const doublediagmatrixfunction &a)

{
#ifdef DEBUG
  if(a.n <= 0){
    cerr << "ERROR from doublediagmatrix integral(): Invalid function\n";
    myexit(1);
  }
#endif
  doublediagmatrix sum;
  doublediagmatrix part;
  int i3 = (a.n - 1) / 3;
  int n3 = a.n - 3 * i3;
  doublediagmatrix *pa = a.v;
  doublediagmatrix f0 = *pa++;
  doublediagmatrix f1 = f0;
  doublediagmatrix f2 = f0;
  doublediagmatrix f3 = f0;
  while(--i3 >= 0){
    f0 = f3;
    f1 = *pa++;
    f2 = *pa++;
    f3 = *pa++;
    part = (3.0 / 8.0) * (f0 + 3 * f1 + 3 * f2 + f3);
    if(used(sum)){
      sum = sum + part;
    }
    else{
      sum = part;
    }
  }
  switch(n3){
  case 1:
    break;
  case 2:
    f0 = f1;
    f1 = f2;
    f2 = f3;
    f3 = *pa++;
    part = (1.0 / 24.0) * (f0 - 5 * f1 + 19 * f2 + 9 * f3);
    if(used(sum)){
      sum = sum + part;
    }
    else{
      sum = part;
    }
    break;
  case 3:
    f0 = f2;
    f1 = f3;
    f2 = *pa++;
    f3 = *pa++;
    part = (1.0 / 3.0) * (f1 + 4 * f2 + f3);
    if(used(sum)){
      sum = sum + part;
    }
    else{
      sum = part;
    }
    break;
  }
  return sum;
}

//
// Ein- und Ausgabe
//

ostream &operator<<(ostream &os, const doublematrixfunction &a)

{
  int w = os.width(0);
  int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
  long f = os.flags(std::_Ios_Fmtflags(0));
#else
  long f = os.flags(0);
#endif
  doublematrix *pa = a.v;
  int i = a.n;
  while(--i >= 0){
    os.width(w);
    os.precision(p);
#if defined(GCC3) || defined(GCC4)
    os.flags(std::_Ios_Fmtflags(f));
#else
    os.flags(f);
#endif
    os << *pa++;
    if(i != 0){
      os << " ";
    }
  }
  os << "\n";
  return os;
}

istream &operator>>(istream &is, const doublematrixfunction &a)

{
  doublematrix *pa = a.v;
  int i = a.n;
  while(--i >= 0){
    is >> *pa++;
  }
  return is;
}

ostream &operator<<(ostream &os, const doublediagmatrixfunction &a)

{
  int w = os.width(0);
  int p = os.precision(0);
#if defined(GCC3) || defined(GCC4)
  long f = os.flags(std::_Ios_Fmtflags(0));
#else
  long f = os.flags(0);
#endif
  doublediagmatrix *pa = a.v;
  int i = a.n;
  while(--i >= 0){
    os.width(w);
    os.precision(p);
#if defined(GCC3) || defined(GCC4)
    os.flags(std::_Ios_Fmtflags(f));
#else
    os.flags(f);
#endif
    os << *pa++;
    if(i != 0){
      os << " ";
    }
  }
  os << "\n";
  return os;
}

istream &operator>>(istream &is, const doublediagmatrixfunction &a)

{
  doublediagmatrix *pa = a.v;
  int i = a.n;
  while(--i >= 0){
    is >> *pa++;
  }
  return is;
}
