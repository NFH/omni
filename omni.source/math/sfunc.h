// sfunc.h
//
// enthaelt Prototypen der in sfunc.C definierten Unterprogramme
//
// JH - 10/01/2007


#ifndef _SFUNC_
#define _SFUNC_

#include "complex.h"

void bessel(complex z, complex *xj, int l);
void neumann(complex z, complex *xj, complex *xn, int l);
void hankel(complex z, complex *h1, complex *h2, int l);
void mbessel(complex z, complex *xj, int l);
void mneumann(complex z, complex *xj, complex *xn, int l);
void mhankel(complex z, complex *h1, complex *h2, int l);

complex cerf(complex z);

double blm(int l1, int m1, int l2, int m2, int l3, int m3);
void sphrm(complex *ylm, complex ct, complex st, complex cf, int lmx);

double Gamma(double x);
double Gammap(double x);
double Gammapp(double x);
double DiGamma(double x);
double DiGammap(double x);

double deg2rad(double x);
double rad2deg(double x);

double Langevin(double x);

double binom(int n, int k);

#endif
