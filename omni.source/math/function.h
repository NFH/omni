// function.h
//
// enthaelt die Definition der reellen und komplexen Funktionenklassen
//

#ifndef _FUNCTION_
#define _FUNCTION_

#include <iostream>
#include <cstdlib>
#include <cstring>

#include "platform.h"

#include "complex.h"

#ifndef NULL
#define NULL	0
#endif

//
// Diese Klassen werden hier definiert
//

class doublefunction;
class complexfunction;
class doubletempfunction;
class complextempfunction;

// JH - Added the following lines for compatibility with gcc4.1
// Thus, the compiler option -ffriend-injection is not needed
complextempfunction integral(const complexfunction &, const complex &);
complextempfunction integral(const complexfunction &, double);
complex integral(const complexfunction &);

//
// Diese Klassen werden hier nicht definiert
//

class doublematrix;
class doublediagmatrix;

class complexmatrix;
class complexdiagmatrix;

class doublematrixfunction;
class doublediagmatrixfunction;

class doubletempmatrixfunction;
class doubletempdiagmatrixfunction;

class complexmatrixfunction;
class complexdiagmatrixfunction;

class complextempmatrixfunction;
class complextempdiagmatrixfunction;

//
// Definition der rellen Funktionenklasse
//

class doublefunction{
protected:
  int		n;		// Anzahl Stuetzstellen
  double		*v;		// Speicher fuer die Stuetzstellen
public:
  // Die Konstruktoren
  doublefunction();
  doublefunction(int);
  // Der Kopierkonstruktor
  doublefunction(const doublefunction &);
  // Der Umkopierer
  doublefunction(const doubletempfunction &);
  // Der Destruktor
  ~doublefunction();
  // Zugriff auf einzelne Stuetzstellen
  double &operator()(int) const;
  // Die Zuweisung
  doublefunction &operator=(double);
  doublefunction &operator=(const doublefunction &);
  doublefunction &operator=(const doubletempfunction &);
  // Setze die Zahl der Stuetzstellen
  doublefunction &setsize(int);
  int getsize() const;
  // Test, ob die Funktion benutzt ist
  friend int used(const doublefunction &);
  // Alle meine Freunde
  friend class complexfunction;
  // Unaere Operatoren
  friend doubletempfunction operator+(const doublefunction &);
  friend doubletempfunction operator-(const doublefunction &);
  // Binaere Operatoren
  friend doubletempfunction operator*(const doublefunction &, const doublefunction &);
  friend doubletempfunction operator/(const doublefunction &, const doublefunction &);
  friend doubletempfunction operator+(const doublefunction &, const doublefunction &);
  friend doubletempfunction operator-(const doublefunction &, const doublefunction &);
  friend doubletempfunction operator*(double, const doublefunction &);
  friend doubletempfunction operator/(double, const doublefunction &);
  friend doubletempfunction operator+(double, const doublefunction &);
  friend doubletempfunction operator-(double, const doublefunction &);
  friend doubletempfunction operator*(const doublefunction &, double);
  friend doubletempfunction operator/(const doublefunction &, double);
  friend doubletempfunction operator+(const doublefunction &, double);
  friend doubletempfunction operator-(const doublefunction &, double);
  friend complextempfunction operator*(const complexfunction &, const doublefunction &);
  friend complextempfunction operator/(const complexfunction &, const doublefunction &);
  friend complextempfunction operator+(const complexfunction &, const doublefunction &);
  friend complextempfunction operator-(const complexfunction &, const doublefunction &);
  friend complextempfunction operator*(const doublefunction &, const complexfunction &);
  friend complextempfunction operator/(const doublefunction &, const complexfunction &);
  friend complextempfunction operator+(const doublefunction &, const complexfunction &);
  friend complextempfunction operator-(const doublefunction &, const complexfunction &);
  friend complextempfunction operator*(const complex &, const doublefunction &);
  friend complextempfunction operator/(const complex &, const doublefunction &);
  friend complextempfunction operator+(const complex &, const doublefunction &);
  friend complextempfunction operator-(const complex &, const doublefunction &);
  friend complextempfunction operator*(const doublefunction &, const complex &);
  friend complextempfunction operator/(const doublefunction &, const complex &);
  friend complextempfunction operator+(const doublefunction &, const complex &);
  friend complextempfunction operator-(const doublefunction &, const complex &);
  friend doubletempmatrixfunction operator*(const doublefunction &, const doublematrix &);
  friend doubletempmatrixfunction operator*(const doublematrix &, const doublefunction &);
  friend complextempmatrixfunction operator*(const doublefunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator*(const doublefunction &, const doublediagmatrix &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrix &, const doublefunction &);
  friend complextempdiagmatrixfunction operator*(const doublefunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrix &, const doublefunction &);
  friend doubletempmatrixfunction operator*(const doublematrixfunction &, const doublefunction &);
  friend doubletempmatrixfunction operator+(const doublematrixfunction &, const doublefunction &);
  friend doubletempmatrixfunction operator-(const doublematrixfunction &, const doublefunction &);
  friend doubletempmatrixfunction operator*(const doublefunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator+(const doublefunction &, const doublematrixfunction &);
  friend doubletempmatrixfunction operator-(const doublefunction &, const doublematrixfunction &);
  friend doubletempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const doublefunction &);
  friend doubletempdiagmatrixfunction operator*(const doublefunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator+(const doublefunction &, const doublediagmatrixfunction &);
  friend doubletempdiagmatrixfunction operator-(const doublefunction &, const doublediagmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const doublefunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const doublefunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const doublefunction &);
  friend complextempmatrixfunction operator*(const doublefunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const doublefunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const doublefunction &, const complexmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const doublefunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const doublefunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const doublefunction &);
  friend complextempdiagmatrixfunction operator*(const doublefunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const doublefunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const doublefunction &, const complexdiagmatrixfunction &);
  // Funktionen
  friend doubletempfunction integral(const doublefunction &, double);
  friend doubletempfunction exp(const doublefunction &);
  friend double integral(const doublefunction &);
  // Ein- und Ausgabe
  friend std::ostream &operator<<(std::ostream &, const doublefunction &);
  friend std::istream &operator>>(std::istream &, const doublefunction &);
};

//
// Definition der komplexen Funktionenklasse
//

class complexfunction{
protected:
  int		n;		// Anzahl Stuetzstellen
  complex		*v;		// Speicher fuer die Stuetzstellen
public:
  // Die Konstruktoren
  complexfunction();
  complexfunction(int);
  // Der Kopierkonstruktor
  complexfunction(const complexfunction &);
  // Der Umkopierer
  complexfunction(const complextempfunction &);
  // Der Destruktor
  ~complexfunction();
  // Zugriff auf einzelne Stuetzstellen
  complex &operator()(int) const;
  // Die Zuweisung
  complexfunction &operator=(double);
  complexfunction &operator=(const complex &);
  complexfunction &operator=(const doublefunction &);
  complexfunction &operator=(const complexfunction &);
  complexfunction &operator=(const complextempfunction &);
  // Setze die Zahl der Stuetzstellen
  complexfunction &setsize(int);
  int getsize() const;
  
  // Test, ob die Funktion benutzt ist
  friend int used(const complexfunction &);
  // Alle meine Freunde
  // Unaere Operatoren
  friend complextempfunction operator+(const complexfunction &);
  friend complextempfunction operator-(const complexfunction &);
  // Binaere Operatoren
  friend complextempfunction operator*(const complexfunction &, const complexfunction &);
  friend complextempfunction operator/(const complexfunction &, const complexfunction &);
  friend complextempfunction operator+(const complexfunction &, const complexfunction &);
  friend complextempfunction operator-(const complexfunction &, const complexfunction &);
  friend complextempfunction operator*(const complex &, const complexfunction &);
  friend complextempfunction operator/(const complex &, const complexfunction &);
  friend complextempfunction operator+(const complex &, const complexfunction &);
  friend complextempfunction operator-(const complex &, const complexfunction &);
  friend complextempfunction operator*(const complexfunction &, const complex &);
  friend complextempfunction operator/(const complexfunction &, const complex &);
  friend complextempfunction operator+(const complexfunction &, const complex &);
  friend complextempfunction operator-(const complexfunction &, const complex &);
  friend complextempfunction operator*(double, const complexfunction &);
  friend complextempfunction operator/(double, const complexfunction &);
  friend complextempfunction operator+(double, const complexfunction &);
  friend complextempfunction operator-(double, const complexfunction &);
  friend complextempfunction operator*(const complexfunction &, double);
  friend complextempfunction operator/(const complexfunction &, double);
  friend complextempfunction operator+(const complexfunction &, double);
  friend complextempfunction operator-(const complexfunction &, double);
  friend complextempfunction operator*(const complexfunction &, const doublefunction &);
  friend complextempfunction operator/(const complexfunction &, const doublefunction &);
  friend complextempfunction operator+(const complexfunction &, const doublefunction &);
  friend complextempfunction operator-(const complexfunction &, const doublefunction &);
  friend complextempfunction operator*(const doublefunction &, const complexfunction &);
  friend complextempfunction operator/(const doublefunction &, const complexfunction &);
  friend complextempfunction operator+(const doublefunction &, const complexfunction &);
  friend complextempfunction operator-(const doublefunction &, const complexfunction &);
  friend complextempmatrixfunction operator*(const complexfunction &, const complexmatrix &);
  friend complextempmatrixfunction operator*(const complexfunction &, const doublematrix &);
  friend complextempmatrixfunction operator*(const complexmatrix &, const complexfunction &);
  friend complextempmatrixfunction operator*(const doublematrix &, const complexfunction &);
  friend complextempdiagmatrixfunction operator*(const complexfunction &, const complexdiagmatrix &);
  friend complextempdiagmatrixfunction operator*(const complexfunction &, const doublediagmatrix &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrix &, const complexfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrix &, const complexfunction &);
  friend complextempmatrixfunction operator*(const complexmatrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator*(const doublematrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator+(const complexmatrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator+(const doublematrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator-(const complexmatrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator-(const doublematrixfunction &, const complexfunction &);
  friend complextempmatrixfunction operator*(const complexfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator*(const complexfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator+(const complexfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator+(const complexfunction &, const doublematrixfunction &);
  friend complextempmatrixfunction operator-(const complexfunction &, const complexmatrixfunction &);
  friend complextempmatrixfunction operator-(const complexfunction &, const doublematrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexdiagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator*(const doublediagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator+(const complexdiagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator+(const doublediagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator-(const complexdiagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator-(const doublediagmatrixfunction &, const complexfunction &);
  friend complextempdiagmatrixfunction operator*(const complexfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator*(const complexfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator+(const complexfunction &, const doublediagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexfunction &, const complexdiagmatrixfunction &);
  friend complextempdiagmatrixfunction operator-(const complexfunction &, const doublediagmatrixfunction &);
  // Funktionen
  friend complextempfunction conj(const complexfunction &);
  friend complextempfunction real(const complexfunction &);
  friend complextempfunction imag(const complexfunction &);
  friend complextempfunction integral(const complexfunction &, const complex &);
  friend complextempfunction integral(const complexfunction &, double);
  friend complex integral(const complexfunction &);
  // Ein- und Ausgabe
  friend std::ostream &operator<<(std::ostream &, const complexfunction &);
  friend std::istream &operator>>(std::istream &, const complexfunction &);
};

//
// Definition der temporaeren reellen Funktionenklasse
//

class doubletempfunction{
protected:
  int		n;		// Anzahl Stuetzstellen
  double		*v;		// Speicher fuer die Stuetzstellen
public:
  // Der Konstruktor
  doubletempfunction(int);
  // Kein Destruktor
  // Alle meine Freunde
  friend class doublefunction;
  friend class doublematrixfunction;
  friend class doublediagmatrixfunction;
  // Unaere Operatoren
  friend doubletempfunction operator+(const doublefunction &);
  friend doubletempfunction operator-(const doublefunction &);
  // Binaere Operatoren
  friend doubletempfunction operator*(const doublefunction &, const doublefunction &);
  friend doubletempfunction operator/(const doublefunction &, const doublefunction &);
  friend doubletempfunction operator+(const doublefunction &, const doublefunction &);
  friend doubletempfunction operator-(const doublefunction &, const doublefunction &);
  friend doubletempfunction operator*(double, const doublefunction &);
  friend doubletempfunction operator/(double, const doublefunction &);
  friend doubletempfunction operator+(double, const doublefunction &);
  friend doubletempfunction operator-(double, const doublefunction &);
  friend doubletempfunction operator*(const doublefunction &, double);
  friend doubletempfunction operator/(const doublefunction &, double);
  friend doubletempfunction operator+(const doublefunction &, double);
  friend doubletempfunction operator-(const doublefunction &, double);
  // Funktionen
  friend doubletempfunction integral(const doublefunction &, double);
  friend doubletempfunction exp(const doublefunction &);
};

//
// Definition der temporaeren komplexen Funktionenklasse
//

class complextempfunction{
protected:
  int		n;		// Anzahl Stuetzstellen
  complex		*v;		// Speicher fuer die Stuetzstellen
public:
  // Der Konstruktor
  complextempfunction(int);
  // Kein Destruktor
  // Alle meine Freunde
  friend class complexfunction;
  friend class complexmatrixfunction;
  friend class complexdiagmatrixfunction;
  // Unaere Operatoren
  friend complextempfunction operator+(const complexfunction &);
  friend complextempfunction operator-(const complexfunction &);
  // Binaere Operatoren
  friend complextempfunction operator*(const complexfunction &, const complexfunction &);
  friend complextempfunction operator/(const complexfunction &, const complexfunction &);
  friend complextempfunction operator+(const complexfunction &, const complexfunction &);
  friend complextempfunction operator-(const complexfunction &, const complexfunction &);
  friend complextempfunction operator*(const complex &, const complexfunction &);
  friend complextempfunction operator/(const complex &, const complexfunction &);
  friend complextempfunction operator+(const complex &, const complexfunction &);
  friend complextempfunction operator-(const complex &, const complexfunction &);
  friend complextempfunction operator*(const complexfunction &, const complex &);
  friend complextempfunction operator/(const complexfunction &, const complex &);
  friend complextempfunction operator+(const complexfunction &, const complex &);
  friend complextempfunction operator-(const complexfunction &, const complex &);
  friend complextempfunction operator*(double, const complexfunction &);
  friend complextempfunction operator/(double, const complexfunction &);
  friend complextempfunction operator+(double, const complexfunction &);
  friend complextempfunction operator-(double, const complexfunction &);
  friend complextempfunction operator*(const complexfunction &, double);
  friend complextempfunction operator/(const complexfunction &, double);
  friend complextempfunction operator+(const complexfunction &, double);
  friend complextempfunction operator-(const complexfunction &, double);
  friend complextempfunction operator*(const complexfunction &, const doublefunction &);
  friend complextempfunction operator/(const complexfunction &, const doublefunction &);
  friend complextempfunction operator+(const complexfunction &, const doublefunction &);
  friend complextempfunction operator-(const complexfunction &, const doublefunction &);
  friend complextempfunction operator*(const doublefunction &, const complexfunction &);
  friend complextempfunction operator/(const doublefunction &, const complexfunction &);
  friend complextempfunction operator+(const doublefunction &, const complexfunction &);
  friend complextempfunction operator-(const doublefunction &, const complexfunction &);
  friend complextempfunction operator*(const complex &, const doublefunction &);
  friend complextempfunction operator/(const complex &, const doublefunction &);
  friend complextempfunction operator+(const complex &, const doublefunction &);
  friend complextempfunction operator-(const complex &, const doublefunction &);
  friend complextempfunction operator*(const doublefunction &, const complex &);
  friend complextempfunction operator/(const doublefunction &, const complex &);
  friend complextempfunction operator+(const doublefunction &, const complex &);
  friend complextempfunction operator-(const doublefunction &, const complex &);
  // Funktionen
  friend complextempfunction conj(const complexfunction &);
  friend complextempfunction real(const complexfunction &);
  friend complextempfunction  imag(const complexfunction &);
  friend complextempfunction integral(const complexfunction &, const complex &);
  friend complextempfunction integral(const complexfunction &, double);
};

// ***********************************************
// * Hier folgen nun die eigentlichen Funktionen *
// ***********************************************

//
// Erzeuge Platzhalter fuer eine reelle Funktion
//

inline	doublefunction::doublefunction()

{
  // Keine Stuetzstellen
  n = 0;
  // Kein Speicher
  v = NULL;
}

//
// Erzeuge eine reelle Funktion
//

inline	doublefunction::doublefunction(int nn)

{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from doublefunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new double[n];
}

//
// Der Kopierkonstruktor
//

inline	doublefunction::doublefunction(const doublefunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Speicher anfordern
  v = new double[n];
  // Und alles kopieren
  memcpy(v, z.v, n * sizeof(double));
}

//
// Der Umkopierer
//

inline	doublefunction::doublefunction(const doubletempfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Einfach Zeiger kopieren
  v = z.v;
}

//
// Der Destruktor
//

inline	doublefunction::~doublefunction()

{
  // Gibt den Speicher frei
  delete[] v;
}

//
// Zugriff auf einzelne Stuetzstellen
//

inline	double &doublefunction::operator()(int nn) const

{
#ifdef DEBUG
  if(nn < 0 || nn >= n){
    cerr << "ERROR from doublefunction: Number out of range\n";
    myexit(1);
  }
#endif
  // Gibt Referenz auf Element zurueck
  return v[nn];
}

//
// Die Zuweisung
//

inline	doublefunction &doublefunction::operator=(double z)

{
  double	*pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

inline	doublefunction &doublefunction::operator=(const doublefunction &z)

{
  if(n != z.n){
    // Anzahl kopieren
    n = z.n;
    // Gibt den Speicher frei
    delete[] v;
    // Speicher anfordern
    v = new double[n];
  }
  // Und alles kopieren
  memcpy(v, z.v, n * sizeof(double));
  return *this;
}

inline	doublefunction &doublefunction::operator=(const doubletempfunction &z)

{
  // Gibt den Speicher frei
  delete[] v;
  // Anzahl kopieren
  n = z.n;
  // Einfach Zeiger kopieren
  v = z.v;
  return *this;
}

//
// Setze Zahl der Stuetzstellen
//

inline	doublefunction &doublefunction::setsize(int nn)
{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from doublefunction: Invalid number\n";
    myexit(1);
  }
#endif
  if(n != nn){
    // Anzahl Stuetzstellen
    n = nn;
    // Gib den Speicher frei
    delete[] v;
    if(n != 0){
      // Noetigen Speicher anfordern
      v = new double[n];
    }
    else{
      // Keinen Speicher anfordern
      v = NULL;
    }
  }
  return *this;
}

inline int doublefunction::getsize() const
{
  return n;
}

//
// Test, ob die Funktion benutzt worden ist
//

inline int used(const doublefunction &z)

{
  return z.n != 0;
}

//
// Erzeuge Platzhalter fuer eine komplexe Funktion
//

inline	complexfunction::complexfunction()

{
  // Keine Stuetzstellen
  n = 0;
  // Kein Speicher
  v = NULL;
}

//
// Erzeuge eine komplexe Funktion
//

inline	complexfunction::complexfunction(int nn)

{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from complexfunction: Invalid number\n";
    myexit(1);
  }
#endif
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new complex[n];
}

//
// Der Kopierkonstruktor
//

inline	complexfunction::complexfunction(const complexfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Speicher anfordern
  v = new complex[n];
  // Und alles kopieren
  memcpy(v, z.v, n * sizeof(complex));
}

//
// Der Umkopierer
//

inline	complexfunction::complexfunction(const complextempfunction &z)

{
  // Anzahl Stuetzstellen
  n = z.n;
  // Einfach Zeiger kopieren
  v = z.v;
}

//
// Der Destruktor
//

inline	complexfunction::~complexfunction()

{
  // Gibt den Speicher frei
  delete[] v;
}

//
// Zugriff auf einzelne Stuetzstellen
//

inline	complex &complexfunction::operator()(int nn) const

{
#ifdef DEBUG
  if(nn < 0 || nn >= n){
    cerr << "ERROR from complexfunction: Number out of range\n";
    myexit(1);
  }
#endif
  // Gibt Referenz auf Element zurueck
  return v[nn];
}

//
// Die Zuweisung
//

inline	complexfunction &complexfunction::operator=(double z)

{
  complex	*pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

inline	complexfunction &complexfunction::operator=(const complex &z)

{
  complex	*pv = v;
  int i = n;
  while(--i >= 0){
    *pv++ = z;
  }
  return *this;
}

inline	complexfunction &complexfunction::operator=(const doublefunction &z)

{
  if(n != z.n){
    // Anzahl kopieren
    n = z.n;
    // Gibt den Speicher frei
    delete[] v;
    // Speicher anfordern
    v = new complex[n];
  }
  // Und alles kopieren
  complex *pv = v;
  double *pz = z.v;
  int i = n;
  while(--i >= 0){
    *pv++ = *pz++;
  }
  return *this;
}

inline	complexfunction &complexfunction::operator=(const complexfunction &z)

{
  if(n != z.n){
    // Anzahl kopieren
    n = z.n;
    // Gibt den Speicher frei
    delete[] v;
    // Speicher anfordern
    v = new complex[n];
  }
  // Und alles kopieren
  memcpy(v, z.v, n * sizeof(complex));
  return *this;
}

inline	complexfunction &complexfunction::operator=(const complextempfunction &z)

{
  // Gibt den Speicher frei
  delete[] v;
  // Anzahl kopieren
  n = z.n;
  // Einfach Zeiger kopieren
  v = z.v;
  return *this;
}

//
// Setze Zahl der Stuetzstellen
//

inline	complexfunction &complexfunction::setsize(int nn)
{
#ifdef DEBUG
  if(nn <= 0){
    cerr << "ERROR from complexfunction: Invalid number\n";
    myexit(1);
  }
#endif
  if(n != nn){
    // Anzahl Stuetzstellen
    n = nn;
    // Gib den Speicher frei
    delete[] v;
    if(n != 0){
      // Noetigen Speicher anfordern
      v = new complex[n];
    }
    else{
      // Keinen Speicher anfordern
      v = NULL;
    }
  }
  return *this;
}

inline int complexfunction::getsize() const
{
  return n;
}

//
// Test, ob die Funktion benutzt worden ist
//

inline int used(const complexfunction &z)

{
  return z.n != 0;
}

//
// Erzeuge eine temporaere reelle Funktion
//

inline	doubletempfunction::doubletempfunction(int nn)

{
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new double[n];
}

//
// Erzeuge eine temporaere komplexe Funktion
//

inline	complextempfunction::complextempfunction(int nn)

{
  // Anzahl Stuetzstellen
  n = nn;
  // Noetigen Speicher anfordern
  v = new complex[n];
}

#endif
