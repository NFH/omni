// intmath.h
//
// Some operations on integers
//
#include <iostream> 
#include <cstdlib>
#include <string>
#include <cstring>
using namespace std;

#ifndef _INTMATH_
#define _INTMATH_

// Funktion zum Berechnen des Betrages eines Integer

inline int iabs(int a){
  return (a >= 0) ? a : -a;
}

// Funktion zum Berechnen der kleinsten zweier Integer

inline int min(int a, int b){
  return (a < b) ? a : b;
}

// Funktion zum Berechnen des groessten zweier Integer

inline int max(int a, int b){
  return (a > b) ? a : b;
}

// Funktion zur Berechnung der groessten ganzen Zahl kleiner gleich x

inline	int integer(double x){
  int i;
  
  i = (int)x;
  if(x < i){
    i -= 1;
  }
  return i;
}

// Funktion zur Berechnung der naechsten ganzen Zahl

inline	int round2(double x){
  return integer(x + 1. / 2.);
}

// Kronecker delta
inline int delta(int i, int j){
  return (i == j) ? 1 : 0;
}

// Fibonacci number
// Returns the n-th Fibonacci number, starting with n = 0
inline unsigned long int fibonacci(int n){
  
  int i;
  
  unsigned long int fn, fnm1, fnm2;
  
  if(n < 0){
    cerr << "ERROR from fibonacci(): n < 0. " << n << endl;
    exit(1);
  }
  else if(n == 0){
    fn = 0;
  }
  else if(n == 1){
    fn = 1;
  }
  else{
    fnm2 = 0;
    fnm1 = 1;
    
    for(i = 1; i < n; i++){
      fn   = fnm1 + fnm2;

      fnm2 = fnm1;
      fnm1 = fn;
    }
  }
  
  return fn;
}

#endif
