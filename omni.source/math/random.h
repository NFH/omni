//
// Driver functions for pseudo random numbers
//

#ifndef _RANDOM_
#define _RANDOM_

#include <cstdio>
#include <cstdlib>
#include <ctime>


// Initialize the random number generator
void init_omni_random(int seed);

// Produce a random number between 0 and 1
double omni_random();

#endif
