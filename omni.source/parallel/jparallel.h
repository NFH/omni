
#ifndef _JPARALLEL_
#define _JPARALLEL_

#include <string>
#include <vector>
#include <map>
#include "iostuff.h"
#include "state.h"

// List of values (used for energy and k_parallel)
struct  value{
  double        item1;          // Erster Untereintrag
  double        item2;          // Zweiter Untereintrag
  double        item3;          // Dritter Untereintrag
};

struct state;

class mpi_stuff{
public:
  static const int NO_MPI = -1;
  int nprocs;
  int rank;
  std::string processor_name;
  int  name_len;
  void init(int, char**);
  void end();
  //This version is useful for a mpi and omp k-spliting
  std::vector<int> split_list(const std::vector<value> &) const;
  //This one is for just mpi splitting. omp should do an inner task or
  //other thing
  std::vector<value> new_list(const std::vector<value> &) const;
  //only needed when the iteration buffer is splitted between both mpi
  //and mp, at the same time and level
  void sync_buffer(Buffer &, const int, const std::string, std::ostream &);
  void sync_staters(state &);
};


class mp_stuff{
private:
  int nthreads;
public:
  void set_thread_number(const int);
  int get_thread_number() const;
};





// - parallelization parameters
class jparallel{
public:
  mpi_stuff mpi; 
  mp_stuff  mp;
};



#endif
