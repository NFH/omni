
#ifndef _IOSTUFF_
#define _IOSTUFF_

#include <list>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

class Buffer;
class Logger;

class Buffer{
private:
  std::list<std::pair<int, std::string> > bufflist;
  int current;
public:
  const int id_size;
  Buffer();
  Buffer(int);
  void write(const int id, std::string str, std::ostream& dataOut);
};





//FM: a small adaptation from http://stackoverflow.com/a/2212940
// Write a stream buffer that prefixes each line with the required message
class LoggerBuf: public std::stringbuf{
  std::ostream& output;
public:
  LoggerBuf(std::ostream& str):output(str){
    mpi_rank_threshold = 0;
  }
  // When we sync the stream with the output. 
  // 1) Output "Message" then the buffer
  // 2) Reset the buffer
  // 3) flush the actual output stream we are using.
  virtual int sync ()
  {
    if(log_level >= log_threshold && mpi_rank <= mpi_rank_threshold){
      output << header << str();
      str("");
      output.flush();
    }
    else{
      str("");
    }
    return 0;
  }
  std::string header;
  int log_level;
  int mpi_rank;
  int log_threshold;
  int mpi_rank_threshold;
};

class Logger: public std::ostream{
  // Logger just uses a version of my special buffer
  LoggerBuf buffer;
  std::string header_pattern;
public:
  static int const DEBUG   = 0;
  static int const INFO    = 1;
  static int const MESSAGE = 2;
  static int const WARNING = 3;
  static int const ERROR   = 4;    
  Logger(std::ostream&);
  void setHeader(const std::string);
  void set_log_level(const int);
  void set_log_threshold(const int);
  void set_mpi_rank(const int);
  void set_mpi_threshold(const int);
};
// Usage of above class
// int main()
// {
//   MyStream myStream(std::cout);
//   myStream << 1 << 2 << 3 << std::endl << 5 << 6 << std::endl << 7 << 8 << std::endl;
// }

#endif
