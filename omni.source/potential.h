// potential.h
//
// Defines the structures for potentials
//

#ifndef _POTENTIAL_
#define _POTENTIAL_

#include "function.h"
#include "cmatfunc.h"

// Define the potential types (ptype)
#define POT_MAG_EMPTY_SPHERE_SIC  -4  // empty, magnetic, spherical, SIC
#define POT_EMPTY_SPHERE_SIC      -3  // empty, non-magnetic, spherical, SIC
#define POT_MAG_EMPTY_SPHERE      -2  // empty, magnetic, spherical
#define POT_EMPTY_SPHERE          -1  // empty, non-magnetic, spherical
#define POT_NONMAG_SPHERE          0  // non-magnetic, spherical
#define POT_MAG_SPHERE             1  // magnetic, spherical
#define POT_MAG_NONSPHERE          2  // magnetic, non-spherical
#define POT_NONMAG_SPHERE_SIC      3  // non-magnetic, spherical, SIC
#define POT_MAG_SPHERE_SIC         4  // magnetic, spherical, SIC
#define POT_NONMAG_SPHERE_U        5  // non-magnetic, spherical, LDA+U
#define POT_MAG_SPHERE_U           6  // magnetic, spherical, LSDA+U

struct psic{
  complexfunction v;              // SIC potential
  complexfunction b;              // Magnetic SIC potential
  int             l;              // Angular quantum number
  int             m;              // Magnetic quantum number
  int             kappa;          // Kappa (total angular momentum)
  int             mu2;            // Twice mue (z-projection of total angular momentum)
  psic();                         // Constructor
  psic(const psic&);              // Copy constructor
  ~psic();                        // Destructor
  psic &operator=(const psic &);  // Assignment (copy)
};

struct potential{
  double	  z;		// Core charge        
  int		  ptype;	// Potential type            
  int		  n;		// Number of radial mesh points
  double	  h;		// Factor for exponential radial mesh
  double	  rm;		// Muffin-tin radius [in Bohr]
  double	  m;		// Mass
  double	  td;		// Debye temperature [in Kelvin]
  double	  ef;		// Fermi level [in eV]
  double	  lambda;	// Electron-phonon coupling strength
  double	  a;		// Constant for thermal correction
  doublefunction  r;		// Radial mesh [in Bohr]
  complexfunction v;		// Potential mesh (times radius, w/o core contribution)
  complexfunction b;		// Magnetic potential mesh [in Hartree]
  complexmatrixfunction vp, vm; // Full potential
  int             nsic;         // Number of SIC potentials
  psic            *sic;         // The SIC potentials
  double          scale;        // Scaling factor
  potential();			// Constructor
  potential(const potential &);	// Copy constructor
  ~potential();			// Destructor
  potential &operator=(const potential &); // Assignment (copy)
};

// Constructor
inline  psic::psic(){
}

// Copy constructor
inline psic::psic(const psic &p){

  v     = p.v;
  b     = p.b;
  l     = p.l;
  m     = p.m;
  kappa = p.kappa;
  mu2   = p.mu2;
}

// Assignment
inline psic &psic::operator=(const psic &p){

  v     = p.v;
  b     = p.b;
  l     = p.l;
  m     = p.m;
  kappa = p.kappa;
  mu2   = p.mu2;

  return *this;
}

// Destructor
inline  psic::~psic(){
}



// Constructor
inline	potential::potential(){
  sic = NULL;
}

// Copy constructor
inline potential::potential(const potential &p){

  int isic;

  z     = p.z;
  ptype = p.ptype;
  n     = p.n;
  h     = p.h;
  rm    = p.rm;
  m     = p.m;
  td    = p.td;
  a     = p.a;
  r     = p.r;

  switch(p.ptype){
  case POT_EMPTY_SPHERE:
  case POT_NONMAG_SPHERE:
  case POT_NONMAG_SPHERE_SIC:
  case POT_NONMAG_SPHERE_U:
    v     = p.v;
    break;
  case POT_MAG_EMPTY_SPHERE:
  case POT_MAG_SPHERE:
  case POT_MAG_NONSPHERE:
  case POT_MAG_SPHERE_SIC:
  case POT_MAG_SPHERE_U:
    v     = p.v;
    b     = p.b;
    break;
  }
  // The non-spherical potentials are not copied yet.
  //vp    = p.vp;
  //vm    = p.vm;
  nsic  = p.nsic;
  scale = p.scale;

  // Copy the SIC potentials
  delete[] sic;
  sic = new psic[nsic + 1];
  
  for(isic = 0; isic <= nsic; isic++){
    sic[isic] = p.sic[isic];
  }

}

// Assignment
inline potential &potential::operator=(const potential &p){

  int isic;

  z     = p.z;
  ptype = p.ptype;
  n     = p.n;
  h     = p.h;
  rm    = p.rm;
  m     = p.m;
  td    = p.td;
  a     = p.a;
  r     = p.r;

  switch(p.ptype){
  case POT_EMPTY_SPHERE:
  case POT_NONMAG_SPHERE:
  case POT_NONMAG_SPHERE_SIC:
  case POT_NONMAG_SPHERE_U:
    v     = p.v;
    break;
  case POT_MAG_EMPTY_SPHERE:
  case POT_MAG_SPHERE:
  case POT_MAG_NONSPHERE:
  case POT_MAG_SPHERE_SIC:
  case POT_MAG_SPHERE_U:
    v     = p.v;
    b     = p.b;
    break;
  }

  // The non-spherical potentials are not copied yet.
  //vp    = p.vp;
  //vm    = p.vm;
  nsic  = p.nsic;
  scale = p.scale;

  // Copy the SIC potentials
  delete[] sic;
  sic = new psic[nsic + 1];
  
  for(isic = 0; isic <= nsic; isic++){
    sic[isic] = p.sic[isic];
  }

  return *this;

}

// Destructor
inline	potential::~potential(){
  delete[] sic;
}

#endif
